import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// import {formatNameMapping} from '../utils/constants';
const SingleChoice = props => {
  const handleSelect = props => {
    const {optionId, answerContent, isCorrect, answerScore} = props;
    const answerObj = {
      optionId,
      answerContent,
      isCorrect,
      score: answerScore,
    };
    if (!props.checkButtonClicked) {
      props.handleAnswerChanged(answerObj);
    }
  };
  return (
    <View>
      {/* view types on selected or not selected answers */}
      {!props.checkButtonClicked || props.scoringType !== 'correctwise' ? (
        <TouchableOpacity
          onPress={() => handleSelect(props)}
          key={props.answerType}>
          <View
            style={[
              props.selectedAnswer[0] &&
              props.selectedAnswer[0].optionId == props.optionId
                ? styles.selectedAnswer
                : styles.questionView,
            ]}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
              }}>
              <View
                style={[
                  props.selectedAnswer[0] &&
                  props.selectedAnswer[0].optionId == props.optionId
                    ? styles.selectedAnswerCircle
                    : styles.questionCircle,
                ]}>
                <Text
                  style={{
                    textAlign: 'center',
                    justifyContent: 'center',

                    color:
                      props.selectedAnswer[0] &&
                      props.selectedAnswer[0].optionId == props.optionId
                        ? 'white'
                        : 'black',
                  }}>
                  {props.answerType}
                </Text>
              </View>
              <View style={{flex: 0.8, justifyContent: 'center'}}>
                <Text style={{lineHeight: 16, justifyContent: 'space-between'}}>
                  {props.answerContent}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      ) : (
        <View
          style={[
            props.checkButtonClicked &&
            props.scoringType === 'correctwise' &&
            props.isCorrect
              ? styles.correctAnswerView
              : props.selectedAnswer[0].optionId == props.optionId &&
                !props.isCorrect
              ? styles.incorrectAnswerView
              : styles.questionView,
          ]}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
            }}>
            <View style={styles.questionCircle}>
              {/* <Text
                style={{
                  textAlign: 'center',
                  justifyContent: 'center',

                  color: 'black',
                }}>
                {props.answerType}
              </Text> */}
              {props.checkButtonClicked &&
              props.scoringType === 'correctwise' &&
              props.isCorrect ? (
                <MaterialCommunityIcons
                  style={styles.correctIcon}
                  name="check"
                />
              ) : props.selectedAnswer[0].optionId == props.optionId &&
                !props.isCorrect ? (
                <Entypo style={styles.wrongIcon} name="cross" />
              ) : null}
            </View>
            <View style={{flex: 0.8, justifyContent: 'center'}}>
              <Text style={{lineHeight: 16, justifyContent: 'space-between'}}>
                {props.answerContent}
              </Text>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};
const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  questionView: {
    marginHorizontal: 6,
    margin: 3,
    padding: 6,
    borderRadius: 8,
    borderWidth: 0.5,
  },
  selectedAnswer: {
    marginHorizontal: 6,
    margin: 3,
    padding: 6,
    borderRadius: 8,
    borderWidth: 2,
    borderColor: '#f14836',
  },
  selectedAnswerCircle: {
    height: 27,
    width: 27,
    borderRadius: 150 / 2,
    justifyContent: 'center',
    margin: 15,
    backgroundColor: '#f14f3f',
  },
  questionCircle: {
    height: 27,
    width: 27,
    borderRadius: 150 / 2,
    justifyContent: 'center',
    margin: 15,
    backgroundColor: '#ebe8e8',
  },
  correctAnswerView: {
    marginHorizontal: 6,
    margin: 3,
    padding: 6,
    borderRadius: 8,
    borderWidth: 2,
    borderColor: '#00c939',
  },
  incorrectAnswerView: {
    marginHorizontal: 6,
    margin: 3,
    padding: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: 'red',
  },
  wrongIcon: {
    textAlign: 'center',
    justifyContent: 'center',
    color: '#f14f3f',
    fontSize: 30,
    backgroundColor: 'white',
  },
  correctIcon: {
    textAlign: 'center',
    justifyContent: 'center',
    color: '#00c939',
    fontSize: 30,
    backgroundColor: 'white',
  },
});
export default SingleChoice;
