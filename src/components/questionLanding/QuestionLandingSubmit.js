import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { get, isEmpty, countBy } from 'lodash-es';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { withNavigation } from 'react-navigation';

const QuestionLandingSubmit = props => {
  const {
    quizSlug,
    sectionSlug,
    courseSlug,
    attemptId,
    questionSlug,
    time,
  } = props;
  const { insertQuestionTrack, updateQuizTrack } = useStoreActions(
    actions => actions.quiz,
  );
  const { insertCandidateStateTrack } = useStoreActions(actions => actions.user);
  const { sectionStartTime, insertCourseTrack } = useStoreActions(
    actions => actions.course,
  );
  const { selectedAnswers, questionSlugs, startTime } = useStoreState(
    state => state.quiz,
  );
  const sections = useStoreState(
    state => state.course && state.course.sections,
  );
  const courseData = useStoreState(state => state.course);
  const user = useStoreState(state => state.user);
  const totalQuestionsCount =
    get(questionSlugs, 'length') || Object.keys(selectedAnswers).length;
  let { true: correctQuestionsCount } = countBy(selectedAnswers, 'isCorrect');
  if (!correctQuestionsCount) {
    correctQuestionsCount = 0;
  }
  const [nextSession, setNextSession] = useState('complete');

  // const correctAnswersDecimal = correctQuestionsCount /
  const timeTaken = Date.now() - startTime;

  const handleSubmitBtnClick = () => {
    const quizVariable = {
      id: attemptId,
      score: correctQuestionsCount,
      is_completed: true,
      time_spent_in_ms: timeTaken,
    };
    const questionVariable = {
      candidate_quiz_track_id: attemptId,
      candidate_id: user.id,
      slug: questionSlug,
      is_correct: selectedAnswers[questionSlug].isCorrect
        ? selectedAnswers[questionSlug].isCorrect
        : false,
      submitted_answer: selectedAnswers[questionSlug].selectedOptions,
      steps_used: selectedAnswers[questionSlug].stepsUsed,
      time_spent_in_ms: time,
      score: selectedAnswers[questionSlug].selectedOptions.map(option => {
        return option.score ? option.score : null;
      }),
      step_info: null,
    };
    insertCandidateStateTrack({
      candidate_state_id: user.next_state.id,
      status: 'completed',
      current_action: user.next_state.lifecycle_state.state.action.action_type,
      next_candidate_state_id: user.upcoming_states[0]
        ? user.upcoming_states[0].id
        : null
    }).then(
      resp => {
        console.log('completed', user.next_state.id, user);
      },
      error => {
        console.log('error in insertCandidateStateTrack', error, user);
      },
    );

    insertQuestionTrack(questionVariable).then(
      resp => {
        // console.log('resp in insertQuestionTrack', resp);
      },
      error => {
        console.log('error in insertQuestionTrack', error);
      },
    );
    updateQuizTrack(quizVariable).then(
      quizResp => {
        // console.log('quizResp in updateQuizTrack', quizResp);
      },
      quizError => {
        console.log('error in updateQuizTrack', quizError);
      },
    );
    // sectionStartTime({courseData, next_session_slug: nextSession})
    if (courseSlug) {
      insertCourseTrack({
        candidate_id: user.id,
        course_slug: courseSlug,
        section_slug: sectionSlug,
        time_spent_in_ms: timeTaken,
        section_type: 'quiz',
      });
    }
    if (courseSlug) {
      props.navigation.navigate('CourseContent', {
        course_slug: courseData.course_slug,
        session_slug: nextSession,
      });
    } else {
      props.navigation.navigate('Prepare');
    }
  };

  const handleNextSession = () => {
    console.log('sections', sections);
    let found = false;
    for (let section of sections) {
      if (found && !section.is_module) {
        return setNextSession(section.slug);
      }
      if (section.slug == sectionSlug) {
        found = true;
      }
    }
  };
  useEffect(() => {
    if (!isEmpty(sections)) {
      handleNextSession();
    }
  }, []);
  return (
    <View>
      {/* sends to courseContent component */}

      <TouchableOpacity
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => handleSubmitBtnClick()}>
        <View
          style={{
            flex: 0.25,

            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={styles.submitCompletely}>Submit Completely</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  submitCompletely: {
    color: 'white',
    margin: 20,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    width: width / 2,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#f14836',
  },
});

export default withNavigation(QuestionLandingSubmit);
