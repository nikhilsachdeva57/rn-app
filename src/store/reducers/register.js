import * as actionTypes from '../actions/actionTypes';

const initialState = {
    isAvailable: false,
    firstName: '',
    lastName: '',
    avatar: {},
    isEdit: false,
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_FIRST_NAME:
            return { ...state, firstName: action.firstName, isAvailable: true };
        case actionTypes.SET_LAST_NAME:
            return { ...state, lastName: action.lastName, isAvailable: true };
        case actionTypes.SET_AVATAR:
            return { ...state, avatar: action.avatar, isAvailable: true };
        case actionTypes.SET_EDIT_PROFILE:
            return { ...state, isEdit: action.isEdit };
        default:
            return state;
    }
};

export default reducer;
