{
  /* handles quiz handing,
    can be used as a standalone for providing 
    quiz directly outside of classroom
*/
}
import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { isEmpty, get } from 'lodash-es';
import { withNavigation } from 'react-navigation';
const QuizLanding = props => {
  const quizSlug = props.navigation.getParam('quiz_slug')
    ? props.navigation.getParam('quiz_slug')
    : props.quizSlug;
  const courseSlug = props.courseSlug;
  const sectionSlug = props.sectionSlug;
  const quizData = useStoreState(state => state.quiz);
  const courseData = useStoreState(state => state.course);
  // no. of attempts of quiz
  const [isAttemptIdCreated, setIsAttemptIdCreated] = useState(false);
  const questionSlugs = get(quizData, 'questionSlugs', []);
  // store > model > quiz.js

  const {
    getQuizDetails,
    getQuestionDetails,
    updateQuizDetails,
    insertQuizTrack,
  } = useStoreActions(actions => actions.quiz);

  const { getCourseDetails } = useStoreActions(actions => actions.course);

  const variables = {
    slug: quizSlug,
  };

  useEffect(() => {
    if (isEmpty(quizSlug)) {
      return;
    }
    if (courseSlug && !courseData.id) {
      getCourseDetails({ courseSlug });
    }
    getQuizDetails({ quizSlug });
    insertQuizTrack(variables);
    updateQuizDetails({
      currentQuestionIndex: -1,
      currentSectionIndex: -1,
    });
  }, []);

  // a quiz has many question, by default obviosuly show the first question
  useEffect(() => {
    if (!isEmpty(questionSlugs)) {
      getQuestionDetails({ questionSlug: questionSlugs[0] });
    }
  }, [questionSlugs]);

  useEffect(() => {
    if (!isEmpty(quizData.attemptId)) {
      setIsAttemptIdCreated(true);
    }
  }, [quizData]);

  if (!isAttemptIdCreated) {
    return (
      <View style={{ flex: 1 }}>
        <ActivityIndicator
          size="large"
          style={{ flex: 1, alignSelf: 'center' }}
          color="#ee6002"
        />
      </View>
    );
  }

  // handling start of quiz, map startTime
  const handleStartBtnClick = async () => {
    const currentTime = new Date().getTime();
    try {
      await AsyncStorage.setItem('quizStartTime', toString(currentTime));
      console.log('quiz start time set', currentTime)
    } catch (error) {
      console.debug(error);
    }
    updateQuizDetails({
      startTime: currentTime,
    });

    props.navigation.navigate('QuestionLanding', {
      courseSlug,
      sectionSlug,
      quizSlug,
      questionSlug: questionSlugs[0],
      attemptId: quizData.attemptId,
    });
    return null;

    // return (
    //     <QuestionLanding data={data} ></QuestionLanding>
    // )
  };

  //TODO: bring instructions from backend

  const instructions = [
    'Attempt every question, there is no negative marking',
    'When you click on ‘Submit’, you will immediately see whether you got the question correct',
    'Click on ‘Next’ to move on to the next question',
    'If you are stuck, click on ‘Step by Step’ to break down the problem into solution steps',
    'Don’t worry about time, you have an unlimited amount',
  ];

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: 'center',
        }}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            paddingTop: 20,
            alignItems: 'center',
          }}>
          <Text style={styles.instructionsHeading}> INSTRUCTIONS</Text>
          <View style={{ flex: 6 }}>
            {instructions.map((instruction, index) => {
              return (
                <View style={styles.containerText} key={index}>
                  <View style={styles.stepsHorizontal}>
                    <View style={styles.circle}>
                      <Text
                        style={{
                          textAlignVertical: 'center',
                          textAlign: 'center',
                          marginTop: 2,
                          color: 'white',
                        }}>
                        {index + 1}
                      </Text>
                    </View>
                    <Text style={styles.stepsText}>{instruction}</Text>
                  </View>
                </View>
              );
            })}
          </View>
          <View style={{ alignSelf: 'center', flex: 2, margin: 20 }}>
            <TouchableOpacity onPress={handleStartBtnClick}>
              <View>
                <Text style={styles.nextSession}>Submit </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  stepsText: {
    marginStart: 10,
    marginEnd: 20,
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    color: '#2F4F4F',
  },
  stepsHorizontal: {
    flexDirection: 'row',
    marginTop: 12,
    flex: 1,
  },
  circle: {
    width: 25,
    height: 25,
    borderRadius: 100 / 2,
    backgroundColor: '#f14836',
  },
  containerText: {
    fontFamily: 'ProductSans-Regular',
    textAlign: 'center',
    marginEnd: 32,
    marginStart: 32,
    flex: 1,
  },
  testButton: {
    height: 20,

    marginTop: 20,
  },
  instructions: {},
  nextSession: {
    color: 'white',
    margin: 20,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',

    width: 150,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#f14836',
    flex: 1,
  },
  instructionsHeading: {
    fontFamily: 'ProductSans-Regular',
    fontSize: 35,
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 24,
    flex: 1,
    alignSelf: 'center',
  },
});

export default withNavigation(QuizLanding);
