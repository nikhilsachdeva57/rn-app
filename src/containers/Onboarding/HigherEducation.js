import React, { Component, useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TextInput,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  ToastAndroid,
  StatusBar,
  Alert,
  ImageBackground,
} from 'react-native';
import { isEmpty } from 'lodash-es';
import { TextInputLayout } from 'rn-textinputlayout';
import { Button } from 'react-native-elements';
import { useStoreState, useStoreActions } from 'easy-peasy';
import ItemsSelectTwo from '../../components/Onboarding/SelectTwoPicker/ItemsSelectTwo';
import * as onboardingUtils from '../../utils/onboardingUtils'

export default function HigherEducation(props) {
  const user = useStoreState(state => state.user);

  const {
    insertCandidateEducationInfo,
    getCandidateEducationInfo,
    insertCandidateStateTrack,
    getDegree,
  } = useStoreActions(actions => actions.user);

  const { id, onboarding_config, state_index, current_action } = props.navigation.state.params;

  const [isValidDegree, setValidDegree] = useState(false);
  const [isValidCollege, setValidCollege] = useState(false);
  const [isValidCGPA, setValidCGPA] = useState(false);
  const [isValidYOG, setValidYOG] = useState(false);

  const [selectedDegree, setSelectedDegree] = useState([]);
  const [selectedYog, setSelectedYOG] = useState([]);
  const [clgName, setClgName] = useState('');
  const [cgpa, setCgpa] = useState(0);

  const [isDegreeFlag, setDegreeFlag] = useState(false);
  const [isNewUser, setNewUser] = useState(true);
  const previousPage = props.navigation.state.params.from;
  let nextButtonName = 'Save';

  useEffect(() => {
    return () => getCandidateEducationInfo(user.id);
  }, [])
  useEffect(() => {
    if (previousPage != 'Profile') {
      nextButtonName = 'Next';
    }

    if (!isEmpty(user) && isEmpty(user.enums_degree) && !isDegreeFlag) {
      getDegree();
      setDegreeFlag(true);
    }
    if (
      isEmpty(user.candidate_bachelor_degree_info) &&
      !isEmpty(user.id) && isNewUser
    ) {
      getCandidateEducationInfo(user.id);
      setNewUser(false);
    }
    if (
      isEmpty(selectedDegree) &&
      !isEmpty(user.candidate_bachelor_degree_info)
    ) {
      setSelectedDegree(getDegreeObject(user.candidate_bachelor_degree_info.degree_name));
      setValidDegree(true);
    }

    if (
      isEmpty(selectedYog) &&
      !isEmpty(user.candidate_bachelor_degree_info)
    ) {
      setSelectedYOG([{
        id: getYogID(user.candidate_bachelor_degree_info.year_of_graduation),
        name: user.candidate_bachelor_degree_info.year_of_graduation.toString(),
      }]);
      setValidYOG(true);
    }

    if (!isEmpty(user.candidate_bachelor_degree_info) && !isEmpty(user.candidate_bachelor_degree_info.cgpa.toString())) {
      setCgpa(user.candidate_bachelor_degree_info.cgpa);
      setValidCGPA(true);
    }
    if (!isEmpty(user.candidate_bachelor_degree_info) && !isEmpty(user.candidate_bachelor_degree_info.college_name)) {
      setClgName(user.candidate_bachelor_degree_info.college_name);
      setValidCollege(true);
    }


  });


  //to create year of graduation options from 1995 to 2019
  var yogOptions = [];
  for (var i = 1; i <= 25; i++) {
    yogOptions.push({
      id: i,
      name: (1994 + i).toString(),
    })
  }

  var degreeOptions = [];
  for (var i in user.enums_degree) {
    var item = user.enums_degree[i];
    degreeOptions.push({
      id: item.id,
      slug: item.slug,
      name: item.name,
    });
  }

  function getDegreeObject(value) {
    for (var i = 0; i < degreeOptions.length; i++) {
      if (degreeOptions[i].slug == value) {
        return [{
          id: degreeOptions[i].id,
          slug: degreeOptions[i].slug,
          name: degreeOptions[i].name,
        }]
      }
    }
  }

  const getYogID = (event) => {
    for (var i = 0; i < yogOptions.length; i++) {
      if (yogOptions[i].name == event) {
        return yogOptions[i].id;
      }
    }
  };

  function getDegreeLabel(val) {
    for (var i = 0; i < degreeOptions.length; i++) {
      if (degreeOptions[i].id == val) {
        return [{
          id: val,
          name: degreeOptions[i].name,
          slug: degreeOptions[i].slug,
        }]
      }
    }
  }

  function getYogLabel(val) {
    for (var i = 0; i < yogOptions.length; i++) {
      if (yogOptions[i].id == val) {
        return [{
          id: val,
          name: yogOptions[i].name,
        }]
      }
    }
  }

  function onCollegeChangeHandler(val) {
    setClgName(val);
    if (val.length > 0) {
      setValidCollege(true);
    } else {
      setValidCollege(false);
    }
  }

  function onCGPAChangeHandler(val) {
    setCgpa(val);
    if (val.length > 0)
      setValidCGPA(true);
    else
      setValidCGPA(false);
  }

  function onSubmitPressHandler() {
    const variables = {
      candidate_id: user.id,
      degree_name: selectedDegree[0].slug,
      college_name: clgName,
      cgpa: cgpa,
      year_of_graduation: parseInt(selectedYog[0].name),
    };
    console.log('variavles', variables);
    insertCandidateEducationInfo(variables).then(
      details => {
        if (previousPage == 'Profile') {
          props.navigation.goBack();
        } else {
          if (state_index + 1 == onboarding_config.length) {
            insertCandidateStateTrack({ candidate_state_id: id, status: 'completed', current_action: current_action })
            props.navigation.navigation('Prepare');
          }
          props.navigation.navigate(onboardingUtils.getOnBoardingState(onboarding_config[state_index + 1]),
            {
              id: id,
              onboarding_config: onboarding_config,
              state_index: state_index + 1,
              current_action: current_action
            }
          );
        }
      },
      error => {
        alert(error);
      },
    );
  }

  HigherEducation.navigationOptions = {
    title: '7 Hires',
  };

  return (
    <ImageBackground
      source={require('../../../assets/onboarding_bg.png')}
      style={{ width: '100%', height: '100%', flex: 1 }}>
      <View style={styles.container}>
        {global.HermesInternal == null ? null : (
          <View style={styles.engine}>
            <Text style={styles.footer}>Engine: Hermes</Text>
          </View>
        )}
        <View style={styles.sectionTitle}>
          <Text style={styles.textTitle}>HigherEducation</Text>
        </View>
        <View style={styles.textInputLayoutContainer}>
          {/*
          degree
            */}
          <View style={styles.textInputContainer}>
            <TextInputLayout labelText={"Degree"}>
              <ItemsSelectTwo
                prefetchedValue={!isEmpty(selectedDegree) ? selectedDegree : null}
                isSelectSingle={true}
                style={{ borderRadius: 5, fontSize: 12, }}
                colorTheme={'#F14836'}
                popupTitle="Select Degree"
                title="Degree"
                cancelButtonText="Cancel"
                selectButtonText="Select"
                searchPlaceHolderText="Eg. BTech"
                data={degreeOptions}
                onSelect={data => {
                  setSelectedDegree(getDegreeLabel(data));
                  if (data.length > 0)
                    setValidDegree(true);
                  else
                    setValidDegree(false);
                }}
                onRemoveItem={data => {
                }}
              />
            </TextInputLayout>
          </View>
          {/*
college name
*/}
          <TextInputLayout style={styles.textInputContainer} labelText={"College Name"}>
            <TextInput
              defaultValue={user.candidate_bachelor_degree_info ? user.candidate_bachelor_degree_info.college_name : null}
              placeholder="College Name"
              style={styles.textInput}
              onChangeText={val => onCollegeChangeHandler(val)}
            />
          </TextInputLayout>
          {/*
cgpa
*/}
          <TextInputLayout style={styles.textInputContainer} labelText={"CGPA"}>
            <TextInput
              defaultValue={user.candidate_bachelor_degree_info ? user.candidate_bachelor_degree_info.cgpa.toString() : null}
              placeholder="CGPA"
              keyboardType={"number-pad"}
              style={styles.textInput}
              onChangeText={val => {
                onCGPAChangeHandler(val)
              }}
            />
          </TextInputLayout>
          {/*
            year of graduation
*/}
          <View style={styles.textInputContainer}>
            <TextInputLayout labelText={"Year of Graduation"}>
              <ItemsSelectTwo
                prefetchedValue={!isEmpty(selectedYog) ? selectedYog : null}
                isSelectSingle={true}
                style={{ borderRadius: 5, fontSize: 12, padding: 5 }}
                colorTheme={'#F14836'}
                popupTitle="Select Year"
                title="Year of Graduation"
                cancelButtonText="Cancel"
                selectButtonText="Select"
                searchPlaceHolderText="Eg. 2017"
                data={yogOptions}
                onSelect={data => {
                  setSelectedYOG(getYogLabel(data));
                  if (data.length > 0)
                    setValidYOG(true);
                  else
                    setValidYOG(false);
                }}
                onRemoveItem={data => {
                }}
              />
            </TextInputLayout>
          </View>

          {/*
submit button
*/}

        </View>
        <View style={styles.materialButton}>
          <Button
            disabled={
              !(isValidDegree &&
                isValidCollege &&
                isValidCGPA &&
                isValidYOG)
            }
            raised
            title={nextButtonName}
            buttonStyle={{ justifyContent: 'center', zIndex: 2, borderRadius: 6, height: 45, paddingHorizontal: 8, backgroundColor: '#f14836', }}
            titleStyle={{ fontWeight: '200', fontSize: 16, textAlignVertical: 'center' }}
            onPress={() => {
              onSubmitPressHandler();
            }}
          />
        </View>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    margin: 16,
  },
  textInputLayoutContainer: {
    flex: 5,
  },
  button: {
    position: 'absolute',
    bottom: 0,
    fontSize: 24,
  },
  materialButton: {
    position: 'absolute',
    bottom: 0,
    fontSize: 60,
    paddingHorizontal: 20,
    justifyContent: 'flex-end',
    width: '100%',
    flex: 1,
  },
  textInputContainer: {
    marginHorizontal: 20,
    marginTop: 20,
    // flexWrap: 'wrap'
  },
  textInput: {
    fontSize: 16,
    // width: "95%",
    height: 40,
    // borderWidth: 1,
    // borderColor: 'gray',
    // textAlign: 'center',
  },

  textTitle: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '900',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionTitle: {
    fontSize: 24,
    flex: 1,
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
