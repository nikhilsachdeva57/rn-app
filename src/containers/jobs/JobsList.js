import React, {Component, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator
} from 'react-native';
import JobListCard from '../../components/JobListCard';
import { useStoreState, useStoreActions } from 'easy-peasy';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {isEmpty} from 'lodash-es';
export default JobsList = (props) =>{

  const [loading, setLoading] = useState(true);
  const navigation = props.navigation;
  const candidate_jobs = useStoreState(state => state.user.candidate_jobs);
  const {
      getCandidateJobs
  } = useStoreActions(actions => actions.user);

  useEffect(() => {
    getCandidateJobs();
  },[])
  useEffect(() => {
    if(!isEmpty(candidate_jobs)) setLoading(false);
  })



  return (


    <ScrollView style={{padding: 10}}>

      {isEmpty(candidate_jobs.notice_message) ? (
          null
      ): (
        <View style={styles.infoView}>
               <Ionicons
      name="md-notifications-outline"
      size={20}
      style={styles.notificationLogo}/>
          <Text style={styles.noticeText}>
            {candidate_jobs.notice_message}
          </Text>
        </View>
      )}
    { loading ? (
      <ActivityIndicator size="large" color="#ee6002" />
    ): 
  (
       candidate_jobs.jobs ? (
         
        candidate_jobs.jobs.map((u,i) => {
          return (
          
            <JobListCard application_open={candidate_jobs.application_open} data={u}/>

          )
        })
       ) : (
        null
      )
      

 
  )}
    </ScrollView>
  );
};
const {height, width} = Dimensions.get('window');
const styles = StyleSheet.create({
  noticeText : {
    padding : 5, 
    fontWeight : "700",
    width : width*0.80
  },
  notificationLogo : {
    padding : 10,
    color : "#EF5350"
  },
  infoView : {
    flexDirection : "row",
    backgroundColor : "#BBDEFB",
    borderRadius : 10,
    elevation : 5,
    width : width*0.9
  },
  topText: {
    marginTop: 30,
    textAlign:"center",
    fontSize: 20,
    fontWeight: "700",
  },
  item : {
    margin : 10,
  }
});

