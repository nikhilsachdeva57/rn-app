{
  /* there are basically 6 type of questions
    fib - fill in the  blanks
    scq - single choice
    mcq
    av_response / audio_response
    boolean - true/false
    numeric
*/
}
import React, { useEffect, useState, Fragment } from 'react';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { get, isEmpty } from 'lodash-es';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import { RNS3 } from 'react-native-aws3';

import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import Quiz from '../../components/Quiz';
import { withNavigation } from 'react-navigation';
import QuestionLandingSubmit from '../../components/questionLanding/QuestionLandingSubmit';
import QuestionLandingNext from '../../components/questionLanding/QuestionLandingNext';

const QuestionLanding = props => {
  const {
    courseSlug,
    sectionSlug,
    quizSlug,
    questionSlug,
    attemptId,
  } = props.navigation.state.params;
  const quizData = useStoreState(state => state.quiz);
  const courseData = useStoreState(state => state.course);
  const allQuestionDetails = useStoreState(
    state => state.quiz.questionsDetails,
  );
  const {
    getQuizDetails,
    getQuestionDetails,
    updateQuizDetails,
    updateSelectedAnswers,
  } = useStoreActions(actions => actions.quiz);
  const { getCourseDetails } = useStoreActions(actions => actions.course);

  // check button clicked is for mapping the that the reponse of person is submitted or not
  // we have two back to back buttons in case of quiz ,
  // first button saves the response and second button that comes after clicking the first button
  // is to go to next question checkButtonClicked is for first button

  const [checkButtonClicked, setCheckButtonClicked] = useState(false);
  const [selectedAnswer, setSelectedAnswer] = useState([]);
  // for mcq only
  const [selectedOption, setSelectedOption] = useState([]);

  // const [stepByStepClicked, setStepByStepClicked] = useState(false);
  const [time, setTime] = useState(0);

  const nextQuestionSlug =
    quizData.questionSlugs[quizData.questionSlugs.indexOf(questionSlug) + 1];

  const activeQuestionDetails = allQuestionDetails[questionSlug];
  //TODO: remove this
  const options = {
    keyPrefix: 'av_response/',
    bucket: '7hires-document',
    region: 'ap-south-1',
    accessKey: 'AKIAI4EHKL4W56OS6EBQ',
    secretKey: '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',
    successActionStatus: 201,
    mode: 'opaque',
  };
  const resetState = () => {
    setSelectedAnswer([]);
    setCheckButtonClicked(false);
    // setStepByStepClicked(false);
    setTime(Date.now());
  };
  // lame old get every data before start
  useEffect(() => {
    if (isEmpty(quizData.id)) {
      getQuizDetails({ quizSlug });
    }
    if (courseSlug && isEmpty(courseData.id)) {
      getCourseDetails({ courseSlug });
    }
    if (isEmpty(activeQuestionDetails)) {
      getQuestionDetails({ questionSlug });
    }

    // get quiz start time to monitor reponses, async storage used instead of cookies,
    // no cookies concept in case of react-native

    async function getStartTime() {
      const startTime = await AsyncStorage.getItem('quizStartTime');
      return parseInt(startTime);
    }
    const startTime = getStartTime();
    updateQuizDetails({
      startTime: startTime,
    });
  }, []);

  useEffect(() => {
    if (isEmpty(quizData.id)) {
      getQuizDetails({ quizSlug });
    }
    if (isEmpty(activeQuestionDetails)) {
      getQuestionDetails({ questionSlug });
    }
    resetState();
    updateQuizDetails({
      currentQuestionIndex: quizData.questionSlugs.indexOf(questionSlug),
      currentSectionIndex: 0,
    });
  }, [questionSlug]);

  // onSubmit of answer, check what type of question was it,
  // for now in case of av_response as well as audio_response,
  // we show voice recorder, upload a file with link to s3
  // addon: there is no concept of correct / not_correct in case of some questions
  // also some mcqs have concept of correctwise and weightwise
  // correctwise are binary marking, weightvise weighted
  const handleCheckClicked = () => {
    const isAnyWrongAnswer = !isEmpty(
      selectedAnswer.filter(option => !option.isCorrect),
    );
    const correctOptionsLength = activeQuestionDetails.options.filter(
      option => option.isCorrect == true,
    ).length;
    const ifNotAllCorrectOptionsSelected =
      selectedAnswer.filter(answer => answer.isCorrect).length !=
      correctOptionsLength;
    const { answerContent } = selectedAnswer[0];
    if (
      activeQuestionDetails.formatId == 'av_response' ||
      activeQuestionDetails.formatId == 'audio_response'
    ) {
      console.log('selectedAnswerka', selectedAnswer);
      const filepath = selectedAnswer[0].audio_data;
      console.log('answerContent', answerContent);
      const fileToUpload = {
        uri: `file://${filepath}`,
        name: `${questionSlug}_${attemptId}.aac`,
        type: 'audio/aac',
      };
      RNS3.put(fileToUpload, options).then(
        response => {
          if (response.status == 201) {
            console.log('s3 response', response.body);
          }
        },
        error => {
          ToastAndroid.show(error, ToastAndroid.LONG);
          console.log('error', error);
        },
      );
    }

    setCheckButtonClicked(true);
    const endTime = Date.now() - time;
    setTime(endTime);
    const selectedAnswerFormatted = [];
    selectedAnswer.forEach(option => {
      const optionObj = {
        ...option,
        isCorrect: option.isCorrect,
        score: option.score,
      };
      delete optionObj.isCorrect;
      selectedAnswerFormatted.push(optionObj);
    });

    // an answer obj to be sent to sever

    const answerObj = {
      [questionSlug]: {
        answerContent,
        selectedOptions: selectedAnswerFormatted,
        stepsUsed: false,
        isCorrect: !isAnyWrongAnswer && !ifNotAllCorrectOptionsSelected,
      },
    };
    updateSelectedAnswers(answerObj);
  };

  // handleAnswerChanged function is passed as props to other components on case of changing answers,
  // whenever a user changes options, this function is called

  const handleAnswerChanged = answerObj => {
    const { formatId } = activeQuestionDetails;
    if (formatId == 'av_response' || formatId == 'audio_response') {
      answerObj[
        'location'
      ] = `https://7hires-document.s3.amazonaws.com/av_response/${questionSlug}_${attemptId}.aac`;
      const selectedAnswerList = [answerObj];
      setSelectedAnswer(selectedAnswerList);
    }
    if (!isEmpty(answerObj.answerContent)) {
      if (formatId === 'mcq') {
        let selectedAnswerList;
        let selectedOptionList = [];
        const answerExist = !isEmpty(
          selectedAnswer.filter(
            answer => answer.optionId === answerObj.optionId,
          ),
        );
        if (answerExist) {
          selectedAnswerList = selectedAnswer.filter(
            answer => answer.optionId !== answerObj.optionId,
          );
        } else {
          selectedAnswerList = [...selectedAnswer, answerObj];
        }
        selectedAnswerList.map(ans => {
          selectedOptionList.push(ans.optionId);
        });

        setSelectedOption(selectedOptionList);
        setSelectedAnswer(selectedAnswerList);
      }
      if (
        formatId === 'boolean' ||
        formatId === 'fib' ||
        formatId === 'numeric' ||
        formatId === 'scq'
      ) {
        const selectedAnswerList = [answerObj];
        setSelectedAnswer(selectedAnswerList);
      }
    }
  };

  if (isEmpty(quizData.id) || isEmpty(allQuestionDetails[questionSlug])) {
    return null;
  } else {
    // need to remove this code from here
    const activeQuestionIndex = quizData.questionSlugs.indexOf(questionSlug);
    const activeSectionIndex = quizData.sectionWiseQuestionSlugs
      .map((section, index) => {
        if (section.includes(questionSlug)) {
          return index;
        } else {
          return null;
        }
      })
      .find(val => val !== null);
    if (
      quizData.currentQuestionIndex !== activeQuestionIndex ||
      quizData.currentSectionIndex !== activeSectionIndex
    ) {
      updateQuizDetails({
        currentQuestionIndex: activeQuestionIndex,
        currentSectionIndex: activeSectionIndex,
        attemptId,
      });
    }
    if (nextQuestionSlug && isEmpty(allQuestionDetails[nextQuestionSlug])) {
      getQuestionDetails({ questionSlug: nextQuestionSlug });
    }
  }

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        style={{ margin: 20 }}
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: 'space-between',
        }}
        showsVerticalScrollIndicator={false}>
        <View style={{ flex: 3 }}>
          <Quiz
            questionSlug={questionSlug}
            checkButtonClicked={checkButtonClicked}
            selectedAnswer={selectedAnswer}
            selectedOption={selectedOption}
            handleAnswerChanged={handleAnswerChanged}
          />
        </View>
        <View style={{ flex: 0.5 }}>
          {checkButtonClicked ? (
            <Fragment>
              {/* if there is a question left in the quiz, show next button else show submit button */}
              {/* TouchableOpacity is the first button mentioned above in comments */}
              {isEmpty(nextQuestionSlug) ? (
                <QuestionLandingSubmit
                  attemptId={attemptId}
                  courseSlug={courseSlug}
                  sectionSlug={sectionSlug}
                  quizSlug={quizSlug}
                  questionSlug={questionSlug}
                  time={time}
                />
              ) : (
                  <QuestionLandingNext
                    attemptId={attemptId}
                    sectionSlug={sectionSlug}
                    quizSlug={quizSlug}
                    nextQuestionSlug={nextQuestionSlug}
                    questionSlug={questionSlug}
                    time={time}
                  />
                )}
            </Fragment>
          ) : (
              <TouchableOpacity
                disabled={isEmpty(selectedAnswer)}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={handleCheckClicked}>
                <View
                  style={[
                    !isEmpty(selectedAnswer)
                      ? styles.submitButton
                      : styles.disabledSubmitButton,
                  ]}>
                  <Text style={{ textAlign: 'center', color: 'white' }}>
                    {activeQuestionDetails.scoringType === 'correctwise'
                      ? 'Check'
                      : 'Submit'}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  submitButton: {
    color: 'white',
    margin: 20,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    width: 150,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#f14836',
    flex: 1,
  },
  disabledSubmitButton: {
    backgroundColor: '#f14836',
    borderColor: 'rgb(204, 202, 202)',
    color: 'rgb(158, 158, 158)',
    backgroundColor: 'rgb(237, 185, 185)',
    margin: 20,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    width: 150,
    height: 45,
    borderRadius: 25,
    flex: 1,
  },
});

export default withNavigation(QuestionLanding);
