// import fetchJSON from 'helpers/fetchJSON';
import {ApolloClient} from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {createHttpLink} from 'apollo-link-http';
import * as tokenUtils from '../utils/tokenUtils';
import {REACT_APP_GRAPHQL_URL} from 'react-native-dotenv';

//TODO: Remove this
const graphQlFetch = async (uri, options) => {
  let token = await tokenUtils.getToken();
  options.headers = options.headers || {};
  if (token) {
    options.headers.Authorization = `Bearer ${token}`;
    options.headers['x-hasura-role'] = 'candidate';
    options.headers['x-hasura-admin-secret'] = 'alliswell';
  }
  return fetch(uri, options);
};

export const graphQlClient = new ApolloClient({
  link: createHttpLink({
    fetch: graphQlFetch,
    uri: REACT_APP_GRAPHQL_URL,
  }),
  cache: new InMemoryCache(),
});
