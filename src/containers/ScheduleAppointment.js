{
  /* 
  datepicker format is YYYY-DD-MM, if you change the format
  it might lead to bugs in the code , adhere to the format if possible
  time format is hh:mmm 
  at the end , date and time is submitted to db in epoch, please make sure of that
*/
}
import React, { Component, useState, useEffect } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ToastAndroid,
  Animated,
  Alert,
  Dimensions,
  ImageBackground,
} from 'react-native';
import { isEmpty } from 'lodash-es';
import { useStoreState, useStoreActions } from 'easy-peasy';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import { slots } from '../utils/constants';
import ItemsSelectTwo from '../components/Onboarding/SelectTwoPicker/ItemsSelectTwo';

const ScheduleAppointment = props => {
  const { getCandidateStates } = useStoreActions(actions => actions.user);
  // no of days to be shown on the datepicker from the current date
  // here 2 denotes 2 days from today to be shown
  // TODO: brinng this data from server
  const [numberOfValidDays] = useState(3);

  const [appointmentDate, setAppointmentDate] = useState(null);
  const [appointmentSlot, setAppointmentSlot] = useState(null);
  // startTime and endTime of the appoointment scheduled
  const [startTime, setStartTime] = useState(0);
  const [endTime, setEndTime] = useState(0);
  const { insertCandidateStateTrack } = useStoreActions(actions => actions.user);

  const user = useStoreState(state => state.user);
  // duration of the meeting in  minutes
  // TODO: bring this data from database
  const [duration] = useState(30);
  const [animatedValue] = useState(new Animated.Value(0));
  const [value, setValue] = useState(0);
  const timeSlots = [];

  useEffect(() => {
    if (!isEmpty(user.id) && isEmpty(user.next_state)) {
      getCandidateStates({ candidate_id: user.id });
    }
  }, []);

  // name field given below as a hack for itemSelectTwo component, it won't work
  // with any other field, id & name field imp
  //itemSelecttwo is not  the ideal choice but for now it's a hack
  // making a timeSlots array as the data required by itemSelectTwo

  for (let i = 0; i < slots.length; i++) {
    let item = slots[i];
    let endTimeTemp = moment(item.startTime, 'HH:mm')
      .add(duration, 'minutes')
      .format('HH:mm');
    timeSlots.push({
      startTime: item.startTime,
      endTime: endTimeTemp,
      id: item.id,
      name: `${item.startTime} - ${endTimeTemp}`,
    });
  }

  // animation of flipping here

  animatedValue.addListener(({ value }) => {
    setValue(value);
  });
  const frontInterpolate = animatedValue.interpolate({
    inputRange: [0, 180],
    outputRange: ['0deg', '180deg'],
  });
  const backInterpolate = animatedValue.interpolate({
    inputRange: [0, 180],
    outputRange: ['180deg', '360deg'],
  });
  const frontOpacity = animatedValue.interpolate({
    inputRange: [89, 90],
    outputRange: [1, 0],
  });
  const backOpacity = animatedValue.interpolate({
    inputRange: [89, 90],
    outputRange: [0, 1],
  });

  const frontAnimatedStyle = {
    transform: [{ rotateY: frontInterpolate }],
  };
  const backAnimatedStyle = {
    transform: [{ rotateY: backInterpolate }],
  };

  const flipCard = () => {
    if (value >= 90) {
      Animated.spring(animatedValue, {
        toValue: 0,
      }).start();
    } else {
      Animated.spring(animatedValue, {
        toValue: 180,
      }).start();
    }
  };

  const showConfirmUploadAlert = () => {
    Alert.alert(
      'Confirm',
      'Appointment Date: ' +
      moment(appointmentDate, 'YYYY/DD/MM').format('YYYY/DD/MM') +
      '\n\nSlot: ' +
      startTime +
      '- ' +
      endTime,
      [
        { text: 'Yes', onPress: () => handleSubmit() },
        {
          text: 'No',
          onPress: () => {
            setStartTime(null);
            setEndTime(null);
            setAppointmentSlot(null);
          },
        },
      ],
      { cancelable: false },
    );
  };
  // function used in itemSelectTwo, sets appointment time

  const getTimeSlot = event => {
    for (let i = 0; i < timeSlots.length; i++) {
      if (slots[i].id == event) {
        setStartTime(timeSlots[i].startTime);
        setEndTime(timeSlots[i].endTime);
        return [
          {
            id: event,
            name: timeSlots[i].name,
          },
        ];
      }
    }
  };

  // submits the time in epoch format to server

  const handleSubmit = () => {
    let slotDate = moment(appointmentDate, 'YYYY/DD/MM').format('YYYY-DD-MM');
    const newAppointment = {
      slot_date: slotDate,
      slot_time: appointmentSlot,
    };

    const obj = {
      candidate_state_id: user.next_state.id,
      status: 'completed',
      next_candidate_state_id: user.upcoming_states[0]
        ? user.upcoming_states[0].id
        : null,
      current_action: 'mock_scheduling',
      start_time:
        parseInt(
          moment(`${slotDate} ${startTime}`, 'YYYY/DD/MM hh:mm').unix(),
        ) * 1000,

      end_time:
        parseInt(moment(`${slotDate} ${endTime}`, 'YYYY/DD/MM hh:mm').unix()) *
        1000,
    };
    // TODO: change navigation here
    insertCandidateStateTrack(obj).then(
      details => {
        console.log('redirect here');
        props.navigation.navigate('Prepare');
      },
      error => {
        console.log('Error in insert candidate state at submit', error);
        alert(error);
      },
    );
  };
  const DatePickerComponent = () => {
    const maxDate = moment(
      Date.now() + numberOfValidDays * 24 * 3600 * 1000,
    ).format('YYYY-DD-MM');
    return (
      <View
        style={{
          alignItems: 'center',
        }}>
        <DatePicker
          mode="date"
          placeholder="Select Date"
          date={!appointmentDate ? null : appointmentDate}
          format="YYYY-DD-MM"
          minDate={moment(Date.now() + 1 * 24 * 3600 * 1000)}
          maxDate={maxDate}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              height: 0,
              marginLeft: 0,
            },
            dateInput: {
              borderColor: 'rgb(240,240,240)',
              borderBottomColor: 'black',
              borderBottomWidth: 0.5,
            },
            dateText: { fontSize: 18 },
            placeholderText: {
              fontSize: 20,
              borderBottomColor: 'black',

              bottomBorder: 10,
            },
          }}
          onDateChange={date => {
            setAppointmentDate(moment(date, 'YYYY/DD/MM'));
          }}
        />
      </View>
    );
  };

  return (
    <ImageBackground
      source={require('../../assets/onboarding_bg.png')}
      style={{ width: '100%', height: '100%', flex: 1 }}>
      <View
        style={{
          flex: 1,
        }}>
        <View
          style={{
            flex: 1.5,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              flex: 1,
              justifyContent: 'center',
              fontSize: 25,
              textAlignVertical: 'center',
              textAlign: 'center',
              lineHeight: 25,
            }}>
            Schedule Mock Interview
          </Text>
        </View>

        <View style={{ flex: 3, justifyContent: 'flex-start' }}>
          {value < 90 && (
            <Animated.View
              style={[
                styles.flipCard,
                frontAnimatedStyle,
                { opacity: frontOpacity },
              ]}>
              {/* here flip card is used, one side has date picker and other has slot picker*/}
              <Text style={styles.flipText}>
                Choose an available day for your video mock/interview
              </Text>
              {DatePickerComponent()}
              <View
                style={{
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}>
                {appointmentDate ? (
                  <TouchableOpacity
                    onPress={() => {
                      flipCard();
                    }}>
                    <Text style={styles.nextButton}>NEXT</Text>
                  </TouchableOpacity>
                ) : (
                    <Text style={styles.nextButtonDisabled}>NEXT</Text>
                  )}
              </View>
            </Animated.View>
          )}
          {value >= 90 && (
            <Animated.View
              style={[
                styles.flipCard,
                styles.flipCardBack,
                backAnimatedStyle,
                { opacity: backOpacity },
              ]}>
              <Text style={styles.flipText}>
                Choose an available time for your video mock/interview
              </Text>
              <View
                style={{
                  justifyContent: 'center',
                  alignSelf: 'center',
                  borderBottomColor: 'black',
                  borderBottomWidth: 0.5,
                  width: '50%',
                }}>
                <ItemsSelectTwo
                  isSelectSingle={true}
                  prefetchedValue={appointmentSlot}
                  selectedTitleStyle={{
                    textAlign: 'center',
                    alignSelf: 'center',
                    color: 'rgba(0, 0, 0, 0.3)',
                    fontSize: 18,
                  }}
                  colorTheme={'#F14836'}
                  popupTitle="Select Time Slot"
                  title="Select Time Slot"
                  cancelButtonText="Cancel"
                  selectButtonText="Select"
                  searchPlaceHolderText={`${timeSlots[0].name}`}
                  data={timeSlots}
                  onSelect={data => {
                    setAppointmentSlot(getTimeSlot(data[0]));
                  }}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      flipCard();
                    }}>
                    <Text style={styles.nextButton}>BACK</Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>

                  <View>
                    {endTime ? (<TouchableOpacity onPress={showConfirmUploadAlert}>
                      <View>
                        <Text style={styles.nextButton}>SCHEDULE</Text>
                      </View>
                    </TouchableOpacity>) : null}
                  </View>

                </View>
              </View>
            </Animated.View>
          )}
        </View>
      </View>
    </ImageBackground>
  );
};
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  flipCard: {
    width: width - 40,
    height: 200,
    borderColor: '1 px solid rgba(0,0,0,.125)',
    backgroundColor: 'rgb(240,240,240)',
    alignSelf: 'center',
    backfaceVisibility: 'hidden',
    borderWidth: 0.5,
    borderRadius: 25,
    justifyContent: 'space-around',
  },
  flipCardBack: {},
  flipText: {
    fontSize: 16,
    marginTop: 20,
    textAlign: 'center',
    lineHeight: 20,
    margin: 4,
  },
  nextButton: {
    color: 'white',
    marginTop: 15,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    width: 120,
    height: 37,
    elevation: 5,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#f14836',
  },
  nextButtonDisabled: {
    marginTop: 15,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    width: 120,
    height: 37,
    elevation: 5,
    margin: 10,
    borderRadius: 10,
    backgroundColor: 'rgb(240,240,240)',
  },
});
export default ScheduleAppointment;
