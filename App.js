import React, {useEffect} from 'react';
import AppNavigator from './routes/AppNavigator';
import {ApolloProvider} from 'react-apollo';
import {Provider} from 'react-redux';
import {StoreProvider, useStoreActions, useStoreState} from 'easy-peasy';
import store from './src/store';
import * as apollo from './apollo';
import {isEmpty} from 'lodash-es';

function App() {
  const {getUserId} = useStoreActions(actions => actions.user);

  const user = useStoreState(state => state.user);
  const appConfig = useStoreState(state => state.appConfig);
  const {getAppIntroConfig} = useStoreActions(actions => actions.appConfig);

  useEffect(() => {
    if (isEmpty(appConfig) && isEmpty(user)) {
      getAppIntroConfig();
    }
    getUserId();
  }, []);

  const client = apollo.create();
  return <AppNavigator />;
}

export default App;
