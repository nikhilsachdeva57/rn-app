import React, {Component, useState, useEffect} from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Platform,
  ToastAndroid,
  Dimensions,
} from 'react-native';
import LottieView from 'lottie-react-native';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import Sound from 'react-native-sound';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
const AudioResponse = props => {
  // IMP: react-native-audio has debugging errors, close debugging and then test the app in development, it will work
  let animate = null;
  const [currentTime, setCurrentTime] = useState(props.currentTime);
  const [recording, setRecording] = useState(props.recording);
  const [stoppedRecording, setStoppedRecording] = useState(props.stopRecording);
  const [finished, setFinished] = useState(props.finished);
  const [audioPath, setAudioPath] = useState(
    AudioUtils.DocumentDirectoryPath + '/recording.aac',
  );
  const [answerObject, setAnswerObject] = useState({});
  const [recordTime, setRecordTime] = useState(props.recordTime);
  const [hasPermission, setHasPermission] = useState(undefined);
  const [attempts, setAttempts] = useState(props.numberOfAttempts);

  const prepareRecordingPath = audioPath => {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: 'Low',
      AudioEncoding: 'aac',
      AudioEncodingBitRate: 32000,
    });
  };

  useEffect(() => {
    AudioRecorder.requestAuthorization().then(isAuthorised => {
      setHasPermission(isAuthorised);

      if (!isAuthorised) return;

      prepareRecordingPath(audioPath);

      AudioRecorder.onProgress = data => {
        //   this.setState({currentTime: Math.floor(data.currentTime)});
        setCurrentTime(Math.floor(data.currentTime));
      };

      AudioRecorder.onFinished = data => {
        // Android callback comes in the form of a promise instead.
        if (Platform.OS === 'ios') {
          _finishRecording(
            data.status === 'OK',
            data.audioFileURL,
            data.audioFileSize,
          );
        }
      };
    });
  }, []);
  useEffect(() => {
    // returned function will be called on component unmount
    return () => {
      AudioRecorder.stopRecording();
      AudioRecorder.onFinished();
    };
  }, []);
  useEffect(() => {
    if (currentTime >= recordTime) {
      _stop();
    }
  }, [currentTime]);
  useEffect(() => {
    setRecordTime(props.recordTime);
    setAttempts(0);
    setCurrentTime(props.currentTime);
  }, [props.checkButtonClicked]);
  const _renderButton = (title, onPress, active) => {
    var style = active ? styles.activeButtonText : styles.buttonText;

    return (
      <TouchableHighlight style={styles.button} onPress={onPress}>
        <MaterialCommunityIcons name="play" />
      </TouchableHighlight>
    );
  };

  const _stop = async () => {
    // animate reset of lotte-gif
    animate.reset();
    if (props.checkButtonClicked) {
      ToastAndroid.show('Audio response already submitted', ToastAndroid.SHORT);
      return;
    }
    if (!recording) {
      console.warn("Can't stop, not recording!");
      return;
    }

    setStoppedRecording(true);
    setRecording(false);

    try {
      const filePath = await AudioRecorder.stopRecording();

      if (Platform.OS === 'android') {
        _finishRecording(true, filePath);
      }

      return filePath;
    } catch (error) {
      console.error(error);
      ToastAndroid.show(error, ToastAndroid.SHORT);
    }
  };

  const _play = async () => {
    if (props.checkButtonClicked) {
      ToastAndroid.show('Audio response already submitted', ToastAndroid.SHORT);
      return;
    }
    if (recording) {
      await _stop();
    }

    // These timeouts are a hacky workaround for some issues with react-native-sound.
    // See https://github.com/zmxv/react-native-sound/issues/89.
    setTimeout(() => {
      var sound = new Sound(audioPath, '', error => {
        if (!error) {
        }
      });

      setTimeout(() => {
        sound.play(success => {
          if (success) {
            console.log('successfully finished playing');
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      }, 100);
    }, 100);
  };

  const _record = async () => {
    if (recording) {
      console.warn('Already recording!');
      return;
    }
    if (props.checkButtonClicked) {
      ToastAndroid.show('Audio response already submitted', ToastAndroid.SHORT);
      return;
    }
    if (!hasPermission) {
      console.warn("Can't record, no permission granted!");
      return;
    }
    // if (attempts == 0) {
    //   ToastAndroid.show(
    //     'You have exceeded the number of attempts',
    //     ToastAndroid.SHORT,
    //   );
    //   return;
    // }
    // ToastAndroid.show(
    //   `Number of attempts left ${attempts - 1}`,
    //   ToastAndroid.SHORT,
    // );
    animate.play(0, 120);
    if (stoppedRecording) {
      prepareRecordingPath(audioPath);
    }

    setRecording(true);
    setAttempts(attempts + 1);
    try {
      const filePath = await AudioRecorder.startRecording();
      // setAttempts(attempts - 1);
    } catch (error) {
      console.error(error);
    }
  };

  const _finishRecording = (didSucceed, filePath, fileSize) => {
    setFinished(didSucceed);

    console.log(
      `Finished recording of duration ${currentTime} seconds at path: ${filePath} and size of ${fileSize ||
        0} bytes, with attempts ${attempts}`,
    );
    const answerObj = {
      optionId: 0,
      audio_data: filePath,
      answerContent: '',
      attempt_count: attempts,
      is_correct: false,
    };
    setAnswerObject(answerObj);
    if (!props.checkButtonClicked) {
      props.handleAnswerChanged(answerObj);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.timer}>
        <AnimatedCircularProgress
          size={100}
          width={10}
          fill={(parseInt(currentTime) / recordTime) * 100}
          tintColor="#f14836"
          backgroundColor="#080042"
          tintColorSecondary="#6bff54">
          {fill => (
            <Text style={{fontSize: 24}}>
              {recordTime - parseInt(currentTime)}
            </Text>
          )}
        </AnimatedCircularProgress>
      </View>
      <View style={styles.lottieView}>
        <LottieView
          ref={animation => {
            animate = animation;
          }}
          source={require('../../../assets/audio_recording.json')}
        />
      </View>
      <View style={styles.controls}>
        {!recording && attempts > 0 && (
          <TouchableHighlight style={styles.button} onPress={_play}>
            <MaterialCommunityIcons style={{fontSize: 40}} name="play" />
          </TouchableHighlight>
        )}

        {recording ? (
          <TouchableHighlight style={styles.button} onPress={_stop}>
            <Feather style={{fontSize: 40}} name="mic-off" />
          </TouchableHighlight>
        ) : (
          <TouchableHighlight style={styles.button} onPress={_record}>
            <Feather style={{fontSize: 40}} name="mic" />
          </TouchableHighlight>
        )}

        {/* {this._renderButton("PAUSE", () => {this._pause()} )} */}
      </View>
      {attempts > 0 && !recording && (
        <Text style={{textAlign: 'center', margin: 10}}>Retry</Text>
      )}
    </View>
  );
};
const {width, height} = Dimensions.get('window');
var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
  },
  controls: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
  },

  timer: {
    marginTop: 20,
    alignSelf: 'center',
    flex: 1,
  },
  disabledButtonText: {
    color: '#eee',
  },
  buttonText: {
    fontSize: 20,
    color: '#fff',
  },
  activeButtonText: {
    fontSize: 20,
    color: '#B81F00',
  },
  lottieView: {
    height: height / 6,
    width: width,
    alignSelf: 'center',
    flex: 10,
  },
});

export default AudioResponse;
