export {getQuizDetails, insertQuizTrack, updateQuizTrack} from './Quiz';
export {getQuestionDetails, insertQuestionTrack} from './Question';
export {
  getSectionDetails,
  getSectionList,
  candidateCourseTracks,
  insertCourseTrack,
} from '../restApi/course';
//export { GET_COURSE_UPCOMING_COMPLETED_CLASS } from '../restApi/classRoom';
// export {
//   GET_VIDEO,
//   GET_GAME,
//   GET_ACTIVITY,
//   GET_SELF_PACED_LESSON,
// } from './content';
// export { GET_CLASSES } from './home';
// export { GET_HOMEWORK } from './homework';
// export {
//   GET_CHAPTERS,
//   GET_CHAPTER_CONTENT,
// } from './library';
// export {
//   GET_PROFILE,
//   GET_AVATARS,
//   SET_PROFILE,
// } from '../restApi/profile';
// export { GET_QUESTION_BY_SLUG, INSERT_QUESTION_TRACK } from '../restApi/question';
// export { GET_QUIZ_BY_SLUG, INSERT_QUIZ_TRACK, UPDATE_QUIZ_TRACK } from '../restApi/quiz';

export {
  GET_NOTICE_PERIOD,
  GET_ENUMS_DEGREE,
  GET_APP_INTRO_CONFIG,
  GET_CANDIDATE_STATES,
  INSERT_CANDIDATE_BASIC_INFO,
  GET_CITIES,
  INSERT_CANDIDATE_EDUCATION_INFO,
  INSERT_CANDIDATE_12TH_INFO,
  INSERT_CANDIDATE_10TH_INFO,
  INSERT_CANDIDATE_CURRENT_EMPLOYMENT_INFO,
  INSERT_CANDIDATE_WORK_EXPERIENCE_INFO,
  INSERT_CANDIDATE_PREFERENCE_INFO,
  INSERT_CANDIDATE_INTERNSHIP_INFO,
  INSERT_CANDIDATE_STATE_TRACK,
  GET_CANDIDATE_STATE_BY_ID,
  GET_CANDIDATE_BASIC_INFO,
  GET_CANDIDATE_EDUCATION_INFO,
  GET_CANDIDATE_12TH_INFO,
  GET_CANDIDATE_10TH_INFO,
  GET_CANDIDATE_CURRENT_EMPLOYMENT_INFO,
  GET_CANDIDATE_WORK_EXPERIENCE_INFO,
  GET_CANDIDATE_PREFERENCE_INFO,
  GET_CANDIDATE_INTERNSHIP_INFO,
  GET_PROFILE_INFO,
  GET_WORK_EXPERIENCE,
  GET_INTERNSHIP_DETAILS,
  getCandidateJobs
} from '../restApi/user';
