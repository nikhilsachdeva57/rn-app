import { createStackNavigator, createAppContainer } from "react-navigation";
import Profile from '../src/containers/Profile/Profile'
import HigherEducation from "../src/containers/Onboarding/HigherEducation";
import BasicInfo from "../src/containers/Onboarding/BasicInfo";
import WorkExperience from "../src/containers/Onboarding/WorkExperience";
import Preferences from "../src/containers/Onboarding/Preferences";
import OnBoardingNavigator from '../routes/OnBoardingNavigator'
import CVUpload from '../src/containers/Profile/CVUpload'
const ProfileNavigator = createStackNavigator({
    Profile,
    CVUpload,
    BasicInfo,
    HigherEducation,
    WorkExperience,
    Preferences,  
});

export default createAppContainer(ProfileNavigator);