import {graphQlClient} from '../helpers/graphQlAPIClient';
import {
  GET_NOTICE_PERIOD,
  GET_ENUMS_DEGREE,
  GET_APP_INTRO_CONFIG,
  GET_CANDIDATE_RESUME,
  INSERT_CANDIDATE_RESUME,
  GET_CANDIDATE_STATES,
  INSERT_CANDIDATE_BASIC_INFO,
  INSERT_CANDIDATE_EDUCATION_INFO,
  INSERT_CANDIDATE_12TH_INFO,
  INSERT_CANDIDATE_10TH_INFO,
  INSERT_CANDIDATE_CURRENT_EMPLOYMENT_INFO,
  INSERT_CANDIDATE_WORK_EXPERIENCE_INFO,
  INSERT_CANDIDATE_PREFERENCE_INFO,
  INSERT_CANDIDATE_INTERNSHIP_INFO,
  GET_USER_MOBILE,
  GET_CITIES,
  GET_ROLES,
  GET_INDUSTRIES,
  GET_CTC_RANGE,
  GET_CANDIDATE_BASIC_INFO,
  INSERT_CANDIDATE_STATE_TRACK,
  GET_CANDIDATE_STATE_BY_ID,
  GET_CANDIDATE_EDUCATION_INFO,
  GET_CANDIDATE_12TH_INFO,
  GET_CANDIDATE_10TH_INFO,
  GET_CANDIDATE_CURRENT_EMPLOYMENT_INFO,
  GET_CANDIDATE_WORK_EXPERIENCE_INFO,
  GET_CANDIDATE_PREFERENCE_INFO,
  GET_CANDIDATE_INTERNSHIP_INFO,
  GET_PROFILE_INFO,
  GET_WORK_EXPERIENCE,
  GET_INTERNSHIP_DETAILS,
  GET_CANDIDATE_JOBS,
  ASSIGN_JOB,
} from '../helpers/queries';
import {GET_APP_VERSION_CONFIG} from '../helpers/queries/user';

export function getUserMobile(id) {
  return graphQlClient.query({
    query: GET_USER_MOBILE,
    variables: {
      id,
    },
  });
}

export function getCandidateStates(candidate_id) {
  console.log('payload getcandid,', candidate_id);
  return graphQlClient.query({
    query: GET_CANDIDATE_STATES,
    fetchPolicy: 'no-cache',
    variables: {
      candidate_id,
    },
  });
}

export function getDegree(variables){
  return graphQlClient.query({
    query:GET_ENUMS_DEGREE,
    variables,
  })
}

export function getNoticePeriod(variables){
  return graphQlClient.query({
    query:GET_NOTICE_PERIOD,
    variables,
  })
}

export function insertCandidateBasicInfo(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_BASIC_INFO,
    variables,
  });
}
export function getCandidateBasicInfo(candidate_id) {
  return graphQlClient.mutate({
    mutation: GET_CANDIDATE_BASIC_INFO,
    variables: {
      candidate_id,
    },
  });
}

export function insertCandidateEducationInfo(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_EDUCATION_INFO,
    variables,
  });
}

export function insertCandidateResume(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_RESUME,
    variables,
  });
}
export function getCandidateResume(candidate_id) {
  return graphQlClient.query({
    query: GET_CANDIDATE_RESUME,
    variables: {
      candidate_id,
    },
  });
}
export function getAppIntroConfig(variables) {
  return graphQlClient.query({
    query: GET_APP_INTRO_CONFIG,
    variables,
  });
}

export function getAppVerionConfig(variables) {
  return graphQlClient.query({
    query: GET_APP_VERSION_CONFIG,
    variables,
  });
}

export function getCandidateEducationInfo(candidate_id) {
  return graphQlClient.query({
    query: GET_CANDIDATE_EDUCATION_INFO,
    variables: {
      candidate_id,
    },
  });
}

export function insertCandidate12thInfo(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_12TH_INFO,
    variables,
  });
}

export function getCandidate12thInfo(candidate_id) {
  return graphQlClient.query({
    query: GET_CANDIDATE_12TH_INFO,
    variables: {
      candidate_id,
    },
  });
}

export function insertCandidate10thInfo(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_10TH_INFO,
    variables,
  });
}

export function getCandidate10thInfo(candidate_id) {
  return graphQlClient.query({
    query: GET_CANDIDATE_10TH_INFO,
    variables: {
      candidate_id,
    },
  });
}

export function insertCandidateCurrentEmploymentInfo(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_CURRENT_EMPLOYMENT_INFO,
    variables,
  });
}

export function getCandidateCurrentEmploymentInfo(candidate_id) {
  return graphQlClient.query({
    query: GET_CANDIDATE_CURRENT_EMPLOYMENT_INFO,
    fetchPolicy: 'no-cache',
    variables: {
      candidate_id,
    },
  });
}

export function insertCandidateWorkExperienceInfo(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_WORK_EXPERIENCE_INFO,
    variables,
  });
}

export function getCandidateWorkExperienceInfo(work_ex_id) {
  return graphQlClient.query({
    query: GET_CANDIDATE_WORK_EXPERIENCE_INFO,
    variables: {
      work_ex_id,
    },
  });
}

export function insertCandidatePreferenceInfo(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_PREFERENCE_INFO,
    variables,
  });
}

export function getCandidatePreferenceInfo(candidate_id) {
  return graphQlClient.query({
    query: GET_CANDIDATE_PREFERENCE_INFO,
    variables: {
      candidate_id,
    },
  });
}

export function insertCandidateInternshipInfo(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_INTERNSHIP_INFO,
    variables,
  });
}

export function getCandidateInternshipInfo(candidate_id) {
  return graphQlClient.query({
    query: GET_CANDIDATE_INTERNSHIP_INFO,
    variables: {
      candidate_id,
    },
  });
}

export function getRoles(variables) {
  return graphQlClient.query({
    query: GET_ROLES,
    variables,
  });
}

export function getCtcRange(variables) {
  return graphQlClient.query({
    query: GET_CTC_RANGE,
    variables,
  });
}

export function getIndustries(variables) {
  return graphQlClient.query({
    query: GET_INDUSTRIES,
    variables,
  });
}

export function getCities(variables) {
  return graphQlClient.query({
    query: GET_CITIES,
    variables,
  });
}

export function getProfileInfo(candidate_id) {
  return graphQlClient.query({
    query: GET_PROFILE_INFO,
    variables: {
      candidate_id,
    },
  });
}

export function getWorkExperience(id) {
  return graphQlClient.query({
    query: GET_WORK_EXPERIENCE,
    variables: {
      id,
    },
  });
}

export function getInternshipDetail(id) {
  return graphQlClient.mutate({
    mutation: GET_INTERNSHIP_DETAILS,
    variables: {
      id,
    },
  });
}

export function getCandidateStateById(id) {
  return graphQlClient.query({
    query: GET_CANDIDATE_STATE_BY_ID,
    variables: {
      id,
    },
  });
}

export function insertCandidateStateTrack(
  candidate_state_id,
  status,
  next_candidate_state_id,
  current_action,
  start_time,
  end_time,
) {
  console.log(candidate_state_id);
  return graphQlClient.mutate({
    mutation: INSERT_CANDIDATE_STATE_TRACK,
    variables: {
      candidate_state_id,
      status,
      next_candidate_state_id,
      current_action,
      start_time,
      end_time,
    },
  });
}

export function getCandidateJobs(variables) {
  return graphQlClient.query({
    query: GET_CANDIDATE_JOBS,
    fetchPolicy: 'no-cache',
    variables,
  });
}

export function assignJob(candidateId, jobId) {
  return graphQlClient.mutate({
    mutation: ASSIGN_JOB,
    variables: {
      candidateId,
      jobId,
    },
  });
}
