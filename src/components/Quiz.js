import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Modal from 'react-native-modal';
import {useStoreState, useStoreActions} from 'easy-peasy';
import {formatNameMapping} from '../utils/constants';
import AnswerOptions from '../components/questions/AnswerOptions';
import Entypo from 'react-native-vector-icons/Entypo';

export default function Quiz(props) {
  Quiz.navigationOptions = {
    title: 'Quiz',
  };
  const quizData = useStoreState(state => state.quiz);
  const {questionSlug} = props;
  const questionData = quizData.questionsDetails[questionSlug];
  const counter = quizData.currentQuestionIndex + 1;
  const [hidden, setHidden] = useState(false);
  useEffect(() => {
    setHidden(false);
  }, [questionSlug]);

  return (
    <ScrollView
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'center',
      }}>
      <View style={{flex: 0.5, flexDirection: 'column'}}>
        <Text
          style={{
            flex: 1,
            justifyContent: 'center',
            textAlignVertical: 'bottom',

            textAlign: 'center',
            fontSize: 14,
            alignSelf: 'center',
          }}>
          {formatNameMapping[questionData.formatId]}
        </Text>
      </View>

      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          marginVertical: 12,
        }}>
        <Text
          style={{
            flex: 0.07,
            justifyContent: 'center',
            fontSize: 18,
            textAlign: 'left',
            lineHeight: 25,
          }}>
          {counter}.
        </Text>
        <Text
          style={{
            flex: 1,
            justifyContent: 'center',
            fontSize: 18,
            textAlign: 'left',
            lineHeight: 25,
          }}>
          {questionData.text}
        </Text>
      </View>

      <View>
        {/* TODO: convert questionData.text to markdown */}

        <TouchableOpacity
          onPress={() => {
            {
              setHidden(!hidden);
            }
          }}>
          {questionData.hint != null && (
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                marginEnd: 20,
                marginBottom: 8,
              }}>
              <View style={{alignItems: 'flex-end', flex: 1}}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                  }}>
                  <Entypo
                    name="info-with-circle"
                    style={{
                      color: '#F14836',
                      fontSize: 20,
                      alignSelf: 'flex-end',
                    }}
                  />
                  <Text style={{textAlign: 'right'}}> Hint</Text>
                </View>
              </View>
            </View>
          )}
        </TouchableOpacity>
      </View>
      <View>
        <Modal
          style={{
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          isVisible={hidden}
          backdropOpacity={0.9}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={600}
          animationOutTiming={600}
          backdropTransitionInTiming={600}
          backdropTransitionOutTiming={600}
          supportedOrientations={['portrait', 'landscape']}>
          <TouchableOpacity
            onPress={() => {
              setHidden(!hidden);
            }}>
            <View style={styles.popUp}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  margin: 20,
                }}>
                <Text style={{margin: 20, fontSize: 20, lineHeight: 25}}>
                  {questionData.hint}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </Modal>
      </View>
      <View style={{flex: 6, justifyContent: 'center'}}>
        <AnswerOptions
          options={questionData.options}
          handleAnswerChanged={props.handleAnswerChanged}
          formatSlug={questionData.formatId}
          selectedAnswer={props.selectedAnswer}
          selectedOption={props.selectedOption}
          checkButtonClicked={props.checkButtonClicked}
          scoringType={questionData.scoringType}
        />
      </View>
    </ScrollView>
  );
}
const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  submit: {
    marginTop: 12,
    borderWidth: 1,
    borderColor: '#fff',
    flex: 1,
    width: width / 3,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#F14836',
    justifyContent: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
  },
  popUp: {
    marginTop: 30,
    marginBottom: 30,
    marginVertical: 12,
    borderRadius: 20,
    borderWidth: 0.5,
    backgroundColor: '#ede5da',
    borderColor: '#DDDDDD',
  },
});
