import React from 'react';
import {View} from 'react-native';
import MultipleChoice from './MultipleChoice';
import TrueFalse from './TrueFalse';
import FillintheBlanks from './FillintheBlanks';
import SingleChoice from './SingleChoice';
import AudioResponse from './AudioResponse';

function AnswerOptions(props) {
  if (props.formatSlug === 'mcq') {
    return props.options.map((option, index) => {
      const answerType = String.fromCharCode(65 + index);
      return (
        <View>
          <MultipleChoice
            key={index}
            checkButtonClicked={props.checkButtonClicked}
            handleAnswerChanged={props.handleAnswerChanged}
            answerContent={option.text}
            answerScore={option.score}
            optionId={option.id}
            answerType={answerType}
            is_correct={option.is_correct}
            selectedAnswer={props.selectedAnswer}
            selectedOption={props.selectedOption}
          />
        </View>
      );
    });
  }
  if (props.formatSlug === 'boolean') {
    return props.options.map((option, index) => {
      const answerType = String.fromCharCode(65 + index);
      return (
        <View>
          <TrueFalse
            key={index}
            optionId={option.id}
            checkButtonClicked={props.checkButtonClicked}
            handleAnswerChanged={props.handleAnswerChanged}
            answerContent={option.text}
            answerScore={option.score}
            answerType={answerType}
            is_correct={option.is_correct}
            selectedAnswer={props.selectedAnswer}
          />
        </View>
      );
    });
  }
  if (props.formatSlug === 'fib' || props.formatSlug === 'numeric') {
    return (
      <View>
        <FillintheBlanks
          handleAnswerChanged={props.handleAnswerChanged}
          options={props.options}
          checkButtonClicked={props.checkButtonClicked}
          selectedAnswer={props.selectedAnswer}
        />
      </View>
    );
  }
  if (props.formatSlug === 'scq') {
    return props.options.map((option, index) => {
      const answerType = String.fromCharCode(65 + index);

      return (
        <View>
          <SingleChoice
            key={index}
            handleAnswerChanged={props.handleAnswerChanged}
            answerContent={option.text}
            answerType={answerType}
            answerScore={option.score}
            optionId={option.id}
            isCorrect={option.isCorrect}
            checkButtonClicked={props.checkButtonClicked}
            selectedAnswer={props.selectedAnswer}
            scoringType={props.scoringType}
          />
        </View>
      );
    });
  }

  if (
    props.formatSlug === 'av_response' ||
    props.formatSlug === 'audio_response'
  ) {
    return (
      <AudioResponse
        handleAnswerChanged={props.handleAnswerChanged}
        checkButtonClicked={props.checkButtonClicked}
        selectedAnswer={props.selectedAnswer}
        numberOfAttempts={0}
        recordTime={60}
        currentTime={0.0}
        finished={false}
        stopRecording={false}
        recording={false}
      />
    );
  } else {
    return <View></View>;
  }
}

export default AnswerOptions;
