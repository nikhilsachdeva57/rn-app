import React, { Component, useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TextInput,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  ToastAndroid,
  StatusBar,
  AsyncStorage,
  Alert,
  ImageBackground,
  Modal,
} from 'react-native';
import { isEmpty } from 'lodash-es';
import { TextInputLayout } from 'rn-textinputlayout';
import { Button } from 'react-native-elements';
import { useStoreState, useStoreActions } from 'easy-peasy';
import RadioSelectOption from '../../components/Onboarding/RadioSelectOption';
import ItemsSelectTwo from '../../components/Onboarding/SelectTwoPicker/ItemsSelectTwo'
import * as onboardingUtils from '../../utils/onboardingUtils'
import DatePicker from 'react-native-datepicker';


export default function WorkExperience(props) {
  const user = useStoreState(state => state.user);

  const [isValidCurrentCompany, setValidCurrentCompany] = useState(false);
  const [isValidCurrentRole, setValidCurrentRole] = useState(false);
  const [isValidCurrentCTC, setValidCurrentCTC] = useState(false);
  const [isValidNoticePeriod, setValidNoticePeriod] = useState(false);
  const [isValidCurrentWorkExp, setValidCurrentWorkExp] = useState(false)
  const [isValidCurrentDate, setValidCurrentDate] = useState(false);

  const [isValidPastCompany, setValidPastCompany] = useState(false);
  const [isValidPastRole, setValidPastRole] = useState(false);
  const [isValidPastCTC, setValidPastCTC] = useState(false);
  const [isValidPastWorkExp, setValidPastWorkExp] = useState(false)
  const [isValidPastStartDate, setValidPastStartDate] = useState(false);
  const [isValidPastEndDate, setValidPastEndDate] = useState(false);

  const [isEmployed, setEmployed] = useState(true);
  const [isMorePastExp, setMorePastExp] = useState(false);

  const [selectedCurrentCompany, setSelectedCurrentCompany] = useState('');
  const [selectedPastCompany, setSelectedPastCompany] = useState('');
  const [selectedCurrentRole, setSelectedCurrentRole] = useState('');
  const [selectedPastRole, setSelectedPastRole] = useState('');
  const [selectedCurrentCTC, setSelectedCurrentCTC] = useState('');
  const [selectedPastCTC, setSelectedPastCTC] = useState('');
  const [workExpCurrent, setWorkExpCurrent] = useState(0);
  const [workExpPast, setWorkExpPast] = useState(0);
  const [noticePeriod, setNoticePeriod] = useState(0);
  const [selectedCurrentStartDate, setSelectedCurrentStartDate] = useState('');
  const [selectedPastStartDate, setSelectedPastStartDate] = useState('');
  const [selectedPastEndDate, setSelectedPastEndDate] = useState('');
  const [isNewUser, setNewUser] = useState(true);
  const [isEnumFlag, setEnumFlag] = useState(false);
  const {
    insertCandidateWorkExperienceInfo,
    getCandidateWorkExperienceInfo,
    getRoles,
    getCtcRange,
    getCandidateCurrentEmploymentInfo,
    insertCandidateCurrentEmploymentInfo,
    insertCandidateStateTrack,
    getNoticePeriod,
  } = useStoreActions(actions => actions.user);
  const { id, onboarding_config, state_index, current_action } = props.navigation.state.params;

  let nextButtonName = 'Save';
  const previousPage = props.navigation.state.params.from;
  useEffect(() => {
    return () => getCandidateCurrentEmploymentInfo(user.id);
  }, []);
  useEffect(() => {
    return () => getCandidateWorkExperienceInfo(user.id);
  }, []);
  useEffect(() => {
    if (previousPage != 'Profile') {
      nextButtonName = 'Next';
    }

    if (isNewUser) {
      if (!isEmpty(user) && isEmpty(user.candidate_current_work_info) &&
        isEmployed) {
        getCandidateCurrentEmploymentInfo(user.id);
      }

      if (!isEmpty(user) && isEmpty(user.candidate_work_experience_info) && !isEmployed) {
        getCandidateWorkExperienceInfo(user.id);
      }
      setNewUser(false);
    }
    if (!isEnumFlag) {
      if (!isEmpty(user) && isEmpty(user.enums_notice_period)) {
        getNoticePeriod();
      }
      if (isEmpty(user.enums_job_roles)) {
        getRoles();
      }
      if (isEmpty(user.enums_ctc_range)) {
        getCtcRange();
      }
      setEnumFlag(true);
    }
    if (isEmployed && !isEmpty(user.candidate_current_work_info)) {
      if (isEmpty(selectedCurrentRole)) {
        setValidCurrentRole(true);
        setSelectedCurrentRole(getRoleObject(user.candidate_current_work_info.current_role));
      }
      if (isEmpty(selectedCurrentCompany)) {
        setValidCurrentCompany(true);
        setSelectedCurrentCompany(user.candidate_current_work_info.current_company);
      }
      if (isEmpty(selectedCurrentCTC)) {
        setValidCurrentCTC(true);
        setSelectedCurrentCTC(getCtcRangeObject(user.candidate_current_work_info.current_ctc))
      }

      if (isEmpty(workExpCurrent)) {
        setWorkExpCurrent(user.candidate_current_work_info.total_work_experience_years.toString());
        setValidCurrentWorkExp(true);
      }
      if (
        isEmpty(selectedCurrentStartDate)
      ) {
        setSelectedCurrentStartDate(user.candidate_current_work_info.date_of_joining);
        setValidCurrentDate(true);
      }
    }

    if (!isMorePastExp && !isEmployed && !isEmpty(user.candidate_work_experience_info)) {
      if (isEmpty(selectedPastRole)) {
        setValidPastRole(true);
        setSelectedPastRole(getRoleObject(user.candidate_work_experience_info.role));
      }
      if (isEmpty(selectedPastCompany)) {
        setValidPastCompany(true);
        setSelectedPastCompany(user.candidate_work_experience_info.company_name);
      }
      if (isEmpty(selectedPastCTC)) {
        setValidPastCTC(true);
        setSelectedPastCTC(getCtcRangeObject(user.candidate_work_experience_info.company_ctc))
      }

      if (isEmpty(workExpPast)) {
        setWorkExpPast(user.candidate_work_experience_info.total_work_experience_years);
        setValidPastWorkExp(true);
      }
      if (
        isEmpty(selectedPastStartDate) &&
        !isEmpty(user.candidate_work_experience_info)
      ) {
        setValidPastStartDate(true);
        setSelectedPastStartDate(user.candidate_work_experience_info.start_date);
      }
      if (
        isEmpty(selectedPastEndDate)
      ) {
        setValidPastEndDate(true);
        setSelectedPastEndDate(user.candidate_work_experience_info.end_date);
      }
    }

    if (isEmpty(noticePeriod) && isEmployed && !isEmpty(user) && !isEmpty(user.candidate_current_work_info)) {
      setNoticePeriod(getNoticePeriodObject(user.candidate_current_work_info.notice_period));
      setValidNoticePeriod(true);
    }
  });

  function getNoticePeriodObject(value) {
    for (var i = 0; i < noticePeriodOptions.length; i++) {
      if (noticePeriodOptions[i].day_count == value) {
        return [{
          id: noticePeriodOptions[i].id,
          day_count: noticePeriodOptions[i].day_count,
          name: noticePeriodOptions[i].name,
          slug: noticePeriodOptions[i].slug,
        }]
      }
    }
  }
  function getCtcRangeObject(value) {
    for (var i = 0; i < ctcOptions.length; i++) {
      if (ctcOptions[i].lower_value == value) {
        return [{
          id: ctcOptions[i].id,
          lower_value: ctcOptions[i].lower_value,
          name: ctcOptions[i].name,
        }]
      }
    }
  }


  function getRoleObject(value) {
    for (var i = 0; i < roleOptions.length; i++) {
      if (roleOptions[i].slug == value) {
        return [{
          id: roleOptions[i].id,
          slug: roleOptions[i].slug,
          name: roleOptions[i].name,
        }]
      }
    }
  }

  var ctcOptions = [];
  for (var i in user.enums_ctc_range) {
    var item = user.enums_ctc_range[i];
    ctcOptions.push({
      id: item.id,
      lower_value: item.lower_value,
      name: item.name,
    });
  }

  var noticePeriodOptions = [];
  for (var i in user.enums_notice_period) {
    var item = user.enums_notice_period[i];
    noticePeriodOptions.push({
      id: item.id,
      day_count: item.day_count,
      name: item.name,
      slug: item.slug,
    });
  }


  var roleOptions = [];
  for (var i in user.enums_job_roles) {
    var item = user.enums_job_roles[i];
    roleOptions.push({
      id: item.id,
      name: item.name,
      slug: item.slug,
    });
  }

  const getRolesLabel = (event) => {
    for (var i = 0; i < roleOptions.length; i++) {
      if (roleOptions[i].id == event) {
        return [{
          id: event,
          name: roleOptions[i].name,
          slug: roleOptions[i].slug,
        }]
      }
    }
  };

  const getNoticePeriodLabel = (event) => {
    for (var i = 0; i < noticePeriodOptions.length; i++) {
      if (noticePeriodOptions[i].id == event) {
        return [{
          id: event,
          day_count: noticePeriodOptions[i].day_count,
          name: noticePeriodOptions[i].name,
          slug: noticePeriodOptions[i].slug,
        }]
      }
    }
  };

  const getCtcLowerValue = (event) => {
    for (var i = 0; i < ctcOptions.length; i++) {
      if (ctcOptions[i].id == event) {
        return [{
          id: ctcOptions[i].id,
          lower_value: ctcOptions[i].lower_value,
          name: ctcOptions[i].name,
        }]
      }
    }
  }

  function onSubmitPressHandler() {
    if (isEmployed) {
      const variablesCurrent = {
        candidate_id: user.id,
        current_company: selectedCurrentCompany,
        current_role: selectedCurrentRole[0].slug,
        current_ctc: selectedCurrentCTC[0].lower_value,
        date_of_joining: selectedCurrentStartDate,
        total_work_experience_years: workExpCurrent,
        notice_period: noticePeriod[0].day_count,
      };
      insertCandidateCurrentEmploymentInfo(variablesCurrent).then(
        details => {
          showAlertForPastWork();
        });
    }
    else {
      const variablesPast = {
        candidate_id: user.id,
        company_name: selectedPastCompany,
        role: selectedPastRole[0].slug,
        company_ctc: selectedPastCTC[0].lower_value,
        start_date: selectedPastStartDate,
        end_date: selectedPastEndDate,
        total_work_experience_years: workExpPast,
      };

      insertCandidateWorkExperienceInfo(variablesPast).then(
        details => {
          showAlertForPastWork();
        });
    }
  }

  function showAlertForPastWork() {
    Alert.alert(
      'Past Work Experience',
      'Do you have any more work experience',
      [
        {
          text: 'Yes', onPress: () => {
            setMorePastExp(true);
            setEmployed(false);


            setSelectedPastCompany('');
            setSelectedPastRole('');
            setSelectedPastCTC('');
            setSelectedPastEndDate('');
            setSelectedPastStartDate('');
            setWorkExpPast(0);

            setValidPastCompany(false);
            setValidPastRole(false);
            setValidPastCTC(false);
            setValidPastWorkExp(true);
            setValidPastStartDate(false);
            setValidPastEndDate(false);

          }
        },
        {
          text: 'No', onPress: () => {
            if (previousPage == 'Profile') {
              props.navigation.goBack();
            } else {
              if (state_index + 1 == onboarding_config.length) {
                insertCandidateStateTrack({ candidate_state_id: id, status: 'completed', current_action: current_action })
                props.navigation.navigation('Prepare');
              }
              props.navigation.navigate(onboardingUtils.getOnBoardingState(onboarding_config[state_index + 1]),
                {
                  id: id,
                  onboarding_config: onboarding_config,
                  state_index: state_index + 1,
                  current_action: current_action
                }
              );
            }
          }
        },
      ],
      { cancelable: false }
    );
  }


  WorkExperience.navigationOptions = {
    title: '7 Hires',
  };
  //TODO: add current and past experience text
  return (
    <ImageBackground
      source={require('../../../assets/onboarding_bg.png')}
      style={{ width: '100%', height: '100%', flex: 1 }}>
      <View style={styles.container}>
        {global.HermesInternal == null ? null : (
          <View style={styles.engine}>
            <Text style={styles.footer}>Engine: Hermes</Text>
          </View>
        )}

        <View style={styles.sectionTitle}>
          <Text style={styles.textTitle}>Work Experience</Text>
        </View>
        {!isMorePastExp ? (
          <View style={{ flex: 2, flexDirection: 'column' }}>
            <View style={{ flex: 1 }}>
              <Text style={{ fontSize: 16, textAlign: 'left', marginHorizontal: 12, marginBottom: 6, }}>Are you currently Employed?</Text>
            </View>
            <View style={{ flex: 1, marginBottom: 12, marginTop: -4, }}>
              <RadioSelectOption
                leftText={"Yes"}
                rightText={"No"}
                isLeftSelected={isEmployed}
                onLeft={() => {
                  setEmployed(true)
                }}
                onRight={() => {
                  setEmployed(false)
                }} />
            </View>
          </View>
        ) : null}

        <View style={{ flex: 8, flexDirection: 'column' }}>
          <ScrollView>
            <View style={styles.textInputLayoutContainer}>
              {!isMorePastExp ? (<View style={styles.textInputContainer}>
                {/*
           total work experience
          */}
                <TextInputLayout>
                  <TextInput
                    defaultValue={isEmployed ? (user.candidate_current_work_info ? user.candidate_current_work_info.total_work_experience_years.toString() : null) : (user.candidate_work_experience_info ? user.candidate_work_experience_info.total_work_experience_years.toString() : null)}
                    placeholder="Total Work Experience(years)"
                    keyboardType={"number-pad"}
                    style={styles.textInput}
                    onChangeText={val => {
                      if (isEmployed) {
                        setWorkExpCurrent(val);
                        if (val.length > 0)
                          setValidCurrentWorkExp(true);
                        else
                          setValidCurrentWorkExp(false);
                      }
                      else {
                        setWorkExpPast(val);
                        if (val.length > 0)
                          setValidPastWorkExp(true);
                        else
                          setValidPastWorkExp(false);
                      }
                    }}
                  />
                </TextInputLayout>
              </View>) : null}
              {/*
current company
          */}
              <TextInputLayout style={styles.textInputContainer}>
                <TextInput
                  defaultValue={isMorePastExp ? null : (isEmployed ? (user.candidate_current_work_info ? user.candidate_current_work_info.current_company : null) : (user.candidate_work_experience_info ? user.candidate_work_experience_info.company_name : null))}
                  placeholder="Company Name"
                  style={styles.textInput}
                  onChangeText={val => {
                    if (isEmployed) {
                      setSelectedCurrentCompany(val);
                      if (val.length > 0)
                        setValidCurrentCompany(true);
                      else
                        setValidCurrentCompany(false);
                    }
                    else {
                      setSelectedPastCompany(val);
                      if (val.length > 0)
                        setValidPastCompany(true);
                      else
                        setValidPastCompany(false);
                    }
                  }}
                />
              </TextInputLayout>
              {/*
current Role
          */}
              <View style={styles.textInputContainer}>
                <TextInputLayout labelText={"Role"}>
                  <ItemsSelectTwo
                    prefetchedValue={isEmployed ? (!isEmpty(selectedCurrentRole) ? selectedCurrentRole : null) : (!isEmpty(selectedPastRole) ? selectedPastRole : null)}
                    isSelectSingle={true}
                    style={{ borderRadius: 5, fontSize: 12, }}
                    colorTheme={'#F14836'}
                    popupTitle="Select Role"
                    title="Current Role"
                    cancelButtonText="Cancel"
                    selectButtonText="Select"
                    searchPlaceHolderText="Eg. Manager"
                    data={roleOptions}
                    onSelect={data => {
                      if (isEmployed) {
                        setSelectedCurrentRole(getRolesLabel(data[0]));
                        if (data.length > 0)
                          setValidCurrentRole(true);
                        else
                          setValidCurrentRole(false);
                      }
                      else {
                        setSelectedPastRole(getRolesLabel(data[0]));
                        if (data.length > 0)
                          setValidPastRole(true);
                        else
                          setValidPastRole(false);
                      }
                    }}
                  />
                </TextInputLayout>
              </View>

              {/*
          Current CTC
          */}
              <View style={styles.textInputContainer}>
                <TextInputLayout labelText={"CTC/ Salary"}>
                  <ItemsSelectTwo
                    prefetchedValue={isEmployed ? (!isEmpty(selectedCurrentCTC) ? selectedCurrentCTC : null) : (!isEmpty(selectedPastCTC) ? selectedPastCTC : null)}
                    isSelectSingle={true}
                    style={{ borderRadius: 5, fontSize: 12, }}
                    colorTheme={'#F14836'}
                    popupTitle="Select Salary per Annum"
                    title="Current CTC"
                    cancelButtonText="Cancel"
                    selectButtonText="Select"
                    searchPlaceHolderText="Eg. 14LPA"
                    data={ctcOptions}
                    onSelect={data => {
                      if (isEmployed) {
                        setSelectedCurrentCTC(getCtcLowerValue(data[0]));
                        if (data.length > 0)
                          setValidCurrentCTC(true);
                        else
                          setValidCurrentCTC(false);
                      }
                      else {
                        setSelectedPastCTC(getCtcLowerValue(data[0]));
                        if (data.length > 0)
                          setValidPastCTC(true);
                        else
                          setValidPastCTC(false);
                      }
                    }}
                  />
                </TextInputLayout>
              </View>

              {/*
          How long have you worked here
          */}

              <View style={{ flex: 1, flexDirection: 'row' }}>
                {/* start date */}
                <View style={{ flex: 1, }}>
                  <TextInputLayout labelText={'Start Date'} style={{
                    marginHorizontal: 20, marginTop: 20,
                  }}>
                    <DatePicker
                      style={{
                        fontSize: 12,
                      }}
                      date={isEmployed ? selectedCurrentStartDate : selectedPastStartDate}
                      mode="date"
                      placeholder="Start Date"
                      format="YYYY-MM-DD"
                      minDate="1990-05-01"
                      maxDate="2019-12-30"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          width: 0,
                          height: 0,
                        },
                        dateInput: {
                          borderWidth: 0,
                          textAlign: 'left',
                          fontSize: 16,
                          position: 'absolute',
                          left: 0,
                          justifyContent: 'center'
                        },
                        placeholderText: {
                          paddingLeft: 0,
                          marginLeft: 0,
                          fontSize: 16,
                          textAlign: 'left'
                        },
                        dateText: {
                          fontSize: 16,
                        },
                      }}
                      onDateChange={value => {
                        if (isEmployed) {
                          setSelectedCurrentStartDate(value);
                          if (value.length > 0)
                            setValidCurrentDate(true);
                          else
                            setValidCurrentDate(false);
                        }
                        else {
                          setSelectedPastStartDate(value);
                          if (value.length > 0)
                            setValidPastStartDate(true);
                          else
                            setValidPastStartDate(false);
                        }
                      }}
                    />
                  </TextInputLayout>
                </View>

                {/* end date */}
                <View style={{ flex: 1, }}>
                  {isEmployed ? (
                    <TextInputLayout style={styles.textInputContainer}>
                      <TextInput
                        editable={false}
                        value={"Present"}
                        placeholder={"End Date"}
                        style={styles.textInput}
                      />
                    </TextInputLayout>
                  ) : (
                      <TextInputLayout labelText={'End Date'} style={{
                        marginHorizontal: 20, marginTop: 20,
                      }}>
                        <DatePicker
                          style={{
                            fontSize: 12,
                          }}
                          date={selectedPastEndDate}
                          mode="date"
                          placeholder="End Date"
                          format="YYYY-MM-DD"
                          minDate="1990-05-01"
                          maxDate="2019-12-30"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                            dateIcon: {
                              width: 0,
                              height: 0,
                            },
                            dateInput: {
                              borderWidth: 0,
                              textAlign: 'left',
                              fontSize: 16,
                              position: 'absolute',
                              left: 0,
                              justifyContent: 'center'
                            },
                            placeholderText: {
                              paddingLeft: 0,
                              marginLeft: 0,
                              fontSize: 16,
                              textAlign: 'left'
                            },
                            dateText: {
                              fontSize: 16,
                            },
                          }}
                          onDateChange={value => {
                            setSelectedPastEndDate(value);
                            if (value.length > 0)
                              setValidPastEndDate(true);
                            else
                              setValidPastEndDate(false);
                          }}
                        />
                      </TextInputLayout>)}
                </View>
              </View>
              <View>

              </View>

              {/*
          Notice Period
          */}
              {(isEmployed && !isMorePastExp) ?
                (<View style={styles.textInputContainer}>
                  <TextInputLayout labelText={"Notice Period"}>
                    <ItemsSelectTwo
                      prefetchedValue={!isEmpty(noticePeriod) ? noticePeriod : null}
                      isSelectSingle={true}
                      style={{ borderRadius: 5, fontSize: 12, }}
                      colorTheme={'#F14836'}
                      popupTitle="Select Duration"
                      title="Notice Period"
                      cancelButtonText="Cancel"
                      selectButtonText="Select"
                      searchPlaceHolderText="Eg. 2 months"
                      data={noticePeriodOptions}
                      onSelect={data => {
                        setNoticePeriod(getNoticePeriodLabel(data[0]));
                        if (data.length > 0)
                          setValidNoticePeriod(true);
                        else
                          setValidNoticePeriod(false);
                      }}
                    />
                  </TextInputLayout>
                </View>)
                : null}
            </View>
          </ScrollView>
        </View>
        {/* for space so that scrollview doesn't collapse with button */}
        <View style={{ flex: 1.5, }}>
          <Text> </Text>

        </View>
        <View style={styles.materialButton}>
          <Button
            disabled={
              isEmployed ? (
                !(isValidCurrentCompany &&
                  isValidCurrentRole &&
                  isValidCurrentCTC &&
                  isValidNoticePeriod &&
                  isValidCurrentWorkExp &&
                  isValidCurrentDate)
              ) :
                (!isMorePastExp ?
                  !(isValidPastCompany &&
                    isValidPastRole &&
                    isValidPastCTC &&
                    isValidPastWorkExp &&
                    isValidPastStartDate &&
                    isValidPastEndDate) :
                  !(isValidPastCompany &&
                    isValidPastRole &&
                    isValidPastCTC &&
                    isValidPastStartDate &&
                    isValidPastEndDate))

            }
            raised
            title={nextButtonName}
            buttonStyle={{ justifyContent: 'center', zIndex: 2, borderRadius: 6, height: 45, paddingHorizontal: 8, backgroundColor: '#f14836', }}
            titleStyle={{ fontWeight: '200', fontSize: 16, textAlignVertical: 'center' }}
            onPress={() => {
              onSubmitPressHandler();
            }}
          />
        </View>
      </View>
    </ImageBackground >
  );
}

const styles = StyleSheet.create({
  modalTitleText: {
    textAlign: 'center',
    fontSize: 20,
    marginHorizontal: 24,
    marginVertical: 16,
  },
  container: {
    flex: 1,
    margin: 16,
    flexDirection: 'column',
  },
  textInputLayoutContainer: {
    flex: 6,
  },
  button: {
    position: 'absolute',
    bottom: 0,
    fontSize: 24,
  },
  materialButton: {
    position: 'absolute',
    bottom: 0,
    fontSize: 60,
    paddingHorizontal: 20,
    justifyContent: 'flex-end',
    width: '100%',
    flex: 1,
  },
  textInputContainer: {
    marginHorizontal: 20,
    marginTop: 20,
    // flexWrap: 'wrap'
  },
  textInput: {
    fontSize: 16,
    // width: "95%",
    height: 40,
    color: '#000000',
    // borderWidth: 1,
    // borderColor: 'gray',
    // textAlign: 'center',
  },
  textTitle: {
    textDecorationColor: 'pink',
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 20,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionTitle: {
    fontSize: 24,
    flex: 1,
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
