import {
  sectionList,
  sectionBySlug,
  candidateCourseTrack,
  insertCourseTracks,
} from './../helpers/queries';
import {graphQlClient} from './../helpers/graphQlAPIClient';

export function getSectionList(courseSlug) {
  return graphQlClient.query({
    query: sectionList,
    variables: {
      slug: courseSlug,
    },
  });
}

export function getSectionDetails(sectionSlug) {
  return graphQlClient.query({
    query: sectionBySlug,
    variables: {
      slug: sectionSlug,
    },
  });
}

export function candidateCourseTracks(candidate_id, courseSlug) {
  return graphQlClient.query({
    query: candidateCourseTrack,
    variables: {
      candidate_id,
      course_slug: courseSlug,
    },
  });
}

export function insertCourseTrack(variables) {
  return graphQlClient.mutate({
    mutation: insertCourseTracks,
    variables,
  });
}
