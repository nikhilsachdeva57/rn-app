import * as actionTypes from './actionTypes';

export const setFirstName = (firstName) => {
    return { type: actionTypes.SET_FIRST_NAME, firstName };
};

export const setLastName = (lastName) => {
    return { type: actionTypes.SET_LAST_NAME, lastName };
};

export const setAvatar = (avatar) => {
    return { type: actionTypes.SET_AVATAR, avatar };
};

export const setEditProfile = (isEdit) => {
    return { type: actionTypes.SET_EDIT_PROFILE, isEdit };
};
