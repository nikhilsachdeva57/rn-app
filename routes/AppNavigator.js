import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import AuthNavigator from './AuthNavigator';
import OnBoardingNavigator from './OnBoardingNavigator';
import HomeNavigator from './HomeNavigator';
import SplashScreen from '../src/containers/SplashScreen';
import BasicInfo from '../src/containers/Onboarding/BasicInfo';
import AppIntro from '../src/containers/Onboarding/AppIntro';
import Prepare from '../src/containers/Prepare/Prepare'
const AppNavigator = createSwitchNavigator({
  Splash: SplashScreen,
  AppIntro,
  Auth: AuthNavigator,
  Home: HomeNavigator,
  Prepare,
});

export default createAppContainer(AppNavigator);
