import {createLogger} from 'redux-logger';
import {createStore, reducer} from 'easy-peasy';
import thunk from 'redux-thunk';
import quiz from './models/quiz';
import course from './models/course';
import user from './models/user';
import appConfig from './models/appConfig'
import {register} from './reducers';

const configuredLogger = createLogger({collapsed: true});

const store = createStore(
  {
    appConfig,
    user,
    register: reducer(register),
    quiz,
    course,
  },
  {
    disableImmer: true,
    middleware: [thunk, configuredLogger],
  },
);

export default store;
