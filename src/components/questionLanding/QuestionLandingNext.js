import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {useStoreActions, useStoreState} from 'easy-peasy';
import {withNavigation} from 'react-navigation';

const QuestionLandingNext = props => {
  const {
    quizSlug,
    sectionSlug,
    attemptId,
    nextQuestionSlug,
    questionSlug,
    time,
  } = props;
  const {insertQuestionTrack} = useStoreActions(actions => actions.quiz);
  const {selectedAnswers} = useStoreState(state => state.quiz);
  const userData = useStoreState(state => state.user);
  const courseData = useStoreState(state => state.course);
  const handleNextBtnClick = () => {
    const variables = {
      candidate_quiz_track_id: attemptId,
      candidate_id: userData.id,
      slug: questionSlug,
      is_correct: selectedAnswers[questionSlug].isCorrect
        ? selectedAnswers[questionSlug].isCorrect
        : false,
      submitted_answer: selectedAnswers[questionSlug].selectedOptions,
      steps_used: selectedAnswers[questionSlug].stepsUsed,
      time_spent_in_ms: time,
      step_info: null,
      score: selectedAnswers[questionSlug].selectedOptions.map(option => {
        return option.score ? option.score : null;
      }),
    };
    let tempCourseSlug;
    insertQuestionTrack(variables).then(
      resp => {
        // console.log('resp in insertQuestionTrack', resp);
      },
      error => {
        console.log('error in insertQuestionTrack', error);
      },
      (tempCourseSlug = courseData ? courseData.course_slug : undefined),
      props.navigation.navigate('QuestionLanding', {
        courseSlug: tempCourseSlug,
        sectionSlug,
        quizSlug,
        questionSlug: nextQuestionSlug,
        attemptId,
      }),
    );
  };
  return (
    <View>
      <TouchableOpacity
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={handleNextBtnClick}>
        <View
          style={{
            flex: 0.25,

            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={styles.nextQuestion}>Next</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  nextQuestion: {
    color: 'white',
    margin: 20,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    width: width / 3,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#f14836',
  },
});

export default withNavigation(QuestionLandingNext);
