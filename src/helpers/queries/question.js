import gql from 'graphql-tag';

export const GET_QUESTION_BY_SLUG = gql`
  query getQuestionBySlug($slug: String!) {
    get_question(slug: $slug) {
      id
      slug_id
      format_id
      options
      steps
      text
      text_image
      hint
      widgets
      scoring_type
    }
  }
`;

export const INSERT_QUESTION_TRACK = gql`
  mutation(
    $candidate_quiz_track_id: uuid!
    $candidate_id: uuid!
    $slug: String!
    $is_correct: Boolean!
    $submitted_answer: jsonb!
    $steps_used: Boolean
    $steps_info: jsonb
    $time_spent_in_ms: bigint!
    $score: jsonb
  ) {
    insert_candidate_activities_candidate_question_track(
      objects: {
        candidate_quiz_track_id: $candidate_quiz_track_id
        candidate_id: $candidate_id
        slug: $slug
        is_correct: $is_correct
        submitted_answer: $submitted_answer
        steps_used: $steps_used
        steps_info: $steps_info
        time_spent_in_ms: $time_spent_in_ms
        score: $score
      }
    ) {
      returning {
        id
      }
    }
  }
`;
