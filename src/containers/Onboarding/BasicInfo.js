import React, { Component, useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TextInput,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  ToastAndroid,
  StatusBar,
  AsyncStorage,
  Alert,
  ImageBackground,
} from 'react-native';
import { isEmpty } from 'lodash-es';
import { TextInputLayout } from 'rn-textinputlayout';
import { Button } from 'react-native-elements';
import { useStoreState, useStoreActions } from 'easy-peasy';
import DatePicker from 'react-native-datepicker';
import ItemsSelectTwo from '../../components/Onboarding/SelectTwoPicker/ItemsSelectTwo';
import * as onboardingUtils from '../../utils/onboardingUtils'
import RadioSelectOption from '../../components/Onboarding/RadioSelectOption'

export default function BasicInfo(props) {

  const user = useStoreState(state => state.user);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [isValidEmail, setValidEmail] = useState(false);
  const [isValidName, setValidName] = useState(false);
  const [isValidDOB, setValidDOB] = useState(true);
  const [isValidGender, setValidGender] = useState(false);
  const [isValidCity, setValidCity] = useState(false);
  const [selectedCity, setSelectedCity] = useState([]);
  const [selectedDate, setSelectedDate] = useState('');
  const [selectedGender, setSelectedGender] = useState('');
  const [locationData, setLocationData] = useState();
  const [isEnumFlag, setEnumFlag] = useState(false);

  const [isNewUser, setNewUser] = useState(true);
  const [isFresher, setFresher] = useState(true);

  //TODO:add ripple effects
  const {
    insertCandidateBasicInfo,
    getCities,
    getCandidateBasicInfo,
  } = useStoreActions(actions => actions.user);

  const { id, onboarding_config, state_index, current_action } = props.navigation.state.params;

  var nextButtonName = "Save";
  const previousPage = props.navigation.state.params.from;

  // useEffect(() => {
  //   return () => getCandidateBasicInfo(user.id);
  // }, [])
  useEffect(() => {
    if (!isEmpty(user) && isEmpty(user.enums_cities) && !isEnumFlag) {
      getCities();
      setEnumFlag(true);
    }
    if (!isEmpty(onboarding_config)) {
      nextButtonName = "Next";
      console.log("onbc", onboarding_config);
    }
    if (!isEmpty(user.id) && isNewUser) {
      getCandidateBasicInfo(user.id);
      console.log('isnewuser', isNewUser)
      setNewUser(false);
    }
    if (isEmpty(name) && !isEmpty(user.basic_info)) {
      setName(user.basic_info.first_name);
      setValidName(true);
    }
    if (isEmpty(email) && !isEmpty(user.basic_info)) {
      setEmail(user.basic_info.email);
      setValidEmail(true);
    }
    if (isEmpty(selectedGender) && !isEmpty(user.basic_info)) {
      setValidGender(true);
      setSelectedGender(getGenderObject(user.basic_info.gender));
    }
    if (isEmpty(selectedCity) && !isEmpty(user.basic_info)) {
      setValidCity(true);
      setSelectedCity(getLocationObject(user.basic_info.current_city));
    }
    if (isEmpty(selectedDate) && !isEmpty(user.basic_info)) {
      setValidDOB(true);
      setSelectedDate(user.basic_info.date_of_birth);
    }
  });

  //get****Object to get the object (containing(id,name,slug)) from the slug(from redux)
  function getLocationObject(value) {
    for (var i = 0; i < locationOptions.length; i++) {
      if (locationOptions[i].slug == value) {
        return [{
          id: locationOptions[i].id,
          slug: locationOptions[i].slug,
          name: locationOptions[i].name,
        }]
      }
    }
  }

  function getGenderObject(value) {
    for (var i = 0; i < genderOptions.length; i++) {
      if (genderOptions[i].slug == value) {
        return [{
          id: genderOptions[i].id,
          slug: genderOptions[i].slug,
          name: genderOptions[i].name,
        }]
      }
    }
  }


  const genderOptions = [
    { id: 1, name: 'Male', slug: 'male' },
    { id: 2, name: 'Female', slug: 'female' },
    { id: 3, name: 'Others', slug: 'others' },
  ];

  var locationOptions = [];

  for (var i in user.enums_cities) {
    var item = user.enums_cities[i];
    locationOptions.push({
      id: item.id,
      name: item.name,
      slug: item.slug,
    });
  }

  // get*****label to get the name(string) (from the id) after selecting the value from picker
  const getLocationLabel = (event) => {
    for (var i = 0; i < locationOptions.length; i++) {
      if (locationOptions[i].id == event) {
        return [{
          id: event,
          name: locationOptions[i].name,
          slug: locationOptions[i].slug,
        }]
      }
    }
  };

  const getGenderLabel = (event) => {
    for (var i = 0; i < genderOptions.length; i++) {
      if (genderOptions[i].id == event) {
        return [{
          id: event,
          name: genderOptions[i].name,
          slug: genderOptions[i].slug,
        }]
      }
    }
  };

  function onEmailChangeHandler(val) {
    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(val)) {
      setValidEmail(true);
      setEmail(val);
    } else {
      setValidEmail(false);
    }
  }

  function onNameChangeHandler(val) {
    if (val.length > 0) {
      setValidName(true);
      setName(val);
    } else {
      setValidName(false);
    }
  }

  function onSubmitPressHandler() {
    const variables = {
      candidate_id: user.id,
      current_city: selectedCity[0].slug,
      date_of_birth: selectedDate,
      email: email,
      first_name: name,
      gender: selectedGender[0].slug,
    };

    console.log('variables', variables)
    //If this page open
    insertCandidateBasicInfo(variables).then(details => {
      if (previousPage == 'Profile') {
        props.navigation.goBack();
      } else {
        if (state_index + 1 == onboarding_config.length) {
          console.log('fjsdjfka')
          insertCandidateStateTrack({ candidate_state_id: id, status: 'completed', current_action: current_action })
          props.navigation.navigation('Prepare');
        }
        props.navigation.navigate(onboardingUtils.getOnBoardingState(onboarding_config[state_index + 1]),
          {
            id: id,
            onboarding_config: onboarding_config,
            state_index: state_index + 1,
            current_action: current_action
          }
        );
      }
    });
  }

  BasicInfo.navigationOptions = {
    title: '7 Hires',
  };

  return (
    <ImageBackground
      source={require('../../../assets/onboarding_bg.png')}
      style={{ width: '100%', height: '100%', flex: 1 }}>
      <View style={styles.container}>
        {global.HermesInternal == null ? null : (
          <View style={styles.engine}>
            <Text style={styles.footer}>Engine: Hermes</Text>
          </View>
        )}
        <View style={styles.sectionTitle}>
          <Text style={styles.textTitle}>Basic Info</Text>
        </View>
        <View style={{ flex: 4, flexDirection: 'column', justifyContent: 'center' }}>
          <ScrollView>
            <View style={styles.textInputLayoutContainer}>
              <TextInputLayout style={styles.textInputContainer}>
                <TextInput
                  defaultValue={user.basic_info ? user.basic_info.first_name : null}
                  placeholder="Name"
                  style={styles.textInput}
                  onChangeText={val => {
                    onNameChangeHandler(val);
                  }}
                />
              </TextInputLayout>
              <TextInputLayout style={styles.textInputContainer}>
                <TextInput
                  defaultValue={user.basic_info ? user.basic_info.email : null}
                  placeholder="Email"
                  style={styles.textInput}
                  onChangeText={val => {
                    onEmailChangeHandler(val);
                  }}
                />
              </TextInputLayout>

              <View style={styles.textInputContainer}>
                <TextInputLayout labelText={"Current City"}>
                  <ItemsSelectTwo
                    prefetchedValue={!isEmpty(selectedCity) ? selectedCity : null}
                    isSelectSingle={true}
                    style={{ borderRadius: 5, fontSize: 12, }}
                    colorTheme={'#F14836'}
                    popupTitle="Select locations"
                    title="Current City"
                    cancelButtonText="Cancel"
                    selectButtonText="Select"
                    searchPlaceHolderText="Eg. Delhi"
                    data={locationOptions}
                    onSelect={data => {
                      setSelectedCity(getLocationLabel(data[0]));
                      if (data.length > 0)
                        setValidCity(true);
                      else
                        setValidCity(false);
                    }}
                    onRemoveItem={data => {
                    }}
                  />
                </TextInputLayout>
              </View>
              <View style={styles.textInputContainer}>
                <TextInputLayout labelText={"Gender"}>
                  <ItemsSelectTwo
                    prefetchedValue={!isEmpty(selectedGender) ? selectedGender : null}
                    isSelectSingle={true}
                    style={{ borderRadius: 5, fontSize: 12, }}
                    colorTheme={'#F14836'}
                    popupTitle="Select Gender"
                    title="Gender"
                    cancelButtonText="Cancel"
                    selectButtonText="Select"
                    searchPlaceHolderText="Eg. Male"
                    data={genderOptions}
                    onSelect={data => {
                      setSelectedGender(getGenderLabel(data[0]));
                      if (data.length > 0)
                        setValidGender(true);
                      else
                        setValidGender(false);
                      console.log('date', selectedDate);
                    }}
                    onRemoveItem={data => {
                    }}
                  />
                </TextInputLayout>
              </View>
              <TextInputLayout labelText={'Date of Birth'} style={{
                marginHorizontal: 20, marginTop: 20,
              }}>
                <DatePicker
                  style={{
                    fontSize: 12,
                  }}
                  date={selectedDate}
                  mode="date"
                  placeholder="Date of Birth"
                  format="YYYY-MM-DD"
                  minDate="1990-05-01"
                  maxDate="2020-06-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      width: 0,
                      height: 0,
                    },
                    dateInput: {
                      borderWidth: 0,
                      textAlign: 'left',
                      fontSize: 16,
                      position: 'absolute',
                      left: 0,
                      justifyContent: 'center'
                    },
                    placeholderText: {
                      paddingLeft: 0,
                      marginLeft: 0,
                      fontSize: 16,
                      textAlign: 'left'
                    },
                    dateText: {
                      fontSize: 16,
                    },
                  }}
                  onDateChange={value => {
                    setSelectedDate(value);
                    if (value.length > 0)
                      setValidDOB(true);
                    else
                      setValidDOB(false);
                  }}
                />
                {/*TODO:style in ItemsSelectTwo*/}
              </TextInputLayout>

            </View>
          </ScrollView>
        </View>
        {/* <View style={styles.textInputContainer}>
          <Text style={{ fontSize: 16, textAlign: 'left', marginHorizontal: 12, marginBottom: 6, }}>Experience Level</Text>
          <View style={{ marginBottom: 12, }}>
            <RadioSelectOption
              leftText={"Fresher"}
              rightText={"Experienced"}
              isLeftSelected={isFresher}
              onLeft={() => {
                setFresher(true)
              }}
              onRight={() => {
                setFresher(false)
              }} />
          </View>
        </View> */}
        <View style={{ flex: 0.5 }}></View>

        <View style={styles.materialButton}>
          <Button
            disabled={
              !(isValidEmail && isValidName && isValidDOB && isValidGender && isValidCity)
              // && isEmpty(user.basic_info)
            }
            raised
            title={nextButtonName}
            buttonStyle={{ justifyContent: 'center', zIndex: 2, borderRadius: 6, height: 45, paddingHorizontal: 8, backgroundColor: '#f14836', }}
            titleStyle={{ fontWeight: '200', fontSize: 16, textAlignVertical: 'center' }}
            onPress={() => {
              onSubmitPressHandler();
            }}
          />
        </View>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  // topText: {
  //   marginTop: 30,
  //   textAlign: "center",
  //   fontSize: 20,
  //   fontWeight: "700",

  // },
  // input: {
  //   marginTop: 40,
  //   margin: 15,
  //   height: 45,
  //   borderWidth: 1,
  //   paddingLeft: 10,
  //   borderRadius: 10,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // },
  // button: {
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   margin: 30,
  //   width: 120,
  //   borderRadius: 10,
  //   alignItems: 'center',
  //   backgroundColor: '#2196F3'
  // },
  // buttonText: {
  //   textAlign: 'center',
  //   padding: 20,
  //   color: 'white'
  // },

  container: {
    flex: 1,
    // justifyContent: 'center',
    margin: 16,
  },
  textInputLayoutContainer: {
    flex: 10,
    justifyContent: 'center',
  },
  button: {
    position: 'absolute',
    bottom: 0,
    fontSize: 24,
  },
  materialButton: {
    position: 'absolute',
    bottom: 0,
    fontSize: 60,
    paddingHorizontal: 20,
    justifyContent: 'flex-end',
    width: '100%',
    flex: 1,
  },
  textInputContainer: {
    marginHorizontal: 20,
    marginTop: 15,
    // flexWrap: 'wrap'
  },
  textInput: {
    fontSize: 16,
    // width: "95%",
    height: 40,
    // borderWidth: 1,
    // borderColor: 'gray',
    // textAlign: 'center',
  },
  textTitle: {
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 20,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionTitle: {
    fontSize: 24,
    flex: 0.5,
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
