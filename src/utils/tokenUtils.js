import AsyncStorage from '@react-native-community/async-storage';

export const getToken = async () => {
  const token = await AsyncStorage.getItem('id_token');
  return token;
};
export const setToken = async token => {

  try {
    await AsyncStorage.setItem('id_token', token);
  } catch (error) {
    console.debug(error);
  }
};
export const deleteToken = async () => {
  AsyncStorage.removeItem('id_token');
};

export const isLoggedIn = async () => {
  return !!(await getToken());
};
export const getTokenData = async () => {
  var jwtDecode = require('jwt-decode');
  const token = await getToken();
  if (!token) {
    return false;
  }
  return jwtDecode(token);
};
