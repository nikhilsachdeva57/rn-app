import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
} from 'react-native';
import {Card, ListItem} from 'react-native-elements';
import style from '../auth/style';
import JobListCard from '../../components/JobListCard';
import {isEmpty} from 'lodash-es';
export default JobsList = (props) =>{

  const job = props.navigation.getParam('job');


  return (
    <ScrollView style={styles.detailView}>
      <View >
      <Image style={styles.logo} source={{uri :job.company.company_logo}}></Image>
	
  <Text style={styles.topText}>{job.job_title}</Text>
  <Text style={styles.jobRoleText}>{job.jobrole.name}</Text>
  <Text style={{padding : 15, fontSize: 15, fontWeight: "700"}}>Description : </Text>
  {!isEmpty(job.job_description) ? (

    job.job_description.map((item,key) =>  {
      return(
        <View>
            <Text key={key} style={styles.descriptionText}>{item.text}</Text>
            </View>
      )
        
    })
  ): null}
  <View style={{flexDirection: "row", padding: 10,}}>
<Text style={{fontWeight: "700"}}>Industry : </Text><Text>{ job.company ? job.company.industry.name : null}</Text>
</View>


{/* Must Have */}
{isEmpty(job.must_have) ? null : (
  <Text style={{padding: 10, fontWeight : "700"}}>Must Have : </Text>
)}

{
  isEmpty(job.must_have) ? (
    null
  ) : 
  (
  
    job.must_have.map((m,i) => {
      return (
        <View style={{flexDirection : "row", padding: 5, marginLeft: 10,}}>
          <Text style={{fontSize: 15,}}>{'\u2022'}</Text>
        <Text>{m.text}</Text>
        </View>
      )
    })

  )
}


{/* Qualification */}


{isEmpty(job.must_have) ? null : (
  <Text style={{padding: 10, fontWeight : "700"}}>Qualification : </Text>
)}

{
  isEmpty(job.qualification) ? (
    null
  ) : 
  (
  
    job.qualification.map((q,i) => {
      return (
        <View style={{flexDirection : "row", padding: 5, marginLeft: 10,}}>
          <Text style={{fontSize: 15,}}>{'\u2022'}</Text>
        <Text>{q.text}</Text>
        </View>
      )
    })

  )
}



{/* CTC */}

<View style={{flexDirection: "row", padding: 10,}}>
<Text style={{fontWeight: "700"}}>CTC : </Text><Text>{ job.ctc_range ? job.ctc_range.name : null}</Text>
</View>
    
    </View>


  
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  logo : {
		margin :20,
		padding: 10,
		position: "absolute",
    width : 170,
		alignSelf: "flex-start"
  },
  jobRoleText : {
    fontSize: 20,
    padding: 15,
  },
  detailView : {
    backgroundColor : "#fff"
  },
  descriptionText: {
    padding : 15,
  },
  topText: {
    marginTop: 30,
    textAlign:"center",
    alignSelf: "flex-start",
    fontSize: 20,
    fontWeight: "700",
    padding: 15,
  },
  item : {
    margin : 10,
  }
});

