import {
  GET_QUESTION_BY_SLUG,
  INSERT_QUESTION_TRACK,
} from './../helpers/queries';
import {graphQlClient} from './../helpers/graphQlAPIClient';

export function getQuestionDetails(quizSlug) {
  return graphQlClient.query({
    query: GET_QUESTION_BY_SLUG,
    variables: {
      slug: quizSlug,
    },
  });
}

export function insertQuestionTrack(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_QUESTION_TRACK,
    variables,
  });
}
