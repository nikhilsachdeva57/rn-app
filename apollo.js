import ApolloClient from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {HttpLink} from 'apollo-link-http';
// import Cookies from 'universal-cookie';
import {REACT_APP_GRAPHQL_URL} from 'react-native-dotenv';
// import * as userUtils from './utils/userUtils';

export const create = () => {
  // const cookie = new Cookies();
  // const token = cookie.get(process.env.REACT_APP_LIDO_ADMIN_TOKEN);
  // if (token && !userUtils.isExpired(token)) {
  return new ApolloClient({
    link: new HttpLink({
      uri: REACT_APP_GRAPHQL_URL,
      headers: {
        'content-type': 'application/json',
        'x-hasura-admin-secret': 'alliswell',
      },
    }),
    cache: new InMemoryCache(),
  });
  // } else {
  //     window.location = process.env.REACT_APP_LOGIN_URL + "?redirect=" + window.location
  // }
};
