import gql from 'graphql-tag';

export const GET_CANDIDATE_STATES = gql
  `query getCandidateStates($candidate_id: uuid!) {
    candidate_candidate_state(where: {candidate_id: {_eq: $candidate_id}, is_active: {_eq: true}}, order_by: {sort_order: asc}) {
      status
      live_session_id
      expire_at
      id
      lifecycle_state {
        lifecycle {
          job {
            job_title
          }
        }
        state {
          action {
            course_slug
            quiz_slug
            action_type
            estimated_time_in_mins
            onboarding_config
          }
          info_text
          info_video_id
          name
          slug
        }
        sort_order
      }
      candidate_state_tracks(limit: 1) {
        is_completed
        created_at
      }
    }
  }
`;

export const INSERT_CANDIDATE_BASIC_INFO = gql
  `mutation ($candidate_id: uuid, $date_of_birth: date, $current_city: String,$email: String, $first_name: String, $gender: String) {
  insert_candidate_candidate_basic_info(objects: {candidate_id: $candidate_id, current_city: $current_city, date_of_birth: $date_of_birth email: $email, first_name: $first_name, gender: $gender}, on_conflict: {constraint: candidate_basic_info_candidate_id_key, update_columns: [first_name, current_city, email, gender, date_of_birth]}) {
    returning {
      candidate_id
      created_at
      current_city
      email
      date_of_birth
      first_name
      gender
      id
    }
  }
}
  `;

export const INSERT_CANDIDATE_RESUME = gql
  `
mutation ($candidate_id: uuid, $file_name: String, $download_url: String,) {
  insert_candidate_candidate_resume(objects: {candidate_id: $candidate_id, download_url: $download_url, file_name: $file_name,}, on_conflict: {constraint: candidate_resume_pkey, update_columns: [file_name,download_url]}) {
    returning {
      candidate_id
      file_name
      download_url
    }
  }
}
`

export const GET_NOTICE_PERIOD = gql
  `
query getNoticePeriod {
  enums_notice_period {
    day_count
    id
    name
    slug
  }
}
`
export const GET_CANDIDATE_RESUME = gql
  `
query ($candidate_id:uuid) {
  candidate_candidate_resume(where: {candidate_id: {_eq: $candidate_id}}) {
    candidate_id
    download_url
    file_name
  }
}
`
export const GET_CANDIDATE_BASIC_INFO = gql
  `query ($candidate_id: uuid) {
  candidate_candidate_basic_info(where: {candidate_id: {_eq: $candidate_id}}) {
    current_city
    date_of_birth
    email
    first_name
    gender
    candidate_id
    created_at
    id
    last_name
    updated_at
    currentCity {
      name
    }
  }
}
`;


export const GET_USER_MOBILE = gql
  `query($id: uuid) {
    users_users(where: {id: {_eq: $id}}) {
      mobile
    }
  }
`;



export const INSERT_CANDIDATE_EDUCATION_INFO = gql
  `
  mutation(
    $candidate_id: uuid
    $degree_name: String
    $college_name: String
    $cgpa: float8
    $year_of_graduation: Int
  ) {
    insert_candidate_candidate_bachelor_degree_info(
      objects: {
        candidate_id: $candidate_id
        degree_name: $degree_name
        college_name: $college_name
        cgpa: $cgpa
        year_of_graduation: $year_of_graduation
      }
      on_conflict: {
        constraint: bachelor_degree_info_pkey
        update_columns: [
          candidate_id
          cgpa
          college_name
          degree_name
          year_of_graduation
        ]
      }
    ) {
      returning {
        candidate_id
        cgpa
        college_name
        created_at
        degree_name
        updated_at
        year_of_graduation
      }
    }
  }
`;

export const GET_CANDIDATE_EDUCATION_INFO = gql
  `
  query getCandidateEducationInfo($candidate_id: uuid) {
    candidate_candidate_bachelor_degree_info(
      where: {candidate_id: {_eq: $candidate_id}}
    ) {
      candidate_id
      cgpa
      college_name
      degree_name
      year_of_graduation
      candidateDegree {
        name
      }
    }
  }
`;

export const INSERT_CANDIDATE_12TH_INFO = gql
  `
  mutation(
    $candidate_id: uuid
    $school_name: String
    $aggregate_percentage: float8
    $school_board: String
    $year_of_graduation: date
  ) {
    insert_candidate_candidate_12th_info(
      objects: {
        candidate_id: $candidate_id
        school_name: $school_name
        school_board: $school_board
        aggregate_percentage: $aggregate_percentage
        year_of_graduation: $year_of_graduation
      }
      on_conflict: {
        constraint: candidate_12th_info_pkey
        update_columns: [
          school_name
          school_board
          aggregate_percentage
          year_of_graduation
          candidate_id
        ]
      }
    ) {
      returning {
        aggregate_percentage
        candidate_id
        created_at
        school_board
        school_name
        updated_at
        year_of_graduation
      }
    }
  }
`;

export const GET_CANDIDATE_12TH_INFO = gql
  `
  query getCandidate12thInfo($candidate_id: uuid) {
    candidate_candidate_12th_info(where: {candidate_id: {_eq: $candidate_id}}) {
      aggregate_percentage
      school_board
      school_name
      year_of_graduation
    }
  }
`;

export const INSERT_CANDIDATE_10TH_INFO = gql
  `
  mutation(
    $candidate_id: uuid
    $school_name: String
    $school_board: String
    $aggregate_percentage: float8
    $year_of_graduation: Int
  ) {
    insert_candidate_candidate_10th_info(
      objects: {
        candidate_id: $candidate_id
        school_name: $school_name
        school_board: $school_board
        aggregate_percentage: $aggregate_percentage
        year_of_graduation: $year_of_graduation
      }
      on_conflict: {
        constraint: candidate_10th_info_pkey
        update_columns: [
          school_name
          school_board
          aggregate_percentage
          year_of_graduation
          candidate_id
        ]
      }
    ) {
      returning {
        aggregate_percentage
        candidate_id
        created_at
        school_board
        school_name
        updated_at
        year_of_graduation
      }
    }
  }
`;

export const GET_CANDIDATE_10TH_INFO = gql
  `
  query getCandidate10thInfo($candidate_id: uuid) {
    candidate_candidate_10th_info(where: {candidate_id: {_eq: $candidate_id}}) {
      aggregate_percentage
      school_board
      school_name
      year_of_graduation
    }
  }
`;

export const GET_ENUMS_DEGREE = gql
  `
query getDegree {
  enums_degree {
    id
    name
    slug
  }
}

`
export const GET_APP_INTRO_CONFIG = gql
  `
query appIntroConfig {
  application_config_app_config_v0(where: {page_name: {_eq: "app_info"}}) {
    config
  }
}

`
export const GET_APP_VERSION_CONFIG = gql
  `
 query appIntroConfig { 
  application_config_app_version_config { 
    force_update 
    is_active 
    version_number 
  } 
} 

`;
export const INSERT_CANDIDATE_CURRENT_EMPLOYMENT_INFO = gql
  `
  mutation(
    $candidate_id: uuid
    $total_work_experience_years: float8
    $current_company: String
    $current_role: String
    $current_ctc: float8
    $date_of_joining: date
    $notice_period: float8
  ) {
    insert_candidate_candidate_current_work_info(
      objects: {
        candidate_id: $candidate_id
        total_work_experience_years: $total_work_experience_years
        current_company: $current_company
        current_role: $current_role
        current_ctc: $current_ctc
        date_of_joining: $date_of_joining
        notice_period: $notice_period
      }
      on_conflict: {
        constraint: candidate_current_work_info_pkey
        update_columns: [
          candidate_id
          current_company
          current_ctc
          current_role
          date_of_joining
          total_work_experience_years
        ]
      }
    ) {
      returning {
        candidate_id
        created_at
        current_company
        current_role
        current_ctc
        date_of_joining
        notice_period
        total_work_experience_years
        updated_at
      }
    }
  }
`;

export const GET_CANDIDATE_CURRENT_EMPLOYMENT_INFO = gql
  `
  query getCandidateCurrentEmploymentInfo($candidate_id: uuid) {
    candidate_candidate_current_work_info(
      where: {candidate_id: {_eq: $candidate_id}}
    ) {
      current_company
      current_ctc
      current_role
      date_of_joining
      notice_period
      total_work_experience_years
      currentRole {
        name
      }
      currentCtc {
        name
      }
      noticePeriod {
        name
      }
    }
  }
`;

export const INSERT_CANDIDATE_WORK_EXPERIENCE_INFO = gql
  `
  mutation(
    $candidate_id: uuid
    $company_name: String
    $role: String
    $company_ctc: float8
    $start_date: date
    $end_date: date
    $total_work_experience_years: float8
  ) {
    insert_candidate_candidate_work_experience_info(
      objects: {
        candidate_id: $candidate_id
        company_name: $company_name
        role: $role
        company_ctc: $company_ctc
        start_date: $start_date
        end_date: $end_date
        total_work_experience_years:$total_work_experience_years
      }
      on_conflict: {
        constraint: candidate_work_experience_info_candidate_id_company_name_compan
        update_columns: [
          candidate_id
          company_name
          company_ctc
          role
          start_date
          end_date
          total_work_experience_years
        ]
      }
    ) {
      returning {
        candidate_id
        company_ctc
        company_name
        created_at
        end_date
        id
        role
        start_date
        updated_at
        total_work_experience_years
      }
    }
  }
`;

export const GET_CANDIDATE_WORK_EXPERIENCE_INFO = gql
  `
  query getCandidateWorkExperienceInfo($work_ex_id: uuid) {
    candidate_candidate_work_experience_info(
      where: {candidate_id: {_eq: $work_ex_id}}
    ) {
      company_ctc
      company_name
      end_date
      role
      start_date
      id
      total_work_experience_years
      companyCtc {
        name
      }
      jobRole {
        name
      }
    }
  }
`;

export const INSERT_CANDIDATE_PREFERENCE_INFO = gql
  `
  mutation(
    $candidate_id: uuid
    $preferred_location: jsonb
    $desired_ctc: float8
    $min_ctc: float8
    $desired_role: jsonb
    $desired_industry: jsonb
  ) {
    insert_candidate_candidate_preference_info(
      objects: {
        candidate_id: $candidate_id
        preferred_location: $preferred_location
        desired_ctc: $desired_ctc
        min_ctc: $min_ctc
        desired_role: $desired_role
        desired_industry: $desired_industry
      }
      on_conflict: {
        constraint: candidate_preference_info_pkey
        update_columns: [
          candidate_id
          preferred_location
          desired_ctc
          desired_role
          desired_industry
          min_ctc
        ]
      }
    ) {
      returning {
        candidate_id
        created_at
        desired_ctc
        desired_industry
        desired_role
        min_ctc
        preferred_location
        updated_at
      }
    }
  }
`;

export const GET_CANDIDATE_PREFERENCE_INFO = gql
  `
  query getCandidatePreferenceInfo($candidate_id: uuid) {
    candidate_candidate_preference_info(
      where: {candidate_id: {_eq: $candidate_id}}
    ) {
      desired_ctc
      desired_industry
      desired_role
      min_ctc
      preferred_location
    }
  }
`;

export const INSERT_CANDIDATE_INTERNSHIP_INFO = gql
  `
  mutation(
    $candidate_id: uuid
    $company_name: String
    $type: String
    $profile: String
    $brief_description: String
  ) {
    insert_candidate_candidate_internship_info(
      objects: {
        candidate_id: $candidate_id
        company_name: $company_name
        type: $type
        profile: $profile
        brief_description: $brief_description
      }
      on_conflict: {
        constraint: candidate_internship_info_pkey
        update_columns: [
          candidate_id
          brief_description
          profile
          company_name
          profile
          type
        ]
      }
    ) {
      returning {
        brief_description
        candidate_id
        company_name
        created_at
        id
        profile
        type
        updated_at
      }
    }
  }
`;

export const GET_CANDIDATE_INTERNSHIP_INFO = gql
  `query getCandidateInternshipInfo($candidate_id: uuid) {
    candidate_candidate_internship_info(
      where: {candidate_id: {_eq: $candidate_id}}
    ) {
      brief_description
      company_name
      profile
      type
      id
      candidate_id
    }
  }
`;

export const GET_CITIES = gql
  `query getCities {
    enums_cities {
      name
      slug
      id
    }
  }
`;

export const GET_CTC_RANGE = gql
  `
query getCtcRange {
  enums_ctc_range {
    id
    lower_value
    name
  }
}
`;
export const GET_INDUSTRIES = gql
  `
query getIndustries {
  enums_industry {
    id
    name
    slug
  }
}
`;

export const GET_ROLES = gql
  `query getRoles {
  enums_job_roles {
    id
    name
    slug
  }
}`;

export const INSERT_CANDIDATE_STATE_TRACK = gql
  `
mutation($candidate_state_id: ID!, $status: String!, 
  $next_candidate_state_id: ID, $current_action: String,
    $start_time: BigInt,
    $end_time: BigInt){
  insert_candidate_state_track(candidate_state_id: $candidate_state_id,
    status: $status,
  next_candidate_state_id:$next_candidate_state_id ,
  current_action: $current_action,
  start_time: $start_time,
  end_time: $end_time){
    id
  }
}
`;

export const GET_CANDIDATE_JOBS = gql`
query{
  getJobsForCandidate{
    application_open
    notice_message
    jobs{
      id
      jobrole{
        slug
        name
      }
      job_title
      job_locations
      job_description
      company{
        name
        industry{
          slug
          name
        }
        company_logo
      }
      ctc_range{
        name
      }
      
    }
  }
}
`;

export const ASSIGN_JOB = gql`
mutation assignJob($candidateId: ID, $jobId: ID){
  assignJob(candidate_id: $candidateId, job_id: $jobId){
   candidate_job{
      job{
        job_title
      }
    }
    candidate_state{
      lifecycle_state{
        state{
          name
        }
      }
    }
  }
}`;