import React, { Component, useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TextInput,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  ToastAndroid,
  StatusBar,
  AsyncStorage,
  Alert,
  ImageBackground,
} from 'react-native';
import { isEmpty } from 'lodash-es';
import { TextInputLayout } from 'rn-textinputlayout';
import { Button } from 'react-native-elements';
import { useStoreState, useStoreActions } from 'easy-peasy';
import ItemsSelectTwo from '../../components/Onboarding/SelectTwoPicker/ItemsSelectTwo'
import * as onboardingUtils from '../../utils/onboardingUtils'


export default function Preferences(props) {
  const [selectedLocations, setSelectedLocations] = useState([]);
  const [selectedRoles, setSelectedRoles] = useState([]);
  const [selectedIndustries, setSelectedIndustries] = useState([]);

  const [locationFlag, setLocationFlag] = useState(false);
  const [roleFlag, setRoleFlag] = useState(false);
  const [industriesFlag, setIndustriesFlag] = useState(false);

  const [desiredCTC, setDesiredCTC] = useState('');
  const [minCTC, setMinCTC] = useState('');

  const [isDesiredCTCPicker, setDesiredCTCPicker] = useState(false);
  const [isDesiredMinPicker, setDesiredMinPicker] = useState(false);

  // const {next_state, upcoming_states} = user;
  const [open, setOpen] = React.useState(false);
  const [value, setValue] = useState('');
  const [cities, setCities] = useState([]);

  const [isValidLocation, setValidLocation] = useState(false);
  const [isValidCTC, setValidCTC] = useState(false);
  const [isValidMinCTC, setValidMinCTC] = useState(false);
  const [isValidRole, setValidRole] = useState(false);
  const [isValidIndustry, setValidIndustry] = useState(false);

  const [isNewUser, setNewUser] = useState(true);
  const [isEnumFlag, setEnumFlag] = useState(false);

  const { id, onboarding_config, state_index, current_action } = props.navigation.state.params;


  const user = useStoreState(state => state.user);
  const {
    insertCandidatePreferenceInfo,
    getCandidatePreferenceInfo,
    getCities,
    getRoles,
    getCtcRange,
    getIndustries,
    getCandidateStates,
    insertCandidateStateTrack,

  } = useStoreActions(actions => actions.user);

  const previousPage = props.navigation.state.params.from;
  var nextButtonName = 'Save';


  useEffect(() => {
    if (previousPage != 'Profile') {
      nextButtonName = 'Next';
    }

    if (!isEnumFlag) {
      if (isEmpty(user.enums_job_roles)) {
        getRoles();
      }
      if (isEmpty(user.enums_industry)) {
        getIndustries();
      }
      if (isEmpty(user.enums_cities)) {
        getCities();
      }
      if (isEmpty(user.enums_ctc_range)) {
        getCtcRange();
      }
      setEnumFlag(true);
    }
    if (isEmpty(user.candidate_preference_info) && isNewUser) {
      getCandidatePreferenceInfo(user.id);
      setNewUser(false);
    }

    if (isEmpty(minCTC) && !isEmpty(user.candidate_preference_info) && !isEmpty(ctcOptions)) {
      setValidMinCTC(true);
      setMinCTC(getCtcRangeObject(user.candidate_preference_info.min_ctc))
    }
    if (isEmpty(desiredCTC) && !isEmpty(user.candidate_preference_info) && !isEmpty(ctcOptions)) {
      setValidCTC(true)
      setDesiredCTC(getCtcRangeObject(user.candidate_preference_info.desired_ctc))
    }
    if (
      isEmpty(selectedLocations) &&
      !isEmpty(user.candidate_preference_info) &&
      !isEmpty(locationOptions) &&
      !locationFlag
    ) {
      setValidLocation(true);
      let temp_locations = [];
      for (var i in user.candidate_preference_info.preferred_location) {
        var item = user.candidate_preference_info.preferred_location[i];
        temp_locations.push(getLocationObject(item));
      }
      setSelectedLocations(temp_locations);
      setLocationFlag(true);
    }
    if (
      isEmpty(selectedRoles) &&
      !isEmpty(user.candidate_preference_info) &&
      !isEmpty(roleOptions) &&
      !roleFlag
    ) {
      setValidRole(true);
      setRoleFlag(true);
      let temp_roles = [];
      for (var i in user.candidate_preference_info.desired_role) {
        let item = user.candidate_preference_info.desired_role[i];
        temp_roles.push(getRoleObject(item));
      }
      setSelectedRoles(temp_roles);
      setRoleFlag(true);
    }

    if (
      isEmpty(selectedIndustries) &&
      !isEmpty(user.candidate_preference_info) &&
      !isEmpty(industryOptions) &&
      !industriesFlag
    ) {
      setValidIndustry(true);
      setIndustriesFlag(true);
      let temp_industry = [];
      for (var i in user.candidate_preference_info.desired_industry) {
        var item = user.candidate_preference_info.desired_industry[i];
        temp_industry.push(getIndustryObject(item));
      }
      setSelectedIndustries(temp_industry);
      setIndustriesFlag(true)
    }
  });

  var ctcOptions = [];
  var industryOptions = [];
  var locationOptions = [];
  var roleOptions = [];

  for (var i in user.enums_ctc_range) {
    var item = user.enums_ctc_range[i];
    ctcOptions.push({
      id: item.id,
      lower_value: item.lower_value,
      name: item.name,
    });
  }
  for (var i in user.enums_cities) {
    var item = user.enums_cities[i];
    locationOptions.push({
      id: item.id,
      name: item.name,
      slug: item.slug,
    });
  }

  for (var i in user.enums_job_roles) {
    var item = user.enums_job_roles[i];
    roleOptions.push({
      id: item.id,
      name: item.name,
      slug: item.slug,
    });
  }

  for (var i in user.enums_industry) {
    var item = user.enums_industry[i];
    industryOptions.push({
      id: item.id,
      name: item.name,
      slug: item.slug,
    });
  }


  function getCtcRangeObject(value) {
    for (var i = 0; i < ctcOptions.length; i++) {
      if (ctcOptions[i].lower_value == value) {
        return [{
          id: ctcOptions[i].id,
          lower_value: ctcOptions[i].lower_value,
          name: ctcOptions[i].name,
        }]
      }
    }
  }

  function getRoleObject(value) {
    for (var i = 0; i < roleOptions.length; i++) {
      if (roleOptions[i].slug == value) {
        return {
          id: roleOptions[i].id,
          slug: roleOptions[i].slug,
          name: roleOptions[i].name,
        }
      }
    }
  }

  function getLocationObject(value) {
    for (var i = 0; i < locationOptions.length; i++) {
      if (locationOptions[i].slug == value) {
        return {
          id: locationOptions[i].id,
          slug: locationOptions[i].slug,
          name: locationOptions[i].name,
        }
      }
    }
  }

  function getIndustryObject(value) {
    for (var i = 0; i < industryOptions.length; i++) {
      if (industryOptions[i].slug == value) {
        return {
          id: industryOptions[i].id,
          slug: industryOptions[i].slug,
          name: industryOptions[i].name,
        }
      }
    }
  }

  const getLocationLabel = (event) => {
    for (var i = 0; i < locationOptions.length; i++) {
      if (locationOptions[i].id == event) {
        return {
          id: event,
          name: locationOptions[i].name,
          slug: locationOptions[i].slug,
        }
      }
    }
  };

  const getRolesLabel = (event) => {
    for (var i = 0; i < roleOptions.length; i++) {
      if (roleOptions[i].id == event) {
        return {
          id: event,
          name: roleOptions[i].name,
          slug: roleOptions[i].slug,
        }
      }
    }
  };

  const getIndustryLabel = (event) => {
    for (var i = 0; i < industryOptions.length; i++) {
      if (industryOptions[i].id == event) {
        return {
          id: event,
          name: industryOptions[i].name,
          slug: industryOptions[i].slug,
        }
      }
    }
  };

  const getCtcLowerValue = (event) => {
    for (var i = 0; i < ctcOptions.length; i++) {
      if (ctcOptions[i].id == event) {
        return [{
          id: ctcOptions[i].id,
          lower_value: ctcOptions[i].lower_value,
          name: ctcOptions[i].name,
        }]
      }
    }
  }
  function onSubmitPressHandler() {
    var locations = [];
    for (var i in selectedLocations) {
      var item = selectedLocations[i].slug;
      if (!isEmpty(item))
        locations.push(item);
    }

    var roles = [];
    for (var i in selectedRoles) {
      var item = selectedRoles[i].slug;
      if (!isEmpty(item))
        roles.push(item);
    }

    var industries = [];
    for (var i in selectedIndustries) {
      var item = selectedIndustries[i].slug;
      if (!isEmpty(item))
        industries.push(item);
    }

    const variables = {
      candidate_id: user.id,
      preferred_location: [... new Set(locations)],
      desired_ctc: desiredCTC[0].lower_value,
      min_ctc: minCTC[0].lower_value,
      desired_role: [... new Set(roles)],
      desired_industry: [... new Set(industries)],
    };

    insertCandidatePreferenceInfo(variables).then(
      details => {
        if (previousPage == 'Profile') {
          props.navigation.goBack();
        } else {
          if (state_index + 1 == onboarding_config.length) {
            console.log('ye user', user)
            insertCandidateStateTrack({
              candidate_state_id: id, status: 'completed', current_action: current_action,
              next_candidate_state_id: user.upcoming_states[0] ? user.upcoming_states[0].id : null
            })
            props.navigation.navigate('Prepare');
          }
          props.navigation.navigate(onboardingUtils.getOnBoardingState(onboarding_config[state_index + 1]),
            {
              id: id,
              onboarding_config: onboarding_config,
              state_index: state_index + 1,
              current_action: current_action
            }
          );
        }
        // handleClickOpen();
      },
      error => {
        alert(error);
      },
    );
  }

  function isPresent(val, arr) {
    for (var i in arr) {
      if (val.slug != arr[i].slug) {
        return true;
      }
    }
  }

  Preferences.navigationOptions = {
    title: '7 Hires',
  };

  return (
    <ImageBackground
      source={require('../../../assets/onboarding_bg.png')}
      style={{ width: '100%', height: '100%', flex: 1 }}>
      <View style={styles.container}>
        {global.HermesInternal == null ? null : (
          <View style={styles.engine}>
            <Text style={styles.footer}>Engine: Hermes</Text>
          </View>
        )}
        <View style={styles.sectionTitle}>
          <Text style={styles.textTitle}>Preferences</Text>
        </View>
        <View style={{ flex: 6, flexDirection: 'column', justifyContent: 'center' }}>
          <ScrollView>
            <View style={styles.textInputLayoutContainer}>
              {/* 
            Preferred Location
             */}
              <View style={styles.eachField}>
                <TextInputLayout labelText={"Preferred Location"}>
                  <ItemsSelectTwo
                    prefetchedValue={!isEmpty(selectedLocations) ? selectedLocations : null}
                    isSelectSingle={false}
                    style={{ borderRadius: 5, fontSize: 12, }}
                    colorTheme={'#F14836'}
                    popupTitle="Select locations"
                    title="Preferred Location"
                    cancelButtonText="Cancel"
                    selectButtonText="Select"
                    searchPlaceHolderText="Eg. Delhi"
                    data={locationOptions}
                    onSelect={val => {
                      //if user selects location n times then get the nth locations only
                      let locations = [];
                      for (var i in val) {
                        locations.push(getLocationLabel(val[i]));
                      }
                      setSelectedLocations(locations);
                      console.log('length', val.length);
                      if (val.length > 0)
                        setValidLocation(true);
                      else
                        setValidLocation(false);
                    }}
                    onRemoveItem={val => {
                      setSelectedLocations('');
                      if (val.length > 0)
                        setValidLocation(true);
                      else
                        setValidLocation(false);
                    }}
                  />
                </TextInputLayout>
              </View>
              {/* 
        Desired CTC 
        */}
              <View style={styles.eachField}>
                <TextInputLayout labelText={"Desired CTC"}>
                  <ItemsSelectTwo
                    prefetchedValue={!isEmpty(desiredCTC) ? desiredCTC : null}
                    isSelectSingle={true}
                    style={{ borderRadius: 5, fontSize: 12, }}
                    colorTheme={'#F14836'}
                    popupTitle="Select Salary per Annum"
                    title="Desired CTC"
                    cancelButtonText="Cancel"
                    selectButtonText="Select"
                    searchPlaceHolderText="Eg. 14LPA"
                    data={ctcOptions}
                    onSelect={val => {
                      setDesiredCTC(getCtcLowerValue(val[0]));
                      if (val.length > 0)
                        setValidCTC(true);
                      else
                        setValidCTC(false);
                    }}
                  />
                </TextInputLayout>
              </View>
              {/* 
        Min acceptable CTC 
        */}
              <View style={styles.eachField}>
                <TextInputLayout labelText={"Minimum acceptable CTC"}>
                  <ItemsSelectTwo
                    prefetchedValue={!isEmpty(minCTC) ? minCTC : null}
                    isSelectSingle={true}
                    style={{ borderRadius: 5, fontSize: 12, }}
                    colorTheme={'#F14836'}
                    popupTitle="Select Salary per Annum"
                    title="Minimum CTC"
                    cancelButtonText="Cancel"
                    selectButtonText="Select"
                    searchPlaceHolderText="Eg. 14LPA"
                    data={ctcOptions}
                    onSelect={val => {
                      setDesiredMinPicker(false);
                      setMinCTC(getCtcLowerValue(val[0]));
                      if (val.length > 0)
                        setValidMinCTC(true);
                      else
                        setValidMinCTC(false)
                    }}
                  />
                </TextInputLayout>
              </View>
              {/* 
            Desired Role 
             */}
              <View style={styles.eachField}>
                <TextInputLayout labelText={"Desired Role"}>
                  <ItemsSelectTwo
                    prefetchedValue={!isEmpty(selectedRoles) ? selectedRoles : null}
                    isSelectSingle={false}
                    style={{ borderRadius: 5, fontSize: 12, }}
                    colorTheme={'#F14836'}
                    popupTitle="Select Role"
                    title="Desired Role"
                    cancelButtonText="Cancel"
                    selectButtonText="Select"
                    searchPlaceHolderText="Eg. Manager"
                    data={roleOptions}
                    onSelect={val => {
                      let roles = [];
                      for (var i in val) {
                        roles.push(getRolesLabel(val[i]));
                      }
                      setSelectedRoles(roles);
                      if (val.length > 0)
                        setValidRole(true);
                      else
                        setValidRole(false)
                    }}
                    onRemoveItem={val => {
                      setSelectedRoles('');
                      if (val.length > 0)
                        setValidRole(true);
                      else
                        setValidRole(false)
                    }}
                  />
                </TextInputLayout>
              </View>

              {/* 
            Desired Industry 
             */}
              <View style={styles.eachField}>
                <TextInputLayout labelText={"Desired Industry"}>
                  <ItemsSelectTwo
                    prefetchedValue={!isEmpty(selectedIndustries) ? selectedIndustries : null}
                    isSelectSingle={false}
                    style={{ borderRadius: 5, fontSize: 12, }}
                    colorTheme={'#F14836'}
                    popupTitle="Select Industry"
                    title="Desired Industry"
                    cancelButtonText="Cancel"
                    selectButtonText="Select"
                    searchPlaceHolderText="Eg. Ed Tech"
                    data={industryOptions}
                    onSelect={val => {
                      let industries = [];
                      for (var i in val) {
                        industries.push(getIndustryLabel(val[i]));
                      }
                      setSelectedIndustries(industries);
                      if (val.length > 0)
                        setValidIndustry(true);
                      else
                        setValidIndustry(false)
                    }}
                    onRemoveItem={val => {
                      setSelectedIndustries('');
                      if (val.length > 0)
                        setValidIndustry(true);
                      else
                        setValidIndustry(false)
                    }}
                  />
                </TextInputLayout>
              </View>

            </View>
          </ScrollView>
        </View>
        {/* for space so that scrollview doesn't collapse with button */}
        <View style={{ flex: 1, }}>
          <Text> </Text>
        </View>
        <View style={styles.materialButton}>
          <Button
            disabled={
              !(isValidCTC &&
                isValidLocation &&
                isValidMinCTC &&
                isValidRole &&
                isValidIndustry)
            }
            raised
            title={nextButtonName}
            buttonStyle={{ justifyContent: 'center', zIndex: 2, borderRadius: 6, height: 45, paddingHorizontal: 8, backgroundColor: '#f14836', }}
            titleStyle={{ fontWeight: '200', fontSize: 16, textAlignVertical: 'center' }}
            onPress={() => {
              onSubmitPressHandler();
            }}
          />
        </View>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  eachField: {
    marginHorizontal: 20,
    marginTop: 20,
  },
  container: {
    flex: 1,
    // justifyContent: 'center',
    margin: 16,
    marginTop: 4,
  },
  textInputLayoutContainer: {
    flex: 12,
    justifyContent: 'center',
  },
  button: {
    position: 'absolute',
    bottom: 0,
    fontSize: 24,
  },
  materialButton: {
    position: 'absolute',
    bottom: 0,
    fontSize: 60,
    paddingHorizontal: 20,
    justifyContent: 'flex-end',
    width: '100%',
    flex: 1,
  },
  textInput: {
    fontSize: 16,
    // width: "95%",
    height: 40,
    // borderWidth: 1,
    // borderColor: 'gray',
    // textAlign: 'center',
  },
  textTitle: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '900',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionTitle: {
    fontSize: 24,
    justifyContent: 'center',
    flex: 1,
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
