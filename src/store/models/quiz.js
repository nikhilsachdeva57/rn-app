import { thunk, action } from 'easy-peasy';
import { isEmpty } from 'lodash-es';
import {
  getQuizDetails as getQuizDetailsAPI,
  getQuestionDetails as getQuestionDetailsAPI,
  insertQuizTrack as insertQuizTrackAPI,
  updateQuizTrack as updateQuizTrackAPI,
  insertQuestionTrack as insertQuestionTrackAPI,
} from '../../restApi';
import camelizeKeys from '../../utils/camelizeKeys';

const morphedQuizResponse = (details) => {
  const getQuiz = details.data.getQuiz;
  const sectionWiseQuestionSlugs = getQuiz.sections[0].fixedQuestions;
  const questionSlugs = sectionWiseQuestionSlugs;
  const totalQuestions = questionSlugs.length
  const requiredObj = {
    ...getQuiz,
    quizSlug: getQuiz.slug,
    totalQuestions: totalQuestions,
    sectionWiseQuestionSlugs: sectionWiseQuestionSlugs,
    questionSlugs: questionSlugs
  };
  return requiredObj;
};

const morphedQuestionObjResponse = (details) => {
  const { getQuestion } = details.data;
  getQuestion[0].options = getQuestion[0].options.map((option, index) => {
    option.id = index;
    return option;
  });
  const requiredObj = {
    [getQuestion[0].slugId]: getQuestion[0]
  };
  return requiredObj;
};

export default {
  id: '',
  quizSlug: '',
  attemptId: '',
  sections: [],
  totalQuestions: 0,
  sectionWiseQuestionSlugs: [],
  questionSlugs: [],
  questionsDetails: {},
  selectedAnswers: {},
  questionUniverse: [],
  currentSectionIndex: -1,
  currentQuestionIndex: -1,
  startTime: 0,
  error: '',

  getQuizDetails: thunk((actions, payload) => {
    return getQuizDetailsAPI(payload.quizSlug)
      .then((details) => {
        details = camelizeKeys(details);
        if (isEmpty(details.data.getQuiz)) {
          actions.updateQuizDetails({ error: 'Quiz not found' });
          return;
        }
        const quizObj = morphedQuizResponse(details);
        return actions.updateQuizDetails(quizObj);
      }, (error) => {
        console.log('Error in GetQuizDetailsAPI:', error);
      });
  }),

  getQuestionDetails: thunk((actions, payload) => {
    console.log(payload)
    return getQuestionDetailsAPI(payload.questionSlug)
      .then((details) => {
        details = camelizeKeys(details);
        const questionObj = morphedQuestionObjResponse(details);
        return actions.updateQuestionsDetails(questionObj);
        
      }, (error) => {
        console.log('Error in GetQuestionDetailsAPI:', error);
      });
  }),

  insertQuizTrack: thunk((actions, payload) => {
    return insertQuizTrackAPI(payload)
      .then((details) => {
        details = camelizeKeys(details);
        console.log(details)
        if (details.data.insertQuizTrack.id) {
          const attemptId = details.data.insertQuizTrack.id;
          actions.updateQuizDetails({ attemptId });
        }
      }, (error) => {
        console.log('Error in insertQuizTrack:', error);
      });
  }),

  updateQuizTrack: thunk((actions, payload) => {
    return updateQuizTrackAPI(payload)
      .then((details) => {
        // actions.updateQuestionsDetails(details);
      }, (error) => {
        console.log('Error in updateQuizTrack:', error);
      });
  }),

  insertQuestionTrack: thunk((actions, payload) => {
    console.log(payload)
    return insertQuestionTrackAPI(payload)
      .then((details) => {
        // Do we need to do something here?
      }, (error) => {
        console.log('Error in insertQuestionTrack:', error);
      });
  }),

  updateQuizDetails:  action((state, payload) => {
    return {
      ...state, ...payload
    }
  }),

  updateSelectedAnswers: action((state, payload) => {
    console.log(payload)
    return {
      ...state,
      selectedAnswers: {
        ...state.selectedAnswers,
        ...payload  
      }
    };
  }),

  updateQuestionsDetails: action((state, payload) => {
    return {
      ...state,
      questionsDetails: {
        ...state.questionsDetails,
        ...payload,
      }
    };
  }),
};
