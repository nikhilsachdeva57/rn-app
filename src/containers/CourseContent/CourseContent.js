import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import WebView from 'react-native-webview';

import { useStoreState, useStoreActions } from 'easy-peasy';
import { changeCaseFirstLetter } from '../../utils/firstCapital';
import { isEmpty } from 'lodash-es';
import { ScrollView } from 'react-native-gesture-handler';
import QuizLanding from '../QuizLanding/QuizLanding';

export default function CourseContent(props) {
  const courseData = useStoreState(state => state.course);
  const sectionDetails = courseData.sectionDetails;
  const [courseSlug, setCourseSlug] = useState(
    props.navigation.state.params.course_slug,
  );

  const [sectionSlug, setSectionSlug] = useState(
    props.navigation.state.params.session_slug,
  );
  const [nextSessionData, setNextSessionData] = useState({ slug: 'complete' });
  const [isNextDisabled, setIsNextDisabled] = useState(false);

  const user = useStoreState(state => state.user);

  const startTime = Date.now();
  const {
    getCourseDetails,
    getSectionDetails,
    sectionStartTime,
    insertCourseTrack,
  } = useStoreActions(actions => actions.course);
  const { getCandidateStates, insertCandidateStateTrack } = useStoreActions(
    actions => actions.user,
  );

  //getCandidateStates gets the states of candidate (onboarding, av_response, training etc (6 states) check db)
  useEffect(() => {
    if (!isEmpty(user) && isEmpty(user.next_state)) {
      getCandidateStates({ candidate_id: user.id });
    }
  });

  useEffect(() => {
    if (!sectionSlug) {
      return;
    }
    if (courseData.sectionDetails.slug != sectionSlug) {
      getSectionDetails({ sectionSlug });
      findNextSession();
    }
    // handleCurrentSession(sectionSlug)
  }, [sectionSlug]);

  useEffect(() => {
    if (isEmpty(courseData.id)) {
      getCourseDetails({ courseSlug });
    }
  }, []);

  useEffect(() => {
    setSectionSlug(props.navigation.state.params.session_slug);
  }, [props.navigation.state.params]);
  const findNextSession = () => {
    let found = false;
    for (let section of courseData.sections) {
      if (found) {
        return setNextSessionData(section);
      }
      if (section.slug == sectionSlug) {
        found = true;
      }
    }
  };

  // youtube next link button, handles time limit by setting a constant one, even if video not played, timer will wait till
  // a certain time elapse

  const handleNextLink = () => {
    // updateCourseTrack({sectionSlug, courseSlug})

    if (Date.now() - startTime > sectionDetails.estimated_time_in_mins * 8000) {
      sectionStartTime({ courseData, next_session_slug: nextSessionData.slug });
      insertCourseTrack({
        candidate_id: user.id,
        course_slug: courseSlug,
        section_slug: sectionSlug,
      });
      props.navigation.navigate('CourseContent', {
        course_slug: courseData.course_slug,
        session_slug: nextSessionData.slug,
      });
      return null;
    } else {
      window.alert('Watch complete video in one snap first! ');
    }
  };

  // handles the completion of training

  const handleCompleteBtnClick = () => {
    insertCandidateStateTrack({
      candidate_state_id: user.next_state.id,
      status: 'completed',
      next_candidate_state_id: user.upcoming_states[0]
        ? user.upcoming_states[0].id
        : null,
      current_action: 'training',
    }).then(details => {
      props.navigation.navigate('Prepare');
      return null;
    });
  };

  const sectionContent = data => {
    if (data.content_type == 'video') {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-around',
          }}>
          <View style={{ flex: 2, justifyContent: 'center' }}>
            <Text style={styles.instructionsHeading}>
              {changeCaseFirstLetter(data.title)}
            </Text>
          </View>

          <View style={styles.videoContainer}>

            <WebView
              allowsFullscreenVideo={true}
              style={{ margin: 20 }}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              source={{
                uri: `${data.video.url}?modestbranding=1&amp;autoplay=1&amp;rel=0&amp;showinfo=0&amp;enablejsapi=1`,
              }}
            />
          </View>

          <View
            style={{
              flex: 3,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}
              isDisabled={isNextDisabled}
              onPress={() => handleNextLink()}>
              <View
                style={{
                  flex: 0.25,

                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text style={styles.nextSession}>Next Session</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    // content quiz starts here

    if (data.content_type == 'quiz') {
      return (
        <QuizLanding
          courseSlug={courseSlug}
          sectionSlug={sectionSlug}
          quizSlug={data.quiz.slug}
        />
      );
    }
  };

  if (sectionSlug == 'complete') {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
        }}>
        <View
          style={{
            margin: 20,
          }}>
          <Text style={styles.instructionsHeading}>
            {changeCaseFirstLetter(courseData.title)}
          </Text>
        </View>
        <View>
          <Text style={styles.description}>
            WOW, You have completed all sessions
          </Text>
        </View>

        <View>
          <TouchableOpacity
            isDisabled={isNextDisabled}
            onPress={handleCompleteBtnClick}>
            <View>
              <Text style={styles.nextSession}>Submit </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  if (!sectionDetails || sectionDetails.slug != sectionSlug) {
    return (
      <View style={{ flex: 1 }}>
        <ActivityIndicator
          size="large"
          style={{ flex: 1, alignSelf: 'center' }}
          color="#ee6002"
        />
      </View>
    );
  }
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'rgb(240,240,240)',
        justifyContent: 'space-around',
      }}>

      <View style={{ flex: 1, justifyContent: 'space-around' }}>
        {sectionContent(sectionDetails)}
      </View>

    </View>
  );
}
const { height, width } = Dimensions.get('window');
const styles = StyleSheet.create({
  videoContainer: {
    flex: 10,
    width: width,
    height: height,
    justifyContent: 'space-around',
  },
  instructionsHeading: {
    fontFamily: 'ProductSans-Regular',
    fontSize: 24,
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
  },
  description: {
    fontFamily: 'ProductSans-Regular',
    fontSize: 20,
    margin: 20,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
  },
  nextSession: {
    color: 'white',
    margin: 20,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    width: width / 3,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#f14836',
  },
  video: {
    flex: 1,

    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
});
