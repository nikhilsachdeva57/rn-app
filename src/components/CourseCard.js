import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

function CourseCard(props) {
  return (
    <View style={styles.courseCardView}>
      <View style={{flex: 2}}>
        <Image source={{uri: props.imageUri}} style={styles.image} />
      </View>
      <View style={{flex: 1, paddingLeft: 10, paddingTop: 10}}>
        <Text>{props.name}</Text>
      </View>
    </View>
  );
}

export default CourseCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  courseCardView: {
    height: 130,
    width: 130,
    marginLeft: 20,
    borderWidth: 0.5,
    borderColor: '#dddddd',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
  },
});
