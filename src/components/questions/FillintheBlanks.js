import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Card,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {useStoreState, useStoreActions} from 'easy-peasy';
import {isEmpty} from 'lodash-es';

const FillintheBlanks = props => {
  const [textBoxHeight, setTextBoxHeight] = useState(40);

  const updateSize = height => {
    setTextBoxHeight(height);
  };
  // trimming answers to lower case
  const handleOnChange = ev => {
    let answerObj = {};
    const userAnswer = ev.trim().toLowerCase();
    const correctAnswer = props.options.filter(
      option => option.text.trim().toLowerCase() === userAnswer,
    );
    if (isEmpty(correctAnswer)) {
      answerObj = {
        optionId: 0,
        answerContent: userAnswer,
        is_correct: false,
      };
    } else {
      answerObj = {
        optionId: 0,
        answerContent: userAnswer,
        is_correct: true,
      };
    }
    props.handleAnswerChanged(answerObj);
  };
  let textInput = {
    fontSize: 16,
    margin: 10,
    alignSelf: 'center',
    height: textBoxHeight,
    lineHeight: 18,
  };
  return (
    <View style={styles.fibView}>
      {/* textInput style is controlled by state, textbox increases if word increase */}
      <TextInput
        placeholder="Eg: Anything"
        style={textInput}
        multiline={true}
        onChangeText={text => {
          handleOnChange(text);
        }}
        onContentSizeChange={e => updateSize(e.nativeEvent.contentSize.height)}
      />
    </View>
  );
};

const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  fibView: {
    alignSelf: 'center',
    borderColor: 'black',
    borderWidth: 0.4,
    width: width - 40,
    borderRadius: 10,
  },
});

export default FillintheBlanks;
