import {
  GET_QUIZ_BY_SLUG,
  INSERT_QUIZ_TRACK,
  UPDATE_QUIZ_TRACK,
} from './../helpers/queries';
import {graphQlClient} from './../helpers/graphQlAPIClient';

export function getQuizDetails(quizSlug) {
  return graphQlClient.query({
    query: GET_QUIZ_BY_SLUG,
    variables: {
      slug: quizSlug,
    },
  });
}

export function insertQuizTrack(variables) {
  return graphQlClient.mutate({
    mutation: INSERT_QUIZ_TRACK,
    variables,
  });
}

export function updateQuizTrack(variables) {
  return graphQlClient.mutate({
    mutation: UPDATE_QUIZ_TRACK,
    variables,
  });
}
