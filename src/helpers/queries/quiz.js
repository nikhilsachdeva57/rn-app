import gql from 'graphql-tag';

export const GET_QUIZ_BY_SLUG = gql`
  query getQuizBySlug($slug: String!) {
    get_quiz(slug: $slug) {
      id
      slug
      sections
    }
  }
`;

export const INSERT_QUIZ_TRACK = gql`
  mutation($slug: String!) {
    insert_quiz_track(slug: $slug) {
      id
    }
  }
`;

export const UPDATE_QUIZ_TRACK = gql`
  mutation(
    $id: ID!
    $score: Float
    $time_spent_in_ms: Int
    $attempt_info: JSON
    $is_completed: Boolean!
  ) {
    update_quiz_track(
      id: $id
      score: $score
      time_spent_in_ms: $time_spent_in_ms
      attempt_info: $attempt_info
      is_completed: $is_completed
    ) {
      id
    }
  }
`;
