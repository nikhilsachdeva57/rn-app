import React, {Component, useEffect} from 'react';
import {StyleSheet, View, Text, ActivityIndicator} from 'react-native';

export default OnBoardingLanding = props => {
  const {id, onboarding_config, current_action} = props.navigation.state.params;

  const getOnBoardingState = config => {
    if (config == 'basic_info_1') return 'BasicInfo';
  };
  useEffect(() => {
    console.log(onboarding_config.length);
    props.navigation.navigate(getOnBoardingState(onboarding_config[0]), {
      id: id,
      onboarding_config: onboarding_config,
      state_index: 0,
      current_action: current_action,
    });
  });
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
      }}>
      <ActivityIndicator size="large" color="#ee6002" />
    </View>
  );
};

const styles = StyleSheet.create({
  topText: {
    marginTop: 30,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '700',
  },
  input: {
    marginTop: 40,
    margin: 15,
    height: 45,
    borderWidth: 1,
    paddingLeft: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 30,
    width: 120,
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: '#2196F3',
  },
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white',
  },
});
