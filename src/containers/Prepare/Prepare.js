import React, {Component, useState, useEffect} from 'react';
import {isEmpty} from 'lodash-es';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  RefreshControl,
  Dimensions,
} from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useStoreActions, useStoreState} from 'easy-peasy';
import {Card, Button} from 'react-native-elements';

export default Prepare = props => {
  const [currentPosition, setCurrentPosition] = useState();
  const [emptyState, setEmptyState] = useState(true);
  const [loading, setLoading] = useState(true);
  const labels = [];
  let states = [];
  const [stepCount, setStepCount] = useState();
  const userData = useStoreState(state => state.user);
  const state = useStoreState(state => state);
  const [stateFetched, setStateFetched] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [singleState, setSingleState] = useState(true);
  const [jobTitle, setJobTitle] = useState();
  const [onBoardingStarted, setOnboardingStarted] = useState(false);

  const {completed_states, next_state, upcoming_states} = userData;
  const {getCandidateStates, insertCandidateStateTrack} = useStoreActions(
    actions => actions.user,
  );

  //TODO: worst code below , hack , remove it imp!!!
  useEffect(() => {
    setRefreshing(true);
    function hack() {
      setTimeout(() => {
        getCandidateStates({
          candidate_id: userData.id,
        });
      }, 2000);
    }
    hack();
  }, []);
  useEffect(() => {
    function load_later() {
      setTimeout(() => {
        setRefreshing(false);
      }, 3000);
    }
    load_later();
  }, []);

  // TODO: remove the code above

  useEffect(() => {
    console.log('st', states, labels);

    //TODO: this code should be changed fully
    if (!isEmpty(userData.id) && !stateFetched && !refreshing) {
      console.log('fetching', stateFetched, userData.id);
      getCandidateStates({
        candidate_id: userData.id,
      });
      states = [];
      setStateFetched(true);
    }

    // console.log('lifecycle_state', next_state)
    // if (!isEmpty(next_state.lifecycle_state)) {
    //   if (next_state.lifecycle_state.state.action.action_type == "onboarding" && !onBoardingStarted) {
    //     console.log('onboarding started')
    //     handleStateNavigation(next_state, "active");
    //     setOnboardingStarted(true)
    //   }
    // }

    if (!isEmpty(completed_states)) {
      for (var i in completed_states) {
        if (completed_states[i].lifecycle_state) {
          states.push(completed_states[i]);
          labels.push(completed_states[i].lifecycle_state.state.name);
        }
        if (isEmpty(jobTitle) && completed_states[i].lifecycle_state) {
          setJobTitle(
            completed_states[i].lifecycle_state.lifecycle.job.job_title,
          );
        }
      }
    }
    // which screen to show
    if (!isEmpty(next_state)) {
      setLoading(false);
    }
    if (!isEmpty(next_state.id)) {
      setEmptyState(false);
    }

    setCurrentPosition(states.length);
    if (!isEmpty(next_state)) {
      if (!isEmpty(next_state.message)) {
        states.push(next_state);
        labels.push(next_state.message);
      } else if (!isEmpty(next_state.lifecycle_state)) {
        states.push(next_state);
        labels.push(next_state.lifecycle_state.state.name);
        if (isEmpty(jobTitle)) {
          setJobTitle(next_state.lifecycle_state.lifecycle.job.job_title);
        }
      }
    }

    if (!isEmpty(upcoming_states)) {
      for (var i in upcoming_states) {
        states.push(upcoming_states[i]);
        labels.push(upcoming_states[i].lifecycle_state.state.name);
        if (isEmpty(jobTitle)) {
          setJobTitle(
            upcoming_states[i].lifecycle_state.lifecycle.job.job_title,
          );
        }
      }
    }
    if (states.length === 1) {
      setSingleState(true);
    } else {
      setStepCount(states.length);
      setSingleState(false);
    }
  });

  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setStateFetched(false);
    wait(3500).then(() => {
      setRefreshing(false);
    });
  };

  function stepIndicatorView() {
    return (
      <StepIndicator
        customStyles={customStyles}
        currentPosition={currentPosition}
        labels={labels}
        renderLabel={view}
        stepCount={stepCount}
        direction="vertical"
      />
    );
  }

  function singleStateCard() {
    return (
      <View style={styles.step}>
        <View style={{flexDirection: 'row'}}>
          <Ionicons
            name={getIcon(next_state.lifecycle_state.state.action.action_type)}
            size={20}
            style={styles.stateIcon}
          />
          <Text style={styles.label}>
            {next_state.lifecycle_state.state.name}
          </Text>
        </View>

        <View
          style={{
            justifyContent: 'flex-start',
            alignSelf: 'flex-start',
            flex: 1,
          }}>
          <Text style={styles.detailText}>
            {next_state.lifecycle_state.state.info_text}
          </Text>
          {!isEmpty(states[0]) ? null : (
            <View
              style={{
                padding: 10,
                position: 'relative',
              }}>
              <TouchableOpacity
                onPress={() => handleStateNavigation(states[0], 'active')}
                style={styles.activeButton}>
                <Text
                  style={{
                    alignSelf: 'center',
                    justifyContent: 'center',
                    textAlign: 'center',
                    color: 'white',
                  }}>
                  Start
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    );
  }

  const getIcon = actionType => {
    //md-contact,md-videocam,md-rocket, md-calendar
    if (actionType == 'onboarding') {
      return 'md-contact';
    }
    if (actionType == 'av_response') {
      return 'md-videocam';
    }
    if (actionType == 'training') {
      return 'md-rocket';
    }
    if (actionType == 'mock_scheduling') {
      return 'md-calendar';
    }
    if (actionType == 'video_mock') {
      return 'md-videocam';
    }
  };

  const handleStateNavigation = (current_state, stepStatus) => {
    console.log('handlestate caled');
    if (stepStatus == 'unfinished') {
      ToastAndroid.show(
        'Complete previous actions to access this module.',
        ToastAndroid.SHORT,
      );
      return;
    }
    const actionType = current_state.lifecycle_state.state.action.action_type;

    insertCandidateStateTrack({
      candidate_state_id: current_state.id,
      status: 'started',
    });
    console.log('current_id', current_state.id);
    if (actionType == 'onboarding') {
      props.navigation.navigate('OnBoardingLanding', {
        id: current_state.id,
        onboarding_config:
          current_state.lifecycle_state.state.action.onboarding_config,
        current_action: 'onboarding',
      });
    }
    if (actionType == 'av_response') {
      props.navigation.navigate('QuizLanding', {
        quiz_slug: current_state.lifecycle_state.state.action.quiz_slug,
      });
    }
    if (actionType == 'training') {
      props.navigation.navigate('Classroom', {
        course_slug: current_state.lifecycle_state.state.action.course_slug,
      });
    }
    if (actionType == 'mock_scheduling') {
      props.navigation.navigate('ScheduleAppointment');
    }
    if (actionType == 'video_mock') {
      props.navigation.navigate('');
    }
  };

  const view = obj => {
    return (
      <View style={styles.step}>
        <View style={{flexDirection: 'row'}}>
          <Ionicons
            name={
              isEmpty(states[obj.position].lifecycle_state)
                ? null
                : getIcon(
                    states[obj.position].lifecycle_state.state.action
                      .action_type,
                  )
            }
            size={20}
            style={styles.stateIcon}
          />
          <Text style={styles.label}>{labels[obj.position]}</Text>
        </View>
        {obj.stepStatus === 'finished' ? (
          <View style={{flexDirection: 'row'}}>
            <Ionicons
              name="md-checkmark"
              size={30}
              color="#41c300"
              style={styles.icon}
            />
            <Text style={{padding: 10}}>
              Completed on :{' '}
              {isEmpty(states[obj.position].candidate_state_tracks)
                ? null
                : new Date(
                    states[obj.position].candidate_state_tracks[0].created_at,
                  ).toLocaleDateString()}
            </Text>
          </View>
        ) : (
          <View
            style={{
              justifyContent: 'flex-start',
              alignSelf: 'flex-start',
              flex: 1,
            }}>
            <Text style={styles.detailText}>
              {isEmpty(states[obj.position].lifecycle_state)
                ? null
                : states[obj.position].lifecycle_state.state.info_text}
            </Text>
            {!isEmpty(states[obj.position].message) ? null : (
              <View
                style={{
                  padding: 10,
                  position: 'relative',
                }}>
                <TouchableOpacity
                  onPress={() =>
                    handleStateNavigation(states[obj.position], obj.stepStatus)
                  }
                  style={
                    obj.stepStatus === 'unfinished'
                      ? styles.inactiveButton
                      : styles.activeButton
                  }>
                  <Text
                    style={{
                      alignSelf: 'center',
                      justifyContent: 'center',
                      textAlign: 'center',
                      color: 'white',
                    }}>
                    Start
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        )}
      </View>
    );
  };

  const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 5,
    stepStrokeCurrentColor: '#fe7013',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#aaaaaa',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 15,
    currentStepIndicatorLabelFontSize: 15,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
    labelColor: '#666666',
    labelSize: 15,
    currentStepLabelColor: '#fe7013',
  };

  return (
    <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
      {!refreshing ? (
        <View>
          <ScrollView
            style={{padding: 20, marginBottom: 90}}
            style={{padding: 20}}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }>
            <Text style={{fontSize: 15}}>
              {isEmpty(jobTitle)
                ? null
                : 'Your current active job application :'}
            </Text>
            <Text style={styles.jobTitle}>
              {isEmpty(jobTitle) ? null : jobTitle}
            </Text>
            <View>
              {loading ? (
                <ActivityIndicator size="large" color="#ee6002" />
              ) : emptyState ? (
                <View>
                  <Card containerStyle={styles.card}>
                    <Text style={styles.cardText}>
                      Your job hunt ends with 7Hires!
                    </Text>
                    <Text style={{padding: 10, fontSize: 17, color: '#ffddb0'}}>
                      {' '}
                      Our short, blended learning and assessment methods ensure
                      that candidates present their best-self at the interviews.
                      Our candidates have been proven to be 3x-5x more likely to
                      crack interviews!
                    </Text>
                    <View
                      style={{
                        padding: 10,
                        justifyContent: 'flex-end',
                        alignSelf: 'flex-end',
                      }}>
                      <TouchableOpacity
                        onPress={() => props.navigation.navigate('JobsList')}
                        style={styles.jobsButton}>
                        <Text
                          style={{
                            alignSelf: 'center',
                            justifyContent: 'center',
                            textAlign: 'center',
                            color: 'white',
                          }}>
                          View Jobs!
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </Card>
                </View>
              ) : singleState ? (
                singleStateCard()
              ) : (
                stepIndicatorView()
              )}
            </View>
            {/* Empty View for Bottom Nav */}
            <View style={{height: 30}} />
          </ScrollView>
        </View>
      ) : (
        <ActivityIndicator
          size="large"
          style={{flex: 1, alignSelf: 'center', justifyContent: 'center'}}
          color="#ee6002"
        />
      )}
    </View>
  );
};
const {height, width} = Dimensions.get('window');
const styles = StyleSheet.create({
  jobTitle: {
    fontSize: 20,
    fontWeight: '700',
  },
  jobsButton: {
    height: 50,
    borderColor: '#fff',
    width: 100,
    borderRadius: 25,
    backgroundColor: '#231266',
    justifyContent: 'center',
    alignSelf: 'flex-end',
  },
  cardText: {
    color: '#fff',
    fontSize: 20,
    shadowRadius: 100,
  },
  card: {
    borderRadius: 20,
    backgroundColor: '#F14836',
    height: 280,
  },
  detailText: {
    height: 55,
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
    fontSize: 12,
    padding: 5,
  },
  activeButton: {
    height: 40,
    elevation: 3,
    width: 100,
    borderRadius: 25,
    backgroundColor: '#F14836',
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },
  inactiveButton: {
    height: 40,
    borderWidth: 1,
    borderColor: '#fff',
    width: 100,
    borderRadius: 25,
    backgroundColor: '#4e4e4e',
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },
  step: {
    elevation: 2,
    backgroundColor: '#fff2df',
    padding: 10,
    borderRadius: 20,
    marginTop: 15,
    height: 150,
    flex: 1,
    width: width * 0.75,
    margin: 10,
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
    alignSelf: 'flex-start',
  },
  label: {
    width: '100%',
    alignSelf: 'flex-start',
    fontSize: 15,
    fontWeight: '700',
  },
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white',
  },
  icon: {
    padding: 10,
  },
  stateIcon: {
    paddingLeft: 5,
    paddingRight: 5,
  },
});
