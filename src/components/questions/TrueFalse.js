import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Card,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {useStoreState, useStoreActions} from 'easy-peasy';
import {isEmpty} from 'lodash-es';

const TrueFalse = props => {
  const handleSelect = props => {
    const {optionId, answerContent, is_correct, answerScore} = props;
    const answerObj = {
      optionId,
      answerContent,
      is_correct,
      score: answerScore,
    };
    if (!props.checkButtonClicked) {
      props.handleAnswerChanged(answerObj);
    }
  };

  return (
    <View style={{flex: 2, justifyContent: 'center'}}>
      <TouchableOpacity
        onPress={() => handleSelect(props)}
        key={props.answerType}
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: 'space-between',
        }}>
        <View
          style={[
            props.selectedAnswer[0] &&
            props.selectedAnswer[0].optionId == props.optionId
              ? styles.selectedAnswer
              : styles.questionView,
          ]}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Text style={{textAlign: 'center'}}>{props.answerContent}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  questionView: {
    height: 55,
    marginHorizontal: 20,
    marginTop: 6,
    borderRadius: 15,
    borderWidth: 0.5,
  },
  selectedAnswer: {
    height: 55,
    marginHorizontal: 20,
    marginTop: 6,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: '#f14836',
  },
});
export default TrueFalse;
