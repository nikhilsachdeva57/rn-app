import axios from 'axios';
import * as tokenUtils from './utils/tokenUtils';
import { REACT_APP_AUTHENTICATE } from 'react-native-dotenv'

class AxiosClient {
    constructor() {
        this.instance = axios.create({
            baseURL: "https://1p280wf0p3.execute-api.ap-south-1.amazonaws.com/prod",//should use env
            responseType: 'json',
            crossDomain: true,
        });
    }

    createOTP = (data) => {
        return new Promise((resolve, reject) => {
            this.instance.post('/create-otp', data)
                .then(resp => resolve(resp.data))
                .catch((error) => {
                    if (error.response && error.response.data) {
                        reject(error.response.data);
                    } else {
                        reject(new Error(error));
                    }
                });
        });
    }

    verifyOTP = (data) => {
        return new Promise((resolve, reject) => {
            this.instance.post('/verify-otp', data)
                .then((resp) => {
                    if (resp.data.token) {
                        tokenUtils.setToken(resp.data.token);
                        resolve(resp.data);
                    }
                    reject(new Error(resp.data.message));
                })
                .catch((error) => {
                    if (error.response && error.response.data) {
                        reject(error.response.data);
                    } else {
                        reject(new Error(error));
                    }
                });
        });
    }
}

export default new AxiosClient();
