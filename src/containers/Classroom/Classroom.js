import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { Button, Card } from 'react-native-elements';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { changeCaseFirstLetter } from '../../utils/firstCapital';
import { isEmpty } from 'lodash-es';
import { ScrollView } from 'react-native-gesture-handler';
export default props => {
  const courseData = useStoreState(state => state.course);
  const user = useStoreState(state => state.user);
  const [courseSlug, setCourseSlug] = useState(
    props.navigation.getParam('course_slug'),
  );
  const [loading, setLoading] = useState(true)
  //TODO: change this name to camelCase : lowpriority
  const [current_session, set_current_session] = useState(null)
  const {
    getCourseDetails,
    getCurrentSection,
    sectionStartTime,
  } = useStoreActions(actions => actions.course);
  // gets course details if not there
  useEffect(() => {
    if (!courseSlug) {
      return;
    }
    getCourseDetails({ courseSlug });
  }, []);
  // gets current section to attempt
  useEffect(() => {
    if (!isEmpty(user.id)) {
    }
    if (isEmpty(courseData.current_session) && !isEmpty(courseData.id)) {
      getCurrentSection({ courseData, candidate_id: user.id });
    }
    if (!isEmpty(courseData) && !isEmpty(courseData.current_session)) {
      set_current_session(courseData.current_session.slug == 'complete'
        ? courseData.sections[1]
        : courseData.current_session)
      setLoading(false)
    }
  }

  );
  // handlestart handles the start of test or after introduction, passes slug on buttonClick
  const handleStart = slug => {
    sectionStartTime({ courseData, next_session_slug: slug });
    props.navigation.navigate('CourseContent', {
      course_slug: courseData.course_slug,
      session_slug: slug,
    });
  };
  const handleCurriculum = () => {
    if (courseData.sections.length == 1) {
      return;
    }
    // quiz button on the classroom page is always disabled, can be accessed only after the video
    const cards = courseData.sections.map((section, index) => {
      const isDisabled = section => {
        if (section.content_type == 'quiz') {
          return true;
        }
        return !section.openable;
      };
      if (section.is_module) {
        return (
          <View style={styles.curriculumView} key={index}>
            <Text style={styles.curriculumTitle}>{`${changeCaseFirstLetter(
              section.title,
            )}. ${section.estimated_time_in_mins} mins`}</Text>
          </View>
        );
      }
      return (
        <View style={styles.curriculumViewBottom} key={index}>
          <TouchableOpacity
            disabled={isDisabled(section)}
            onPress={() => {
              handleStart(section.slug);
            }}>
            <Text style={styles.curriculumBottomText}>
              {`${section.sort_order}. ${changeCaseFirstLetter(section.title)}`}
            </Text>
            <Text style={styles.curriculumBottomText}>
              Duration:
              {section.estimated_time_in_mins} mins (
              {changeCaseFirstLetter(section.content_type)})
            </Text>
            <View>
              <View
                style={{
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}>
                {!isDisabled(section) ? (
                  <Text style={styles.startNowButton}>Start Now</Text>
                ) : (
                    <Text style={styles.startNowbuttonDisabled}>Start Now</Text>
                  )}
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    });
    return cards;
  };
  // jsx of session (current)
  const currentSessionView = () => {
    // const current_session =
    //   courseData.current_session.slug == 'complete'
    //     ? courseData.sections[1]
    //     : courseData.current_session;
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            handleStart(current_session.slug);
          }}>
          <Card
            containerStyle={styles.card}
            dividerStyle={styles.dividerStyle}
            titleStyle={styles.titleStyle}
            imageStyle={styles.imageStyle}
            title={`${current_session.sort_order}. ${current_session.title}`}>
            <Text style={{ marginBottom: 5, width: 200 }}>
              Duration : {current_session.estimated_time_in_mins} mins. (
              {changeCaseFirstLetter(current_session.content_type)})
            </Text>
            <View>
              <Text style={styles.description}>
                {changeCaseFirstLetter(current_session.description)}
              </Text>
            </View>
            {/* <Button onPress={() => handleStart(current_session.slug)}></Button> */}
            <View>
              <View
                style={{
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}>
                <Text style={styles.startNowButton}>Start Now</Text>
              </View>
            </View>
          </Card>
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <View style={{ flex: 1 }}>
      {!loading ? (<View>
        <ScrollView>
          <View style={styles.mainView}>
            <View style={styles.textTitle}>
              <Text style={styles.instructionsHeading}> Next Active Session</Text>
            </View>
            {/* <View style={styles.courseCurriculum}>
        <Button title="Course Curriculum" />
      </View> */}
            <View style={styles.containerCard}>{currentSessionView()}</View>
            <View style={styles.textTitle}>
              <Text style={styles.instructionsHeading}> Course Curriculum</Text>
            </View>
            <View style={styles.containerCard}>{handleCurriculum()}</View>
          </View>
        </ScrollView>
      </View>) : <ActivityIndicator
          size="large"
          style={{ flex: 1, alignSelf: 'center' }}
          color="#ee6002"
        />}
    </View>
  );
};
const { height, width } = Dimensions.get('window');
const styles = StyleSheet.create({
  buttonNext: {
    color: '#f14836',
  },
  buttonView: {},
  startNowButton: {
    color: 'white',
    marginTop: 15,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    width: 120,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#f14836',
  },
  startNowbuttonDisabled: {
    color: 'rgb(158, 158, 158)',
    marginTop: 15,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    width: 120,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#f14836',
    borderColor: 'rgb(204, 202, 202)',
    backgroundColor: 'rgb(237, 185, 185)',
  },
  containerCard: {
    flex: 3,
  },
  curriculumTitle: {
    marginTop: 13,
    paddingLeft: 10,
    alignSelf: 'flex-start',
  },
  curriculumView: {
    backgroundColor: 'rgb(240,240,240)',
    borderColor: 'rgba(0,0,0,.125)',
    marginLeft: 20,
    marginRight: 20,
    height: 47,
    borderWidth: 0.5,
    borderRadius: 5,
  },
  curriculumViewBottom: {
    borderColor: '1 px solid rgba(0,0,0,.125)',
    marginLeft: 20,
    marginRight: 20,
    padding: 20,
    borderWidth: 0.5,
    backgroundColor: 'rgb(252, 252, 252)',
  },
  curriculumBottomText: {
    paddingLeft: 10,
    alignSelf: 'flex-start',
  },
  dividerStyle: {
    backgroundColor: '#bbbb',
  },
  textTitle: {
    flex: 0.8,
    justifyContent: 'center',
  },
  mainView: {
    backgroundColor: 'rgb(244, 244, 244)',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  courseCardView: {
    height: 0.75 * height,
    width: 0.75 * width,
    marginLeft: 20,
    borderWidth: 0.5,
    borderRadius: 25,
    borderColor: '#dddddd',
  },
  card: {
    borderRadius: 10,
  },
  description: {},
  titleStyle: {
    alignSelf: 'flex-start',
    fontFamily: 'nunitoRegular',
  },
  instructionsHeading: {
    fontFamily: 'ProductSans-Regular',
    fontSize: 24,
    marginTop: 4,
    marginBottom: 15,
    marginHorizontal: 24,
    paddingTop: 20,
  },
});