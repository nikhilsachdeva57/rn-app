import { thunk, action } from 'easy-peasy';
import { isEmpty } from 'lodash-es';
import { getTokenData } from '../../utils/tokenUtils';

import {
  getNoticePeriod as getNoticePeriodAPI,
  getDegree as getDegreeAPI,
  getCandidateResume as getCandidateResumeAPI,
  getRoles as getRolesAPI,
  getCtcRange as getCtcRangeAPI,
  getIndustries as getIndustriesAPI,
  getUserMobile as getUserMobileAPI,
  getCandidateStates as getCandidateStatesAPI,
  insertCandidateResume as insertCandidateResumeAPI,
  insertCandidateBasicInfo as insertCandidateBasicInfoAPI,
  insertCandidateEducationInfo as insertCandidateEducationInfoAPI,
  insertCandidate12thInfo as insertCandidate12thInfoAPI,
  insertCandidate10thInfo as insertCandidate10thInfoAPI,
  insertCandidateCurrentEmploymentInfo as insertCandidateCurrentEmploymentInfoAPI,
  insertCandidateWorkExperienceInfo as insertCandidateWorkExperienceInfoAPI,
  insertCandidatePreferenceInfo as insertCandidatePreferenceInfoAPI,
  insertCandidateInternshipInfo as insertCandidateInternshipInfoAPI,
  getCandidateBasicInfo as getCandidateBasicInfoAPI,
  getCandidateEducationInfo as getCandidateEducationInfoAPI,
  getCandidate12thInfo as getCandidate12thInfoAPI,
  getCandidate10thInfo as getCandidate10thInfoAPI,
  getCandidateCurrentEmploymentInfo as getCandidateCurrentEmploymentInfoAPI,
  getCandidateWorkExperienceInfo as getCandidateWorkExperienceInfoAPI,
  getCandidatePreferenceInfo as getCandidatePreferenceInfoAPI,
  getCandidateInternshipInfo as getCandidateInternshipInfoAPI,
  getCities as getCitiesAPI,
  getProfileInfo as getProfileInfoAPI,
  getWorkExperience as getWorkExperienceAPI,
  getInternshipDetail as getInternshipDetailAPI,
  getUserMobile,
  insertCandidateStateTrack as insertCandidateStateTrackAPI,
  getCandidateJobs as getCandidateJobsAPI,
  assignJob as assignJobAPI,
} from '../../restApi/user';

const morphedStates = states => {
  if (!isEmpty(states)) {
    const completed_states = states.filter((state, index) => {
      return state.status == 'completed';
    });
    const upcoming_states = states.filter((state, index) => {
      return state.status == 'created';
    });
    let next_state = states.filter((state, index) => {
      return state.status == 'started' || state.status == 'scheduled';
    });
    next_state = next_state[0]
      ? next_state[0]
      : { message: 'wait for sometime to know your result', id: '1' };
    return {
      completed_states,
      upcoming_states,
      next_state,
    };
  } else {
    let next_state = { message: 'No states present.' };
    return {
      next_state,
    };
  }
};

export default {
  id: '',
  mobile: '',

  is_profile_fetched: false,

  basic_info: '',
  candidate_bachelor_degree_info: '',
  candidate_12th_info: '',
  candidate_10th_info: '',
  candidate_current_work_info: '',
  candidate_work_experience_info: [],
  candidate_preference_info: '',
  candidate_internship_info: [],
  candidate_jobs: [],

  error: '',

  next_state: '',
  upcoming_states: '',
  completed_states: '',

  cities: [],

  getUserId: thunk(async (actions, payload) => {
    const data = await getTokenData();
    if (!data) {
      return actions.updateUserDetails({ error: true });
    }
    if (window.Sentry) {
      window.Sentry.configureScope(scope => {
        scope.setUser({ id: action.data.id, username: action.data.name });
      });
    }
    return actions.updateUserDetails({
      id: data.sub,
      name: data.name,
    });
  }),

  getCandidateStates: thunk((actions, payload) => {
    return getCandidateStatesAPI(payload.candidate_id).then(
      details => {
        const states = details.data.candidate_candidate_state
          ? details.data.candidate_candidate_state
          : [];
        let obj = morphedStates(states);
        return actions.updateUserDetails(obj);
      },
      error => {
        console.log(
          'Error in getCandidateStatesAPI:',
          error.networkError.result.errors,
        );
      },
    );
  }),

  getCandidateResume: thunk((actions, payload) => {
    return getCandidateResumeAPI(payload).then(
      details => {
        return actions.updateUserDetails(details.data);
      });
  }),

  getCities: thunk((actions, payload) => {
    return getCitiesAPI().then(
      details => {
        return actions.updateUserDetails(details.data);
      },
    )
  }),

  getRoles: thunk((actions, payload) => {
    return getRolesAPI().then(
      details => {
        return actions.updateUserDetails(details.data);
      },
      error => { },
    );
  }),
  getIndustries: thunk((actions, payload) => {
    return getIndustriesAPI().then(details => {
      return actions.updateUserDetails(details.data);
    });
  }),

  getDegree: thunk((actions, payload) => {
    return getDegreeAPI().then(
      details => {
        return actions.updateUserDetails(details.data);
      }
    )
  }),

  getNoticePeriod: thunk((actions, payload) => {
    return getNoticePeriodAPI().then(
      details => {
        return actions.updateUserDetails(details.data);
      }
    )
  }),

  getCtcRange: thunk((actions, payload) => {
    return getCtcRangeAPI().then(details => {
      return actions.updateUserDetails(details.data);
    });
  }),

  insertCandidateBasicInfo: thunk((actions, payload) => {
    return insertCandidateBasicInfoAPI(payload).then(details => {
      const basic_info =
        details.data.insert_candidate_candidate_basic_info.returning[0];
      return actions.updateUserDetails({ basic_info });
    });
  }),

  insertCandidateResume: thunk((actions, payload) => {
    return insertCandidateResumeAPI(payload).then((details) => {
      const candidate_resume = details.data.insert_candidate_candidate_resume.returning[0];
      return actions.updateUserDetails({ candidate_resume });
    });
  }),

  getUserMobile: thunk((actions, payload) => {
    return getUserMobileAPI(payload).then(
      details => {
        const mobile = details.data.users_users[0].mobile;
        return actions.updateUserDetails({ mobile });
      },
      error => {
        console.log('error in getUserInfo', error);
      },
    );
  }),

  getCandidateBasicInfo: thunk((actions, payload) => {
    getCandidateBasicInfoAPI(payload).then(
      details => {
        const basic_info = details.data.candidate_candidate_basic_info[0];
        return actions.updateUserDetails({ basic_info });
      },
      error => {
        console.log('error in getCandidateBasicInfo', error);
      },
    );
  }),

  insertCandidateEducationInfo: thunk((actions, payload) => {
    return insertCandidateEducationInfoAPI(payload).then(details => {
      const candidate_bachelor_degree_info =
        details.data.insert_candidate_candidate_bachelor_degree_info
          .returning[0];
      return actions.updateUserDetails({ candidate_bachelor_degree_info });
    });
  }),

  getCandidateEducationInfo: thunk((actions, payload) => {
    getCandidateEducationInfoAPI(payload).then(details => {
      const candidate_bachelor_degree_info =
        details.data.candidate_candidate_bachelor_degree_info[0];
      console.log(candidate_bachelor_degree_info)
      return actions.updateUserDetails({ candidate_bachelor_degree_info });
    });
  }),

  insertCandidate12thInfo: thunk((actions, payload) => {
    return insertCandidate12thInfoAPI(payload).then(details => {
      const candidate_12th_info =
        details.data.insert_candidate_candidate_12th_info.returning[0];
      return actions.updateUserDetails({ candidate_12th_info });
    });
  }),

  getCandidate12thInfo: thunk((actions, payload) => {
    getCandidate12thInfoAPI(payload).then(details => {
      const candidate_12th_info = details.data.candidate_candidate_12th_info[0];
      return actions.updateUserDetails({ candidate_12th_info });
    });
  }),

  insertCandidate10thInfo: thunk((actions, payload) => {
    return insertCandidate10thInfoAPI(payload).then(details => {
      const candidate_10th_info =
        details.data.insert_candidate_candidate_10th_info.returning[0];
      return actions.updateUserDetails({ candidate_10th_info });
    });
  }),

  getCandidate10thInfo: thunk((actions, payload) => {
    getCandidate10thInfoAPI(payload).then(details => {
      const candidate_10th_info = details.data.candidate_candidate_10th_info[0];
      return actions.updateUserDetails({ candidate_10th_info });
    });
  }),

  insertCandidateCurrentEmploymentInfo: thunk((actions, payload) => {
    return insertCandidateCurrentEmploymentInfoAPI(payload).then(details => {
      const candidate_current_work_info =
        details.data.insert_candidate_candidate_current_work_info.returning[0];
      return actions.updateUserDetails({ candidate_current_work_info });
    });
  }),

  getCandidateCurrentEmploymentInfo: thunk((actions, payload) => {
    getCandidateCurrentEmploymentInfoAPI(payload).then(details => {
      const candidate_current_work_info =
        details.data.candidate_candidate_current_work_info[0];
      return actions.updateUserDetails({ candidate_current_work_info });
    });
  }),

  insertCandidateWorkExperienceInfo: thunk((actions, payload) => {
    return insertCandidateWorkExperienceInfoAPI(payload).then(details => {
      const candidate_work_experience_info =
        details.data.insert_candidate_candidate_work_experience_info
          .returning[0];
      return actions.updateUserDetails({ candidate_work_experience_info });
    });
  }),

  getCandidateWorkExperienceInfo: thunk((actions, payload) => {
    getCandidateWorkExperienceInfoAPI(payload).then(details => {
      const candidate_work_experience_info = !isEmpty(
        details.data.candidate_candidate_work_experience_info,
      )
        ? details.data.candidate_candidate_work_experience_info[0]
        : null;
      return actions.updateUserDetails({ candidate_work_experience_info });
    });
  }),

  insertCandidatePreferenceInfo: thunk((actions, payload) => {
    return insertCandidatePreferenceInfoAPI(payload).then(details => {
      const candidate_preference_info =
        details.data.insert_candidate_candidate_preference_info.returning[0];
      return actions.updateUserDetails({ candidate_preference_info });
    });
  }),

  getCandidatePreferenceInfo: thunk((actions, payload) => {
    getCandidatePreferenceInfoAPI(payload).then(details => {
      const candidate_preference_info =
        details.data.candidate_candidate_preference_info[0];
      return actions.updateUserDetails({ candidate_preference_info });
    });
  }),

  insertCandidateInternshipInfo: thunk((actions, payload) => {
    return insertCandidateInternshipInfoAPI(payload).then(details => {
      const candidate_internship_info =
        details.data.insert_candidate_candidate_internship_info.returning[0];
      return actions.updateUserDetails({ candidate_internship_info });
    });
  }),

  getCandidateInternshipInfo: thunk((actions, payload) => {
    getCandidateInternshipInfoAPI(payload).then(details => {
      const candidate_internship_info =
        details.data.candidate_candidate_internship_info;
      return actions.updateUserDetails({ candidate_internship_info });
    });
  }),

  getProfileInfo: thunk((actions, payload) => {
    getProfileInfoAPI(payload).then(details => {
      const profile_info = details.data.users_users
        ? details.data.users_users[0]
        : null;
      return actions.updateUserDetails({
        ...profile_info,
        is_profile_fetched: true,
      });
    });
  }),

  // getWorkExperience: thunk((actions, payload) => {
  //   getWorkExperienceAPI(payload).then(details => {
  //     const candidate_work_experience_info = !isEmpty(
  //       details.data.candidate_candidate_work_experience_info,
  //     )
  //       ? details.data.candidate_candidate_work_experience_info[0]
  //       : null;
  //     return actions.updateUserDetails({ candidate_work_experience_info });
  //   });
  // }),

  getInternshipDetail: thunk((actions, payload) => {
    getInternshipDetailAPI(payload).then(details => {
      const candidate_internship_info =
        details.data.candidate_candidate_internship_info;
      return actions.updateUserDetails({ candidate_internship_info });
    });
  }),

  getCandidateJobs: thunk((actions, payload) => {
    getCandidateJobsAPI(payload).then(details => {
      const candidate_jobs = details.data.getJobsForCandidate;
      return actions.updateUserDetails({ candidate_jobs });
    });
  }),

  updateUserDetails: action((state, payload) => {
    return {
      ...state,
      ...payload,
    };
  }),

  getCandidateStateById: thunk((actions, payload) => {
    return getCandidateStateByIdAPI(payload.id).then(
      details => {
        const next_state = !isEmpty(details.data.candidate_candidate_state)
          ? details.data.candidate_candidate_state[0]
          : [];
        return actions.updateUserDetails({ next_state });
      },
      error => {
        console.log('Error in getCandidateStatesAPIById:', error);
      },
    );
  }),

  insertCandidateStateTrack: thunk((actions, payload) => {
    console.log('payload icst', payload);
    return insertCandidateStateTrackAPI(
      payload.candidate_state_id,
      payload.status,
      payload.next_candidate_state_id,
      payload.current_action,
      payload.start_time,
      payload.end_time,
    ).then(
      details => {
        return true;
      },
      error => {
        console.log('Error in insertCandidateStateTrack:', error);
      },
    );
  }),

  assignJob: thunk((actions, payload) => {
    return assignJobAPI(payload.candidateId, payload.jobId).then(
      details => {
        return true;
      },
      error => {
        console.log('Error in assignJobAPI:', error);
      },
    );
  }),
};
