import {thunk, action} from 'easy-peasy';
import {isEmpty} from 'lodash-es';
import {
  getSectionList as getSectionListAPI,
  getSectionDetails as getSectionDetailAPI,
  candidateCourseTracks as candidateCourseTrackAPIS,
  insertCourseTrack as insertCourseTrackAPI,
} from '../../restApi';

const morphedCourseResponse = details => {
  const modules = details.data.course_courses[0]
    ? details.data.course_courses[0].modules
    : [];
  let sections = [];
  let total_sections = 0;
  let sort_order = 1;
  for (let m of modules) {
    sections.push({
      title: m.module.title,
      slug: m.module.slug,
      estimated_time_in_mins: m.module.estimated_time_in_mins,
      is_module: true,
      openable: true,
    });
    for (let section of m.module.sections) {
      total_sections += 1;
      let s = section.section;
      s.openable = true;
      s.is_module = false;
      s.course_slug = 'test-1';
      s.sort_order = sort_order;
      sort_order += 1;
      s.is_completed = false;
      // s.is_completed = s.course_tracks? s.course_tracks.length >0 : false
      sections.push(s);
    }
  }
  const requiredObj = {
    sections,
    course_slug: details.data.course_courses[0].slug,
    title: details.data.course_courses[0].title,
    id: details.data.course_courses[0].id,
    totalSections: total_sections,
  };
  return requiredObj;
};

const morphedModules = (details, payload) => {
  const last_completed_track =
    details.data.candidate_activities_course_track.length > 0
      ? details.data.candidate_activities_course_track[0]
      : null;
  let current_session = null;
  let found = false;
  const sections = payload.courseData.sections.map((section, index) => {
    if (section.content_type == 'quiz') section.openable = false;
    if (
      !last_completed_track &&
      !section.is_module &&
      isEmpty(current_session)
    ) {
      current_session = section;
    }
    if (found && !section.is_module && isEmpty(current_session)) {
      current_session = section;
    }
    if (
      last_completed_track &&
      last_completed_track.section_slug == section.slug
    ) {
      found = true;
    }
    if (!isEmpty(current_session)) {
      section.openable = false;
    }

    return section;
  });

  return {
    sections,
    current_session: current_session ? current_session : {slug: 'complete'},
  };
};

export default {
  id: '',
  course_slug: '',
  attemptId: '',
  sections: [],
  totalSections: 0,
  sectionDetails: '',
  current_session: '',
  next_session: '',
  error: null,

  getCourseDetails: thunk((actions, payload) => {
    return getSectionListAPI(payload.courseSlug).then(
      details => {
        if (isEmpty(details.data.course_courses)) {
          actions.updateCourseDetails({error: 'course not found'});
          return;
        }
        const courseObj = morphedCourseResponse(details);
        actions.updateCourseDetails(courseObj);
      },
      error => {
        console.log('Error in GetCourseDetailsAPI:', error);
      },
    );
  }),

  getCurrentSection: thunk((actions, payload) => {
    const sections = payload.courseData.sections;
    return candidateCourseTrackAPIS(
      payload.candidate_id,
      payload.courseData.course_slug,
    ).then(
      details => {
        const courseObj = morphedModules(details, payload);
        actions.updateCourseDetails(courseObj);
      },
      error => {
        console.log('Error in getCurrentSection:', error);
      },
    );
  }),

  getSectionDetails: thunk((actions, payload) => {
    return getSectionDetailAPI(payload.sectionSlug).then(
      details => {
        const questionObj = details.data.course_sections
          ? details.data.course_sections[0]
          : null;
        actions.updateSectionDetails(questionObj);
      },
      error => {
        console.log('Error in GetQuestionDetailsAPI:', error);
      },
    );
  }),

  sectionStartTime: thunk((actions, payload) => {
    let sections = payload.courseData.sections;
    for (let section of sections)
      if (section.slug == payload.next_session_slug) {
        section.startTime = Date.now();
        break;
      }
    actions.updateCourseDetails({sections: sections});
  }),

  insertCourseTrack: thunk((actions, payload) => {
    return insertCourseTrackAPI(payload).then(
      details => {
        // Do we need to do something here?
      },
      error => {
        console.log('Error in insertQuestionTrack:', error);
      },
    );
  }),

  updateCourseDetails: action((state, payload) => {
    return {
      ...state,
      ...payload,
    };
  }),

  updateSectionDetails: action((state, payload) => {
    return {
      ...state,
      sectionDetails: {
        ...state.sectionDetails,
        ...payload,
      },
    };
  }),
};
