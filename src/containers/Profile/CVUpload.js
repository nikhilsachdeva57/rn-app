import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
  ToastAndroid,
  Alert,
  PermissionsAndroid,
} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import { RNS3 } from 'react-native-aws3';
import Feather from 'react-native-vector-icons/Feather';
import { Card } from 'react-native-elements';
import Button from '../../components/Button';
import { useStoreActions, useStoreState } from 'easy-peasy';

export default CVUpload = props => {
  CVUpload.navigationOptions = {
    title: 'Upload your Resume',
  };
  const [resume, setResume] = useState('');
  const [isFileSelected, setFileSelected] = useState(false);
  const [isAnimating, setAnimating] = useState(false);
  const user = useStoreState(state => state.user);
  const { insertCandidateResume, getCandidateResume } = useStoreActions(actions => actions.user);

  useEffect(() => {
    return () => getCandidateResume(user.id);
  }, []);
  async function downloadFile() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Storage Permission',
          message: 'App needs access to memory to download the file ',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      } else {
        Alert.alert(
          'Permission Denied!',
          'You need to give storage permission to download the file',
        );
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async function selectFile() {
    downloadFile();
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });
      if (res.size < 5242880) {
        setResume(res);
        setFileSelected(true);
        showConfirmUploadAlert(res);
      } else {
        ToastAndroid.show(
          'Please select file with size less than 5MB',
          ToastAndroid.LONG,
        );
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        alert('Please select a file');
      } else {
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  }

  function showConfirmUploadAlert(file) {
    Alert.alert(
      'Confirm Upload',
      'File Name:' + file.name,
      [
        { text: 'Yes', onPress: () => uploadFile(file) },
        {
          text: 'No',
          onPress: () => {
            setFileSelected(false);
          },
        },
      ],
      { cancelable: false },
    );
  }

  const options = {
    keyPrefix: 'cv_upload/resume/' + user.id,
    bucket: '7hires-document',
    region: 'ap-south-1',
    accessKey: 'AKIAI4EHKL4W56OS6EBQ',
    secretKey: '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',
    successActionStatus: 201,
    mode: 'opaque',
  };

  function uploadFile(File) {
    const fileToUpload = {
      uri: File.uri,
      name: File.name,
      type: 'application/pdf',
    };
    setAnimating(true);
    RNS3.put(fileToUpload, options).then(response => {
      if (response.status == 201) {
        setAnimating(false);
        const variables = {
          candidate_id: user.id,
          file_name: File.name,
          download_url: response.body.postResponse.location,
        };
        console.log(
          'varibles',
          variables,
        );
        insertCandidateResume(variables).then(details => {
          ToastAndroid.show('Uploaded Successfully', ToastAndroid.SHORT);
          props.navigation.goBack();
        });
      } else {
        console.log('error uploading');
        throw new Error('Failed to upload image to S3');
        ToastAndroid.show('Please choose other file', ToastAndroid.SHORT);
      }
    });
  }
  return (
    <View style={styles.container}>
      <Card
        containerStyle={{
          borderRadius: 10,
          borderTopLeftRadius: 11,
          backgroundColor: 'transparent',
        }}
        wrapperStyle={{ borderColor: '#5DE289' }}>
        <View>
          <Text
            style={[styles.textStyle, { textAlign: 'center', marginBottom: 20 }]}>
            Please select file from your local storage{' '}
          </Text>
          <View style={{ alignSelf: 'center', marginTop: 8 }}>
            <Feather
              name="file-text"
              style={{ color: '#F14386', fontSize: 48 }}
            />
          </View>
          <Button
            title={'Open Files'}
            textColor={'white'}
            style={{ backgroundColor: '#f14836', marginTop: 6, marginBottom: 12 }}
            onPress={() => selectFile()}
          />
          <Text style={{ fontSize: 12, textAlign: 'center' }}>
            Supported File Types: .pdf
          </Text>
          <Text style={{ fontSize: 12, textAlign: 'center' }}>
            Max. File size allowed: 5MB
          </Text>

          <ActivityIndicator
            animating={isAnimating}
            size="large"
            style={{ flex: 1, alignSelf: 'center' }}
            color="#ee6002"
          />
          {isFileSelected ? (
            <View>
              <Text style={styles.textStyle}>
                File Name: {resume.name ? resume.name : ''}
                {'\n'}
              </Text>
            </View>
          ) : null}
        </View>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
    borderRadius: 8,
    backgroundColor: '#ffffff',
    padding: 16,
  },
  buttonStyle: {
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#DDDDDD',
    padding: 5,
  },
  textStyle: {
    backgroundColor: '#fff',
    fontSize: 15,
    marginTop: 16,
    color: 'black',
  },
  imageIconStyle: {
    height: 20,
    width: 20,
    resizeMode: 'stretch',
  },
  topText: {
    marginTop: 30,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '700',
  },
  input: {
    marginTop: 40,
    margin: 15,
    height: 45,
    borderWidth: 1,
    paddingLeft: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 30,
    width: 120,
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: '#2196F3',
  },
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white',
  },
});
