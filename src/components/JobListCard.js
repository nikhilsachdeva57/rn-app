import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, Image, ToastAndroid, TouchableOpacity, Dimensions } from 'react-native';
//import Icon from 'react-native-vector-icons/FontAwesome';
import {Card, Icon} from 'react-native-elements';
import {isEmpty} from 'lodash-es';
import {withNavigation} from 'react-navigation';
import {useStoreActions, useStoreState} from 'easy-peasy'
import JobsDetail from '../containers/jobs/JobsDetail';
function JobListCard(props){

	const {
		assignJob
	} = useStoreActions(actions => actions.user);

  const userData = useStoreState(state => state.user);
const onYesPressHandler = (jobId) => {
	const variables = {
		candidateId : userData.id,
		jobId: jobId
	}
	assignJob(variables).then(
		details => {
				return true;
		},
		error => {
		  alert(error);
		},
	  );
}


function onApplyPressHandler(applicationOpen,jobId){
	if(applicationOpen){
		Alert.alert(
		'Confirm Apply',
		'Applying for this position will begin your preparation!',
		[
		  { text: 'Yes', onPress: () => onYesPressHandler(jobId) },
		  {
			text: 'No', onPress: () => {
				 
			}
		  },
		],
		{ cancelable: false }
	  );
	}
	else{
		ToastAndroid.show("Please complete the active course before applying for another job.", ToastAndroid.SHORT);
	}
}
return (
<TouchableOpacity onPress={() => props.navigation.navigate('JobsDetail', {
	job : props.data,
})}>
	<View style={styles.card}>
		<View style={styles.row}>
	  <Text style={styles.jobTitle}>{props.data.job_title} </Text>
	   </View>
	   <View style={styles.row}>
  <Text style={{marginBottom: 5, width: 200, flex :2}}>
    {props.data.company ? props.data.company.name : null}
  </Text>
  <Image style={styles.logo} source={{uri :props.data.company.company_logo}}></Image>
  </View>
<View style={styles.row}>
	
  <Icon name="location-on" size={20} style={{alignSelf: "flex-start", }} />

    { props.data.job_locations ? (
			props.data.job_locations.map((item,key) => {
				return(
					<Text key={key} style={{fontSize: 13,textAlign: 'center'}}>{item} </Text>
				)
			
			})
	) : null}

  </View>

	<View style={styles.row}>
  <Text style={styles.rupeeSymbol}>{'\u20B9'}</Text>
  <Text style={{marginBottom: 5, fontSize: 13, marginTop: 2, marginLeft: 5}}>
   {props.data.ctc_range ? props.data.ctc_range.name : null}
  </Text>
  </View>

  <View style={{position: "relative"}}>
  <TouchableOpacity
			  onPress={() => onApplyPressHandler(props.application_open,props.data.id)}
              style={props.application_open ? styles.applyButton : styles.inactiveButton}>
                <Text
                  style={{
                    alignSelf: 'center',
                    justifyContent: 'center',
                    textAlign: 'center',
					color: 'white',
					fontWeight: "700",
                  }}>
                  Apply
                </Text>
              </TouchableOpacity>
			  </View>
</View>
</TouchableOpacity>
);
				}
const {height, width} = Dimensions.get('window');
const styles = StyleSheet.create({
	jobTitle: {
		alignSelf: "flex-start",
		fontSize: 15,
		color: "#F4511E",
		fontWeight: "700"
	},
	applyButton : {
		height: 40,
		elevation:3,
		width: 100,
		margin : 10,
		borderRadius: 25,
		backgroundColor: '#F14836',
		justifyContent: 'center',
		alignSelf: 'flex-end',
	},
	inactiveButton : {
		height: 40,
		elevation:3,
		width: 100,
		margin: 10,
		borderRadius: 25,
		backgroundColor: '#4e4e4e',
		justifyContent: 'center',
		alignSelf: 'flex-end',
	},
	logo : {

		padding: 10,
		position: "relative",
		width : width*0.4,
		justifyContent : "flex-end",

	},
	card : {
		margin :10,
		padding: 10,
		elevation: 2,
		backgroundColor: "#fff",
		height: 180,
		borderRadius : 10,

	},
	titleStyle : {
		margin: 0,
		padding: 0,
		color: "#3722f6",
		alignSelf: "flex-start",
	},
	
	imageStyle:{
		height: 40,
		width: 10,
	},
	row: {
		flexDirection: 'row',
		width : width*0.8,
	},
	rupeeSymbol : {
		fontSize: 20,
		marginLeft: 5,
		fontWeight: "700",
	}
  });
export default withNavigation(JobListCard);

