import { createStackNavigator, createAppContainer } from 'react-navigation';

import QuizLanding from '../src/containers/QuizLanding/QuizLanding';
import QuestionLanding from '../src/containers/QuestionLanding/QuestionLanding';
import Classroom from '../src/containers/Classroom/Classroom';
import CourseContent from '../src/containers/CourseContent/CourseContent';
import OnBoardingNavigator from '../routes/OnBoardingNavigator';
import Prepare from '../src/containers/Prepare/Prepare';
import QuestionLandingSubmit from '../src/components/questionLanding/QuestionLandingSubmit';
import ScheduleAppointment from '../src/containers/ScheduleAppointment';

const ClassroomNavigator = createStackNavigator({
  Prepare,
  OnBoardingNavigator,
  Classroom,
  CourseContent,
  QuizLanding,
  QuestionLanding,
  QuestionLandingSubmit,
  ScheduleAppointment,
});

export default createAppContainer(ClassroomNavigator);
