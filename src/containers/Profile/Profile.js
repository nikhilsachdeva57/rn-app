import React, { Component, useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  Dimensions,
  ScrollView,
  ProgressBarAndroid,
  ProgressViewIOS,
  ImageBackground,
  TouchableOpacity,
  Linking,
  ToastAndroid,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from "react-native-vector-icons/Feather";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Card, Button } from 'react-native-elements';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { isEmpty } from 'lodash-es';
import * as tokenUtils from '../../utils/tokenUtils'
// import * as profileUtils from '../../utils/profileUtils'


export default function Profile(props) {
  const [isResumeUploaded, setResumeUploaded] = useState(false);
  const user = useStoreState(state => state.user);
  const {
    getCandidateBasicInfo,
    getUserMobile,
    getCandidateEducationInfo,
    getCandidateWorkExperienceInfo,
    getCandidatePreferenceInfo,
    getCandidateResume,
    getCandidateCurrentEmploymentInfo,
  } = useStoreActions(
    actions => actions.user,
  );

  let currentProgress = 0.6;
  let resultProgress = currentProgress * 100;

  useEffect(() => {
    if (!isEmpty(user.id)) {
      getCandidateBasicInfo(user.id);
      getUserMobile(user.id);
      getCandidateEducationInfo(user.id);
      getCandidatePreferenceInfo(user.id);
      getCandidateWorkExperienceInfo(user.id);
      getCandidateResume(user.id);
      getCandidateCurrentEmploymentInfo(user.id);

    }
  }, []);


  const onLogoutPressHandler = () => {
    tokenUtils.deleteToken();
    props.navigation.navigate('Splash');
  };

  const handleClick = (url) => {
    console.log('url', url)
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        ToastAndroid.show('Please upload a different file', ToastAndroid.SHORT);
      }
    });
  };

  const { width, height } = Dimensions.get('window')
  return (
    // <ImageBackground
    //   source={require('../../../assets/main_bg,png')}
    //   style={{width: '100%', height: '100%', flex: 1}}>
    <ScrollView>
      <View style={styles.container}>
        <Card
          title="Profile"
          containerStyle={styles.containerProfileCardStyle}>
          <View style={styles.profileImageStack}>
            <Image
              style={styles.image}
              source={{
                uri:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSv21QQVsRVx8pLrTRrjMsihk6Z5jvsVbSm321iAnv9wDmArXDz&s',
              }}></Image>
            <Ionicons name="ios-add-circle" style={styles.icon} />
          </View>
          {!isEmpty(user.basic_info) ? (
            <View>
              <Text style={styles.userName}>{user.basic_info.first_name ? user.basic_info.first_name : null}</Text>
              <Text style={styles.userEmail}>{user.basic_info.email ? user.basic_info.email : null}</Text>
              <Text style={styles.userMobile}>{user ? user.mobile : null}</Text>
              <View style={{ flex: 1, flexDirection: 'row', marginTop: 6 }}>
                <View style={styles.userCurrentCity}>
                  <Text style={{ textAlign: 'center', alignSelf: 'center', }}>{user.basic_info.currentCity ? user.basic_info.currentCity.name : null}</Text>
                </View>
                <View
                  style={{
                    alignSelf: 'flex-end',
                    fontSize: 25,
                    color: '#f14836',
                    position: 'absolute',
                    end: 0,
                  }}>
                  <TouchableOpacity
                    onPress={() =>
                      props.navigation.navigate('BasicInfo', { from: 'Profile' })
                    }>
                    <MaterialCommunityIcons
                      name="circle-edit-outline"
                      style={{ fontSize: 25, color: '#f14836' }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ) : (

              <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 16 }}>
                <TouchableOpacity
                  onPress={() =>
                    props.navigation.navigate('BasicInfo', { from: 'Profile' })
                  }>
                  <Ionicons
                    name="ios-add-circle"
                    style={{ color: '#f14836', fontSize: 40, alignSelf: 'center' }}
                  />
                </TouchableOpacity>
              </View>
            )}

        </Card>

        {/*
 progress
*/}
        <Card
          containerStyle={{
            height: 100,
            borderRadius: 10,
            borderTopLeftRadius: 11,
            backgroundColor: 'transparent',
          }}
          wrapperStyle={{ borderColor: '#5DE289' }}>
          <View style={{ margin: 12 }}>
            <Text>
              Profile Strength:{' '}
              <Text style={{ fontWeight: 'bold' }}> {resultProgress} %</Text>
            </Text>
          </View>
          <View>
            {Platform.OS === 'android' ? (
              <ProgressBarAndroid
                styleAttr="Horizontal"
                progress={0.5}
                indeterminate={false}
                style={{ marginHorizontal: 12 }}
              />
            ) : (
                <ProgressViewIOS progress={70} />
              )}
            {/* <Text style={{alignSelf:'flex-end'}}>100%</Text> */}
          </View>
        </Card>
        {/* 
Resume Upload
*/}

        <Card
          containerStyle={{
            borderRadius: 10,
            borderTopLeftRadius: 11,
            backgroundColor: 'transparent',
          }}
          wrapperStyle={{ borderColor: '#5DE289' }}>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('CVUpload')
            }>
            <View style={{ flex: 1, marginHorizontal: 12, flexDirection: 'row', marginBottom: 12, }}>
              <Text style={{ justifyContent: 'center', marginTop: 2, fontWeight: 'bold' }}>
                Resume Upload
                </Text>
            </View>
            <View style={{ marginHorizontal: 12 }}>
              {!isEmpty(user.candidate_candidate_resume) ? (
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <Feather name="file-text" style={{ color: '#F14386', fontSize: 24, marginEnd: 8, }} />
                  {/* <Text>File name: {user.candidate_resume.file_name}</Text> */}
                  <Text>Resume</Text>
                  <View style={{ position: 'absolute', end: 0, marginTop: -6, }}>
                    <Button title={"Show"} titleStyle={{ fontWeight: '200', fontSize: 14, textAlignVertical: 'center' }} buttonStyle={{ justifyContent: 'center', zIndex: 2, borderRadius: 6, height: 35, paddingHorizontal: 12, backgroundColor: '#f14836', }}
                      onPress={() => {
                        handleClick(user.candidate_candidate_resume[0].download_url)
                      }} />
                  </View>
                </View>
              ) : (
                  <TouchableOpacity
                    onPress={() =>
                      props.navigation.navigate('CVUpload')
                    }>
                    <View>
                      <View>
                        <MaterialCommunityIcons
                          name="cloud-upload"
                          style={{
                            color: '#f14836',
                            fontSize: 25,
                            position: 'absolute',
                            marginTop: -6,
                            end: 0,
                          }}
                        />
                      </View>
                      <Text>Please upload your resume</Text>
                    </View>
                  </TouchableOpacity>
                )}
            </View>
          </TouchableOpacity>
        </Card>
        {/*                
          
          Job Preferences

           */}

        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate('Preferences', { from: 'Profile' })
          }>
          <Card
            containerStyle={{
              borderRadius: 10,
              borderTopLeftRadius: 11,
              backgroundColor: 'transparent',
            }}
            wrapperStyle={{ borderColor: '#5DE289' }}>
            <View style={{ marginHorizontal: 12, flexDirection: 'row', marginBottom: 12, }}>
              <Text style={{ justifyContent: 'center', marginTop: 13, fontWeight: 'bold' }}>
                Job Preferences
                </Text>
              <MaterialCommunityIcons
                name="circle-edit-outline"
                style={styles.editPreferences}
              />
            </View>
            <View style={{ marginHorizontal: 20, }}>
              {!isEmpty(user.candidate_preference_info) ? (
                <View>
                  <Text>Desired CTC: {user.candidate_preference_info.desired_ctc ? user.candidate_preference_info.desired_ctc : null} LPA</Text>
                  <Text>Desired Industry: {user.candidate_preference_info.desired_industry ? user.candidate_preference_info.desired_industry : null}</Text>
                  <Text>Desired Role: {user.candidate_preference_info.desired_role ? user.candidate_preference_info.desired_role : null}</Text>
                  <Text>Min CTC: {user.candidate_preference_info.min_ctc ? user.candidate_preference_info.min_ctc : null}</Text>
                  <Text>Preferred Location: {user.candidate_preference_info.preferred_location ? user.candidate_preference_info.preferred_location : null}
                  </Text>
                </View>
              ) : null}
            </View>
          </Card>
        </TouchableOpacity>
        {/*                
          
                Work Experience
          
           */}
        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate('WorkExperience', { from: 'Profile' })
          }>
          <Card
            pointerEvents="none"
            containerStyle={{
              borderRadius: 10,
              borderTopLeftRadius: 11,
              backgroundColor: 'transparent',
            }}
            wrapperStyle={{ borderColor: '#5DE289' }}>
            <View style={{ marginHorizontal: 12, flexDirection: 'row', marginBottom: 12, }}>
              <Text style={{ justifyContent: 'center', marginTop: 13, fontWeight: 'bold' }}>
                Work Experience
                </Text>
              <Ionicons
                name="ios-add-circle"
                style={styles.editPreferences}
              />
            </View>
            <View style={{ marginHorizontal: 15, }}>
              {!isEmpty(user.candidate_current_work_info) ? (
                <View>
                  <Text>Company Name: {user.candidate_current_work_info.current_company ? user.candidate_current_work_info.current_company : null}</Text>
                  <Text>Current CTC: {user.candidate_current_work_info.currentCtc ? user.candidate_current_work_info.currentCtc.name : null}</Text>
                  <Text>Current Role: {user.candidate_current_work_info.currentRole ? user.candidate_current_work_info.currentRole.name : null} </Text>
                  <Text>Notice Period: {user.candidate_current_work_info.noticePeriod ? user.candidate_current_work_info.noticePeriod.name : null} </Text>
                </View>
              ) :
                (!isEmpty(user.candidate_work_experience_info) ? (
                  <View>
                    <Text>Company Name: {user.candidate_work_experience_info.company_name ? user.candidate_work_experience_info.company_name : null}</Text>
                    <Text>CTC/Salary: {user.candidate_work_experience_info.companyCtc ? user.candidate_work_experience_info.companyCtc.name : null}</Text>
                    <Text>Role: {user.candidate_work_experience_info.jobRole ? user.candidate_work_experience_info.jobRole.name : null}</Text>
                  </View>
                ) : null
                )}
            </View>
          </Card>
        </TouchableOpacity>
        {/*                
          
                Educational Information
          
           */}
        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate('HigherEducation', { from: 'Profile' })
          }>
          <Card
            containerStyle={{
              borderRadius: 10,
              borderTopLeftRadius: 11,
              backgroundColor: 'transparent',
            }}
            wrapperStyle={{ borderColor: 'black' }}>
            <View style={{ marginHorizontal: 12, flexDirection: 'row', marginBottom: 12, }}>
              <Text style={{ justifyContent: 'center', marginTop: 13, fontWeight: 'bold' }}>
                Educational Information
                </Text>
              <Ionicons
                name="ios-add-circle"
                style={styles.editPreferences}
              />
            </View>
            <View style={{ marginHorizontal: 20, }}>
              {!isEmpty(user.candidate_bachelor_degree_info) ? (
                <View>
                  <Text>College Name: {user.candidate_bachelor_degree_info.college_name ? user.candidate_bachelor_degree_info.college_name : null}</Text>
                  <Text>Degree: {user.candidate_bachelor_degree_info.candidateDegree ? user.candidate_bachelor_degree_info.candidateDegree.name : null}</Text>
                  <Text>Year of Graduation: {user.candidate_bachelor_degree_info.year_of_graduation ? user.candidate_bachelor_degree_info.year_of_graduation : null}
                  </Text>
                </View>
              ) : null}
            </View>
          </Card>
        </TouchableOpacity>
        <View
          style={{ justifyContent: 'center', alignSelf: 'center', flex: 1 }}>
          <TouchableOpacity
            onPress={onLogoutPressHandler}
            style={styles.logoutButton}
            activeOpacity={0.5}>
            <Text
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                textAlign: 'center',
                color: 'white',
              }}>
              Logout
              </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView >
    // </ImageBackground>
  );
}

const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  logoutButton: {
    marginTop: 12,
    borderWidth: 1,
    borderColor: '#fff',
    flex: 1,
    width: width / 3,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#F14836',
    justifyContent: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
  },
  containerProfileCardStyle: {
    flexDirection: 'column',
    borderRadius: 10,
    borderTopLeftRadius: 11,
    shadowColor: '#235734',
    backgroundColor: 'transparent',
  },
  profileImageStack: {
    width: 120,
    height: 100,
  },
  container: {
    flex: 1,
    marginVertical: 12,
    backgroundColor: 'transparent',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    position: 'absolute',
    left: width * 0.3,
  },
  icon: {
    marginLeft: 4,
    top: 0,
    left: width * 0.5 - 12,
    position: 'absolute',
    color: '#f14836',
    fontSize: 30,
  },
  profile: {
    color: '#121212',
    fontFamily: 'roboto-regular',
    marginTop: -165,
    marginLeft: 165,
  },

  userName: {
    color: '#121212',
    fontFamily: 'roboto-regular',
    marginTop: 12,
    alignSelf: 'center',
  },
  userEmail: {
    color: '#121212',
    fontFamily: 'roboto-regular',
    marginTop: 7,
    alignSelf: 'center',
  },
  userMobile: {
    color: '#121212',
    fontFamily: 'roboto-regular',
    marginTop: 8,
    alignSelf: 'center',
  },
  userCurrentCity: {
    color: '#121212',
    fontFamily: 'roboto-regular',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  editPreferences: {
    color: '#f14836',
    fontSize: 25,
    position: 'absolute',
    marginTop: 12,
    end: 0,
  },
});
