import queryString from 'query-string';
import camelizeKeys from 'utils/camelizeKeys';
// import logHelpers from 'helpers/log';

export default function fetchJSON(url, options = {}) {
  return new Promise((resolve, reject) => {
    options = {
      ...options,
      method: options.method || 'get',
      mode: 'cors',
      headers: {
        ...options.headers,
        // Authorization: getAuthorizationHeader(),
      },
    };

    if (options.body) {
      options.body = JSON.stringify(options.body);
    }

    fetch(`${url}${options.query ? `?${queryString.stringify(options.query)}` : ''}`, options)
      .then(response => new Promise((innerResolve, innerReject) => {
        response.json()
          .then((json) => {
            innerResolve({ statusCode: response.status, ...json });
          }, (err) => {
            // logHelpers.logError(err);
            innerReject(err);
          })
          .catch((err) => {
            // logHelpers.logError(err);
            innerReject(err);
          });
      }))
      .then((jsonResponse) => {
        if (jsonResponse.statusCode >= 200 && jsonResponse.statusCode <= 299) {
          resolve(camelizeKeys(jsonResponse));
        } else {
          // logHelpers.logError(jsonResponse);
          reject(camelizeKeys(jsonResponse));
        }
      })
      .catch((error) => {
        // logHelpers.logError(error);
        reject(error);
      });
  });
}
