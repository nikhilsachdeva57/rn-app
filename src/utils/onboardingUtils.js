

export const getOnBoardingState = (config) => {
    if(config=="basc_info_1") return "BasicInfo";
    if(config=="higher_education_1") return "HigherEducation";
    if(config=="work_experience_1") return "WorkExperience";
    if(config=="preference_1") return "Preferences";
  }
