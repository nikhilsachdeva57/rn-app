import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import HigherEducation from '../src/containers/Onboarding/HigherEducation';
import BasicInfo from '../src/containers/Onboarding/BasicInfo';
import WorkExperience from '../src/containers/Onboarding/WorkExperience';
import Preferences from '../src/containers/Onboarding/Preferences';
import OnBoardingLanding from '../src/containers/Onboarding/OnBoardingLanding'
import Prepare from '../src/containers/Prepare/Prepare'
const OnBoardingNavigator = createSwitchNavigator({
  OnBoardingLanding,
  BasicInfo,
  HigherEducation,
  WorkExperience,
  Preferences,
  Prepare,
},

  {
    title: 'Onboarding',
  });

export default createAppContainer(OnBoardingNavigator);
