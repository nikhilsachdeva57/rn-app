import {thunk, action} from 'easy-peasy';
import {
  getAppIntroConfig as getAppIntroConfigAPI,
  getAppVerionConfig as getAppVerionConfigAPI,
} from '../../restApi/user';

export default {
  application_config_app_config_v0: '',
  application_config_app_version_config: '',

  getAppIntroConfig: thunk((actions, payload) => {
    return getAppIntroConfigAPI().then(details => {
      return actions.updateUserDetails(details.data);
    });
  }),

  getAppVerionConfig: thunk((actions, payload) => {
    return getAppVerionConfigAPI().then(details => {
      return actions.updateUserDetails(details.data);
    });
  }),

  updateUserDetails: action((state, payload) => {
    return {
      ...state,
      ...payload,
    };
  }),
};
