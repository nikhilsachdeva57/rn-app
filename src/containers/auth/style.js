import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  topTitle:{
    flex:1,
    fontWeight:'500',
    fontSize:24,
    textAlign:'center',
    textAlignVertical:'center'
  },
  topVerification:{
    flex:0.5,
    fontWeight:'400',
    fontSize:18,
    textAlign:'center',
    textAlignVertical:'center'
  },
  mobileIcon: {
    width: 60,
    height: 109,
    alignSelf:'center',
  },
  topText: {
    marginTop: 30,
    textAlign: "center",
    fontSize: 25,
    fontWeight: "400",
  },
  input: {
    flex:4,
    fontSize: 16,
    fontWeight: "300",
    paddingStart:20,
    borderStartWidth:2,
    borderStartColor:'red'
  },
  button: {
    borderRadius: 30,
    alignSelf: "center",
    borderWidth: 1,
    height:55,
    width:"100%",
    borderColor: "#00000040",
    alignItems: 'center',
    backgroundColor: "#F14836",
    justifyContent:'center',
    marginTop: 40,
  },
  disabledButton: {
    borderRadius: 30,
    height:55,
    alignSelf: "center",
    borderWidth: 1,
    width:"100%",
    justifyContent:'center',
    marginTop: 40,
    borderColor: "#00000040",
    alignItems: 'center',
    backgroundColor: "#F1483640"
  },

  buttonText: {
    textAlign: 'center',
    justifyContent:'center',
    fontSize: 18,
    fontWeight:"300",
    color: 'white'
  }
});