import React, { Component, useState } from 'react';
import styles from './style';
import {
  TextInput,
  View,
  TouchableOpacity,
  Text,
  ToastAndroid,
  Image,
} from 'react-native';
import { useStoreActions, useStoreState } from 'easy-peasy';
import axios from '../../axiosClient';
//cannot enter ph
export default OTP = props => {
  const { getUserId } = useStoreActions(actions => actions.user);
  const [validOTP, setValidOTP] = useState(false);
  const [otp, setOtp] = useState('');
  const [loading, setLoading] = useState(false);
  const [mobile,setMobile] = useState(props.navigation.getParam('mobile'));

  const onSendOTPPressHandler = (event) => {
    
    axios.createOTP({ mobile }).then((resp) => {
      ToastAndroid.show("" + resp.message, ToastAndroid.SHORT);
    }).catch((err) => {
      ToastAndroid.show("" + err.message, ToastAndroid.SHORT);
      console.debug(err);
    });
  }

  const onOTPChangeHandler = event => {
    setOtp(event);
    if (event.length === 4) setValidOTP(true);
    else setValidOTP(false);
  };

  const onVerifyOTPPressHandler = event => {
    setLoading(true);
    axios
      .verifyOTP({ mobile, otp })
      .then(resp => {
        getUserId();
        ToastAndroid.show('' + resp.message, ToastAndroid.SHORT);
        props.navigation.navigate('Home');
      })
      .catch(err => {
        setLoading(false);
        ToastAndroid.show('' + err.message, ToastAndroid.SHORT);
      });
  };

  return (
    <View style={{ flex: 1, margin: 24, }}>
      <Text style={styles.topTitle}>Enter OTP</Text>
      <View style={{ flex: 2, justifyContent: 'center' }}>
        <Image source={require('../../../assets/otp_screen.png')} style={styles.mobileIcon} />
      </View>
      <Text style={styles.topVerification}>Verification</Text>
      <Text style={{ flex: 1, marginTop: 12, fontSize: 14, textAlignVertical: 'center' }}>
        <Text>Enter the OTP sent to </Text>
        <Text style={{ fontWeight: "bold" }}> +91 </Text>
        <Text style={{ fontWeight: "bold" }}>{mobile}</Text>
      </Text>
      <View style={{ flex: 1,justifyContent: 'center', }}>
        <View style={{ height: 50, borderWidth: 1, flexDirection: 'row', borderRadius: 10, justifyContent: 'center' }}>
          <TextInput
            style={{
              flex: 4,
              fontSize: 16,
              fontWeight: "300",
              paddingStart: 20,
            }}
            onChangeText={onOTPChangeHandler}
            keyboardType={'numeric'}
            maxLength={4}
            placeholder="Enter OTP"
          />
        </View>
      </View>
      <TouchableOpacity
        disabled={!validOTP || loading}
        onPress={onVerifyOTPPressHandler}
        style={{ flex: 2, }}>
        <View
          style={validOTP && !loading ? styles.button : styles.disabledButton}>
          <Text style={styles.buttonText}>Verify OTP</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{ flex: 1 }}
        onPress={onSendOTPPressHandler}>
        <View style={{ flex: 0.5, flexDirection: 'row', justifyContent: 'flex-start' }}>
          <Text style={{ fontSize: 14, }}>Didn't receive the verification OTP? </Text>
          <Text style={{ color: '#f14386' }}> Resend Again</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
