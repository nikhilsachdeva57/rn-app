import React, { Component, useState } from 'react';
import styles from './style'

import {
  TextInput,
  View,
  TouchableOpacity,
  Text,
  ToastAndroid,
  Image,
  StyleSheet,
} from 'react-native';

import axios from '../../axiosClient';

export default Mobile = (props) => {

  const [validMobile, setValidMobile] = useState();
  const [mobile, setMobile] = useState('');
  const [loading, setLoading] = useState(false);
  const onSendOTPPressHandler = (event) => {
    setLoading(true);
    axios.createOTP({ mobile }).then((resp) => {
      ToastAndroid.show("" + resp.message, ToastAndroid.SHORT);
      props.navigation.navigate('OTP', {
        mobile: mobile,
      });
    }).catch((err) => {
      ToastAndroid.show("" + err.message, ToastAndroid.SHORT);
      console.debug(err);
    });
    setLoading(false)
  }

  const onMobileChangeHandler = (event) => {
    setMobile(event);
    if (event.length == 10) {
      setValidMobile(true);
    }
    else {
      setValidMobile(false);
    }
    console.log(validMobile,loading);
  }

  return (
    <View style={{ flex: 1, margin: 24, }}>
      <Text style={styles.topTitle}>Welcome!</Text>
      <View style={{ flex: 2, justifyContent:'center' }}>
        <Image source={require('../../../assets/mobile.png')} style={styles.mobileIcon} />
      </View>
      <Text style={styles.topVerification}>Verification</Text>
      <Text style={{flex:1,marginTop:12, fontSize:14, textAlignVertical:'center'}}>
        <Text>We will send you a</Text>
        <Text style={{ fontWeight: "bold" }}> One Time Password </Text>
        <Text>on you mobile number</Text>
      </Text>
      <View style={{ flex: 1, justifyContent: 'center',}}>
        <View style={{ height: 50, borderWidth: 1, flexDirection: 'row', borderRadius: 10, justifyContent: 'center' }}>
          <View style={{ borderEndWidth: 1, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center' ,textAlignVertical:'center' , fontWeight:"500"}}>+91</Text>
        </View>
        <TextInput
          style={styles.input}
          onChangeText={onMobileChangeHandler}
          keyboardType={'numeric'}
          maxLength={10}
          placeholder="Enter Mobile No"
        />
      </View>
    </View>
    <TouchableOpacity style={{flex:2,flexDirection:'column',justifyContent:'center'}}disabled={!validMobile || loading} onPress={onSendOTPPressHandler}>
      <View style={validMobile && !loading ? styles.button : styles.disabledButton}>
        <Text style={styles.buttonText}>Send OTP</Text>
      </View>
    </TouchableOpacity>
    <View style={{flex:1,}}></View>
    </View>
  );
};
