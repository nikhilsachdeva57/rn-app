import gql from 'graphql-tag';

export const sectionList  = gql
`query course_courses($slug: String) {
    course_courses(where: {slug: {_eq: $slug}}) {
      modules(order_by: {sort_order: asc}) {
        module {
          slug
          title
          estimated_time_in_mins
          sections(order_by: {sort_order: asc}) {
            section {
              content_type
              title
              description
              slug
              estimated_time_in_mins
              id
            }
            sort_order
            
          }
        }
        sort_order
      }
      slug
      title
      id
    }
  }`


  export const sectionBySlug = gql
   `query course_sections($slug: String) {
    course_sections(where: {slug: {_eq: $slug}}) {
      content_type
      description
      is_active
      quiz {
        description
        title
        slug
        questions {
          question {
            estimated_time_in_mins
            options
            text
            slug
            format_id
            hint
            solutions {
              solution {
                content
                content_type
                slug
                title
              }
            }
          }
        }
      }
      estimated_time_in_mins
      title
      slug
      video {
        slug
        title
        url
        video_source_id
      }
    }
  }`
  
export const candidateCourseTrack = gql 
`
query candidateCourseTrack($candidate_id: uuid!, $course_slug: String!){
  candidate_activities_course_track(where: {candidate_id: {_eq: $candidate_id}, course_slug: {_eq: $course_slug}}, order_by: {completed_at: desc}) {
    id
    is_completed
    course_slug
    candidate_id
    section_slug
  }
}
`
export const insertCourseTracks = gql 
`
mutation insertCourseTrack($candidate_id: uuid!, $course_slug: String!, $section_slug: String!) {
  insert_candidate_activities_course_track(objects: {candidate_id: $candidate_id, course_slug: $course_slug, section_slug: $section_slug}) {
    returning {
      id
    }
  }
}
`