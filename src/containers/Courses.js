import React, {useState} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import CourseCard from '../components/CourseCard';

export default function App(props) {
  const [getPopularCourses, setPopularCourses] = useState([
    {
      id: 1,
      is_active: true,
      title: 'title',
      description: 'description',
      image:
        'https://visualhierarchy.co/blog/wp-content/uploads/2016/01/unnamed-327x34.png',
    },
    {
      id: 2,
      is_active: true,
      title: 'title',
      description: 'description',
      image:
        'https://visualhierarchy.co/blog/wp-content/uploads/2016/01/unnamed-327x34.png',
    },
    {
      id: 3,
      is_active: true,
      title: 'title',
      description: 'description',
      image:
        'https://visualhierarchy.co/blog/wp-content/uploads/2016/01/unnamed-327x34.png',
    },
    {
      id: 4,
      is_active: true,
      title: 'title',
      description: 'description',
      image:
        'https://visualhierarchy.co/blog/wp-content/uploads/2016/01/unnamed-327x34.png',
    },
  ]);

  const [
    getBusinessDevelopmentCourses,
    setBusinessDevelopmentCourses,
  ] = useState([
    {
      id: 1,
      is_active: true,
      title: 'title',
      description: 'description',
      image:
        'https://visualhierarchy.co/blog/wp-content/uploads/2016/01/unnamed-327x34.png',
    },
    {
      id: 2,
      is_active: true,
      title: 'title',
      description: 'description',
      image:
        'https://visualhierarchy.co/blog/wp-content/uploads/2016/01/unnamed-327x34.png',
    },
    {
      id: 3,
      is_active: true,
      title: 'title',
      description: 'description',
      image:
        'https://visualhierarchy.co/blog/wp-content/uploads/2016/01/unnamed-327x34.png',
    },
    {
      id: 4,
      is_active: true,
      title: 'title',
      description: 'description',
      image:
        'https://visualhierarchy.co/blog/wp-content/uploads/2016/01/unnamed-327x34.png',
    },
  ]);

  return (
    <View style={{flex: 1, backgroundColor: 'white', paddingTop: 20}}>
      {getPopularCourses && (
        <Text style={styles.popularCourses}>Popular Courses</Text>
      )}
      <View style={{marginTop: 10}}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {getPopularCourses &&
            getPopularCourses.map(item => {
              return (
                <CourseCard imageUri={item.image} name="Home" key={item.id} />
              );
            })}
        </ScrollView>
      </View>
      <View style={{marginTop: 20}}>
        <Text style={styles.businessDevelopmentCourses}>
          Business Development Courses
        </Text>
      </View>
      <View style={{marginTop: 10}}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {getPopularCourses &&
            getPopularCourses.map(item => {
              return (
                <CourseCard imageUri={item.image} name="Home" key={item.id} />
              );
            })}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  popularCourses: {
    fontWeight: 'bold',
    fontSize: 24,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  businessDevelopmentCourses: {
    fontWeight: 'bold',
    fontSize: 24,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
});
