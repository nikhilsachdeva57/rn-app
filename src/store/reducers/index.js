import {combineReducers} from 'redux'

import register from './register';


export { default as register } from './register';

export const reducers = combineReducers({
     register,
})
