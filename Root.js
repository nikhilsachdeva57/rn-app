import React from 'react';
import {ApolloProvider, Query} from 'react-apollo';
import {StoreProvider} from 'easy-peasy';
import {Provider} from 'react-redux';
import apollo from './apollo';
import App from './App';
import store from './src/store';


export default function Root(props) {
  return (
    <Provider store={store}>
      <StoreProvider store={store}>
          <App />
      </StoreProvider>
    </Provider>
  );
}
