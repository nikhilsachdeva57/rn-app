import React from 'react';

export const LOGIN_HEADER = <>Ready to start your next career journey?</>;
export const LOGIN_LABEL = 'Enter registered mobile number';
export const LOGIN_PLACEHOLDER = 'Registered mobile number';
export const LOGIN_OTP_HEADER = 'We’ve sent an OTP to your mobile number';
export const LOGIN_OTP_LABEL = 'Enter OTP';
export const LOGIN_OTP_PLACEHOLDER = 'OTP';

export const CHOOSE_AVTAR = 'Choose your avatar';

export const PROFILE_HEADER = 'Enter your name';
export const PROFILE_FIRSTNAME_LABEL = (
  <>
    First name<sup>*</sup>
  </>
);
export const PROFILE_LASTNAME_LABEL = (
  <>
    Last name<sup>*</sup>
  </>
);
export const PROFILE_FIRSTNAME = 'First name';
export const PROFILE_LASTNAME = 'Last name';

// apis endpoints

export const API_HOST = 'https://apollo.lidolearning.com';

export const PROFILE_EDIT_HEADER = 'Edit Your Profile';
export const PROFILE_EDIT_AVATAR_HEADER = 'Choose your avatar';

//TODO: Move this to a separate file named sources
export const CLASS_CHALLENGE = 'class_challenge';
export const HOME = 'home';
export const PAST_CLASSES = 'past_classes';
export const CHAPTER = 'chapter';

export const formatNameMapping = {
  scq: 'Single Choice Question',
  mcq: 'Multiple Choice Question',
  fib: 'Fill in the Blanks',
  numeric: 'Numeric',
  boolean: 'True or False',
  av_response: 'Audio and Video Response',
};

export const slots = [
  {id: 1, startTime: '09:00'},
  {id: 2, startTime: '09:30'},
  {id: 3, startTime: '10:00'},
  {id: 4, startTime: '10:30'},
  {id: 5, startTime: '11:00'},
  {id: 6, startTime: '11:30'},
  {id: 7, startTime: '12:00'},
  {id: 8, startTime: '12:30'},
  {id: 9, startTime: '19:00'},
  {id: 10, startTime: '19:30'},
  {id: 11, startTime: '20:00'},
  {id: 12, startTime: '20:30'},
  {id: 13, startTime: '21:00'},
  {id: 14, startTime: '21:30'},
  {id: 15, startTime: '22:00'},
];


export const app_info_slides = [
  {
    key: 's1',
    title: 'Requirement Gathering',
    text:
      'We gather information regarding your requirements: Information specific to Job description and desired behavioural traits.',
    image: {
      uri: 'https://www.7hires.com/assets/images/our-service-1.jpg',
    },
  },
  {
    key: 's2',
    title: 'Learning & Assessment',
    text:
      'We design a short course curriculum along with assignments and quizzes, focused on your specific role.',
    image: {
      uri: 'https://www.7hires.com/assets/images/our-service-1.jpg',
    },
  },
  {
    key: 's3',
    title: 'Talent Curation',
    text:
      'We curate applications through our blended assessment methods and schedule the best-fit candidates for final interviews.',
    image: {
      uri: 'https://www.7hires.com/assets/images/our-service-1.jpg',
    },
  },
];

