import React, { useEffect } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  StatusBar,
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useStoreActions, useStoreState } from 'easy-peasy';
import isEmpty from 'lodash-es';

import { app_info_slides } from '../../utils/constants'

export default function AppIntro(props) {
  const appConfig = useStoreState(state => state.appConfig);
  const { getAppIntroConfig, getAppVerionConfig } = useStoreActions(
    actions => actions.appConfig,
  );

  useEffect(() => {
    if (isEmpty(appConfig)) {
      getAppIntroConfig();
    }
    // console.log('appconfig',appConfig.application_config_app_config_v0[0].config[0])

    // console.log('asdf',appConfig.application_config_app_config_v0[0].config[0].title )
    // const slides = [
    //   {
    //     key: 's1',
    //     title: !isEmpty(appConfig) ? appConfig.application_config_app_config_v0[0].config[0].title : null,
    //     text: !isEmpty(appConfig) ? appConfig.application_config_app_config_v0[0].config[0].description : null,
    //     image: {
    //       uri: !isEmpty(appConfig) ? appConfig.application_config_app_config_v0[0].config[0].img_url : null,
    //     },
    //   },
    //   {
    //     key: 's2',
    //     title: !isEmpty(appConfig) ? appConfig.application_config_app_config_v0[0].config[1].title : null,
    //     text: !isEmpty(appConfig) ? appConfig.application_config_app_config_v0[0].config[1].description : null,
    //     image: {
    //       uri: !isEmpty(appConfig) ? appConfig.application_config_app_config_v0[0].config[1].img_url : null,
    //     },
    //   },
    //   {
    //     key: 's3',
    //     title: !isEmpty(appConfig) ? appConfig.application_config_app_config_v0[0].config[2].title : null,
    //     text: !isEmpty(appConfig) ? appConfig.application_config_app_config_v0[0].config[2].description : null,
    //     image: {
    //       uri: !isEmpty(appConfig) ? appConfig.application_config_app_config_v0[0].config[2].img_url : null,
    //     },
    //   },
    // ];


  }, []);

  function _renderNextButton() {
    return (
      <View style={styles.buttonCircle}>
        <MaterialIcons
          name="navigate-next"
          color="rgba(255, 255, 255, .9)"
          size={32}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  }
  function _renderDoneButton() {
    return (
      <View style={styles.buttonCircle}>
        <MaterialIcons
          name="done"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  }
  const _onDone = () => {
    props.navigation.navigate('Auth');
  };

  function _renderItem({ item }) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: item.backgroundColor,
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingBottom: 120,
        }}>
        <Image style={styles.image} source={item.image} />
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  }
  return (
    <ImageBackground
      source={require('../../../assets/onboarding_bg.png')}
      style={{ width: '100%', height: '100%', flex: 1 }}>
      <StatusBar hidden />
      <AppIntroSlider
        slides={app_info_slides}
        renderItem={(item) => _renderItem(item)}
        onDone={_onDone}
        renderDoneButton={() => _renderDoneButton()}
        renderNextButton={() => _renderNextButton()}
      />
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 300,
    height: 300,
    marginHorizontal: 24,
    marginBottom: 32,
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
    color: '#A0A0A0',
    textAlign: 'center',
    marginBottom: 24,
    justifyContent: 'flex-end',
    marginHorizontal: 24,
  },
  title: {
    fontSize: 20,
    color: '#444',
    textAlign: 'center',
    marginBottom: 16,
    fontWeight: 'bold',
    marginHorizontal: 24,
  },
  buttonCircle: {
    width: 60,
    height: 60,
    backgroundColor: '#F14836',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -16,
    marginEnd: 10,
  },
});


