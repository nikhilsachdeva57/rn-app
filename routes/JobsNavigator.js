import { createAppContainer, createStackNavigator,} from 'react-navigation';
import JobsList from '../src/containers/jobs/JobsList'
import JobsDetail from '../src/containers/jobs/JobsDetail'

const JobsNavigator = createStackNavigator({
    JobsList : JobsList,
    JobsDetail : JobsDetail


});
  
export default createAppContainer(JobsNavigator);
