import React, {Component, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  Dimensions,
  ScrollView,
  ProgressBarAndroid,
  ProgressViewIOS,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import BasicInfo from './BasicInfo';
import StepIndicator from 'react-native-step-indicator';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import ViewPager from '@react-native-community/viewpager';

export default function StartOnboarding(props) {

  const [currentPage, setCurrentPage] = useState(0);
  const PAGES = ['Page 1', 'Page 2', 'Page 3', 'Page 4', 'Page 5'];

  const secondIndicatorStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#fe7013',
    stepStrokeWidth: 3,
    separatorStrokeFinishedWidth: 4,
    stepStrokeFinishedColor: '#fe7013',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#fe7013',
  };

  renderStepIndicator = params => (
    <MaterialIcon {...getStepIndicatorIconConfig(params)} />
  );

  const getStepIndicatorIconConfig = ({position, stepStatus}) => {
    const iconConfig = {
      name: 'feed',
      color: stepStatus === 'finished' ? '#ffffff' : '#fe7013',
      size: 15,
    };
    switch (position) {
      case 0: {
        iconConfig.name = 'account-circle';
        break;
      }
      case 1: {
        iconConfig.name = 'location-on';
        break;
      }
      case 2: {
        iconConfig.name = 'work';
        break;
      }
      case 3: {
        iconConfig.name = 'payment';
        break;
      }
      case 4: {
        iconConfig.name = 'track-changes';
        break;
      }
      default: {
        break;
      }
    }
    return iconConfig;
  };

  function onStepPress(position) {
      setCurrentPage(position);
    this.viewPager.setPage(position)
  }

  function renderViewPagerPage(data){
    return (
      <View style={styles.page}>
        <Text>{data}</Text>
      </View>
    )
  }

  return (
    <View style={{flex: 1, flexDirection: 'column'}}>
      {/* <View style={styles.stepIndicator}> */}
        <StepIndicator
          style={styles.stepIndicator}
          renderStepIndicator={renderStepIndicator}
          customStyles={secondIndicatorStyles}
          currentPosition={currentPage}
          onPress={(position) => onStepPress(position)}

          // labels={[
          //   // 'Cart',
          //   // 'Delivery Address',
          //   // 'Order Summary',
          //   // 'Payment Method',
          //   // 'Track',
          // ]}
        />
      {/* {/* </View> */}
      <ViewPager
          style={{ flexGrow: 1 }}
          ref={viewPager => {
            this.viewPager = viewPager
          }}
          onPageSelected={page => {
            this.setState({ currentPage: page.position })
          }}
        >
          {PAGES.map(page => renderViewPagerPage(page))}
        </ViewPager>

      <BasicInfo  style={{flex: 10, flexDirection: 'column',backgroundColor:'red',}}/>
    </View>
  );
}

const styles = StyleSheet.create({
  stepIndicator: {
    flex: 1,
    marginTop:24,
    backgroundColor:'pink',
  },
});
