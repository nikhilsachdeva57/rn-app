import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {createBottomTabNavigator, createAppContainer} from 'react-navigation';
import Contest from '../src/containers/Contest';
import JobsNavigator from './JobsNavigator';
import ProfileNavigator from './ProfileNavigator';
import ClassroomNavigator from './ClassroomNavigator';
import StartOnboarding from '../src/containers/Onboarding/StartOnboarding';

const HomeNavigator = createBottomTabNavigator(
  {
    Prepare: {
      screen: ClassroomNavigator,
      navigationOptions: {
        tabBarLabel: 'Prepare',
        tabBarIcon: ({tintColor}) => (
          <Icon name="md-school" color={tintColor} size={25} />
        ),
      },
    },
    Jobs: {
      screen: JobsNavigator,
      navigationOptions: {
        tabBarLabel: 'Jobs',
        tabBarIcon: ({tintColor}) => (
          <Icon name="md-briefcase" color={tintColor} size={25} />
        ),
      },
    },
    Contest: {
      screen: Contest,
      navigationOptions: {
        tabBarLabel: 'Contest',
        tabBarIcon: ({tintColor}) => (
          <Icon name="md-create" color={tintColor} size={25} />
        ),
      },
    },
    Profile: {
      screen: ProfileNavigator,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: ({tintColor}) => (
          <Icon name="md-happy" color={tintColor} size={25} />
        ),
      },
    },
  },

  {
    tabBarOptions: {
      upperCaseLabel: false,
      labelStyle: {
        fontSize: 15,
      },
      activeTintColor: '#F14836',
      inactiveTintColor: 'gray',
    },
    headerMode: 'none',
  },
);

export default createAppContainer(HomeNavigator);
