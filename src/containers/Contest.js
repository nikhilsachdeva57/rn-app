import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';

export default Contest = props => {
  return (
    <View style={{padding: 10}}>
      <Text style={styles.topText}>Contest</Text>
      <Text style={styles.topText}>Coming Soon</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  topText: {
    marginTop: 30,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '700',
  },
  input: {
    marginTop: 40,
    margin: 15,
    height: 45,
    borderWidth: 1,
    paddingLeft: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 30,
    width: 120,
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: '#2196F3',
  },
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white',
  },
});
