import {createStackNavigator, createAppContainer} from 'react-navigation';
import Mobile from '../src/containers/auth/Mobile';
import OTP from '../src/containers/auth/OTP';
const AuthNavigator = createStackNavigator({
  Mobile: {
    screen: Mobile,
    navigationOptions: {
      header: null,
    }
  },

  OTP: {
    screen: OTP,
    navigationOptions: {
      header: null,
    }
  }
} , {headerMode: 'screen'} );

export default createAppContainer(AuthNavigator);
