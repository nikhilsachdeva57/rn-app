import gql from 'graphql-tag';

export const GET_CHAPTERS = gql
`
query getChapters($courses: [String!]!) {
    chapters: course_chapters(courses: $courses) {
        id
        thumbnail
        title
    }
}`;

export const GET_CHAPTER_CONTENT = gql
`
query getUnits($chapter: ID!) {
    units: chapter_content(chapter_id: $chapter) {
        content {
            content_type
            video {
                name: title
                thumbnail
                slug
                url
                type
            }
            quiz {
                name
                estimated_time_in_mins
                slug
            }
            activity {
                name: title
                thumbnail
                slug
                url
                estimated_time_in_mins
            }
            game {
                name: title
                thumbnail
                slug
                url
                estimated_time_in_mins
            }
            self_paced_lesson {
                name: title
                thumbnail
                slug
                url
                estimated_time_in_mins
            }
        }
    }
}
`;
