import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

export default RadioSelectOptions = props => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={props.onLeft} style={[props.isLeftSelected ? styles.correctTextContainer : styles.incorrectTextContainer]}>
                <View >
                    <Text style={[props.isLeftSelected ? styles.correctText : styles.incorrectText]}>{props.leftText}</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={props.onRight} style={[props.isLeftSelected ? styles.incorrectTextContainer : styles.correctTextContainer]}>
                <View >
                    <Text style={[props.isLeftSelected ? styles.incorrectText : styles.correctText]}>{props.rightText}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderWidth: 1.5,
        borderColor: '#F14836',
        borderRadius: 8,
        marginHorizontal:12,
        height:40,
    },
    correctTextContainer: {
        flex: 1,
        textAlign: 'center',
        fontSize: 14,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F14836',
    },
    incorrectTextContainer: {
        flex: 1,
        textAlign: 'center',
        fontSize: 14,
        alignItems: 'center',
        justifyContent: 'center',
    },
    correctText: {
        color: 'white',
    },
    incorrectText: {
        color: '#F14836',
    },
});
