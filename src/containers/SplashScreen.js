import React, {Component, useEffect} from 'react';
import * as tokenUtils from '../utils/tokenUtils';
import {useStoreActions, useStoreState} from 'easy-peasy';
import {StyleSheet, View, Text, Image, ActivityIndicator} from 'react-native';

export default SplashScreen = props => {
  const user = useStoreState(state => state.user);

  useEffect(() => {
    console.debug('user', user);
    setTimeout(async function() {
      const token = await tokenUtils.getToken();
      if (token !== null) {
        props.navigation.navigate('Home');
      } else {
        props.navigation.navigate('AppIntro');
      }
    }, 2000);
  }, []);

  return (
    <View style={{justifyContent: 'center', alignItems: 'center', margin: 40}}>
      <Image style={styles.logo} source={require('../../assets/7hires.png')} />
      <Text style={styles.title}>An app for last time preparation</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  logo: {
    alignSelf: 'center',
    marginTop: '70%',
    height: '20%',
    width: '80%',
  },

  title: {
    fontSize: 18,
    marginTop: '5%',
    alignSelf: 'center',
  },
});
