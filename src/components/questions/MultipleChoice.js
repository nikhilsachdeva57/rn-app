import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

const MultipleChoice = props => {
  const handleSelect = props => {
    const {optionId, answerContent, is_correct, answerScore} = props;
    const answerObj = {
      optionId,
      answerContent,
      is_correct,
      score: answerScore,
    };

    if (!props.checkButtonClicked) {
      props.handleAnswerChanged(answerObj);
    }
  };
  return (
    <View>
      <TouchableOpacity
        onPress={() => handleSelect(props)}
        key={props.answerType}>
        <View
          style={[
            props.selectedOption.indexOf(props.optionId) !== -1
              ? styles.selectedAnswer
              : styles.questionView,
          ]}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
            }}>
            <View
              style={[
                props.selectedOption.indexOf(props.optionId) !== -1
                  ? styles.selectedAnswerCircle
                  : styles.questionCircle,
              ]}>
              <Text
                style={{
                  textAlign: 'center',
                  justifyContent: 'center',

                  color:
                    props.selectedOption.indexOf(props.optionId) !== -1
                      ? 'white'
                      : 'black',
                }}>
                {props.answerType}
              </Text>
            </View>
            <View style={{flex: 0.8, justifyContent: 'center'}}>
              <Text style={{lineHeight: 16}}>{props.answerContent}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};
const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  questionView: {
    marginHorizontal: 6,
    margin: 3,
    padding: 6,
    borderRadius: 8,
    borderWidth: 0.5,
  },
  selectedAnswer: {
    marginHorizontal: 6,
    margin: 3,
    padding: 6,
    borderRadius: 8,
    borderWidth: 2,
    borderColor: '#f14836',
  },
  selectedAnswerCircle: {
    height: 27,
    width: 27,
    borderRadius: 150 / 2,
    justifyContent: 'center',
    margin: 15,
    backgroundColor: '#f14f3f',
  },
  questionCircle: {
    height: 27,
    width: 27,
    borderRadius: 150 / 2,
    justifyContent: 'center',
    margin: 15,
    backgroundColor: '#ebe8e8',
  },
});
export default MultipleChoice;
